package my.dearfadz.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;


public class EmptyViewObserver extends RecyclerView.AdapterDataObserver {

    public static EmptyViewObserver attach(@NonNull RecyclerView recyclerView, @Nullable View view, @Nullable RecyclerView.Adapter adapter) {
        EmptyViewObserver observer = new EmptyViewObserver(recyclerView, view);
        observer.setAdapter(adapter);
        return observer;
    }

    private RecyclerView recyclerView;
    private View emptyView;

    private EmptyViewObserver(@NonNull RecyclerView recyclerView, @Nullable View view) {
        this.recyclerView = recyclerView;
        this.emptyView = view;
    }

    private void setAdapter(@Nullable RecyclerView.Adapter adapter) {
        final RecyclerView.Adapter oldAdapter = recyclerView.getAdapter();
        if (oldAdapter != null && oldAdapter.hasObservers()) {
            try {
                oldAdapter.unregisterAdapterDataObserver(this);
            } catch (IllegalStateException e) {
                Log.d(EmptyViewObserver.class.getSimpleName(), "EmptyViewObserver was not registered previously. Ignoring exception");
            }
        }
        recyclerView.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(this);
        }
        updateEmptyView();
    }

    public void setEmptyView(@Nullable View view) {
        emptyView = view;
        updateEmptyView();
    }

    @Override
    public void onItemRangeInserted(int positionStart, int itemCount) {
        updateEmptyView();
    }

    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount) {
        updateEmptyView();
    }

    @Override
    public void onChanged() {
        updateEmptyView();
    }

    public void updateEmptyView() {
        if (emptyView != null) {
            if (recyclerView.getAdapter() == null) {
                emptyView.setVisibility(View.VISIBLE);
            } else {
                emptyView.setVisibility(recyclerView.getAdapter().getItemCount() > 0 ? View.GONE : View.VISIBLE);
            }
        }
    }
}
