package my.dearfadz.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.webkit.MimeTypeMap;

import com.koushikdutta.async.util.StreamUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageUtils {


//    public static String IMAGE_DIRECTORY_NAME = "tempImage";

//    public static File getOutputMediaFile() {
//
//        // External sdcard location
//        File mediaStorageDir = new File(
//                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                Log.d("TAG LOAD IMAGE", "Oops! Failed create "
//                        + IMAGE_DIRECTORY_NAME + " directory");
//                return null;
//            }
//        }
//
//        // Create a media file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
//        return new File(mediaStorageDir, "IMG_" + timeStamp + ".jpg");
//    }

    private static final String INDIVIDUAL_DIR_NAME = "upload_cache";
    private static final int DEFAULT_SIZE = 600;

    @WorkerThread
    public static File saveImageToFile(Context context, File sourceImage) {
        return saveImageToFile(context, sourceImage, DEFAULT_SIZE, DEFAULT_SIZE);
    }

    @WorkerThread
    public static File savePdfImageToFile(Context context, File sourceImage, float h, float w) {
        return saveImageToFile(context, sourceImage, ((int) h), ((int) w));
    }

    @WorkerThread
    @Nullable
    public static File copyDataFromStream(Context context, Uri uri) {
        if (uri == null) return null;

        File cacheDir = getIndividualCacheDirectory(context);
        String type = context.getContentResolver().getType(uri);
        if (type == null) {
            type = "jpg";
        } else {
            type = MimeTypeMap.getSingleton().getExtensionFromMimeType(type);
        }

        File file = new File(cacheDir, System.currentTimeMillis() + "_ori." + type);

        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file, false);

            StreamUtility.copyStream(inputStream, outputStream);

            inputStream.close();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            file.delete();
            file = null;
        }

        return file;
    }

    @WorkerThread
    public static Bitmap getRotatedBitmap(Context context, String filePath) {

        System.gc();
        File cacheDir = getIndividualCacheDirectory(context);

        Bitmap scaled = null;
        try {
            int rotate = getImageOrientation(filePath);
            if (rotate != 0) {
                scaled = rotate(scaled, rotate);
            }
        } catch (OutOfMemoryError e) {
            System.gc();
            if (scaled != null) scaled.recycle();
            //saveImageToFile(context, sourceImage, DEFAULT_SIZE, DEFAULT_SIZE);
            // Out of Memory while scale or rotate, upload non-scaled image
            return scaled;
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return scaled;
    }

    @WorkerThread
    public static File saveImageToFile(Context context, File sourceImage, int width, int height) {

        System.gc();
        File cacheDir = getIndividualCacheDirectory(context);

        Bitmap scaled = null;
        try {
            scaled = decodeSampledBitmapFromFile(sourceImage.getAbsolutePath(), width, height);
            int rotate = getImageOrientation(sourceImage.getAbsolutePath());
            if (rotate != 0) {
                scaled = rotate(scaled, rotate);
            }

            //https://stackoverflow.com/questions/6908604/android-crop-center-of-bitmap
            int dimension = getSquareCropDimensionForBitmap(scaled);
            scaled = ThumbnailUtils.extractThumbnail(scaled, dimension, dimension);

        } catch (OutOfMemoryError e) {
            System.gc();
            if (scaled != null) scaled.recycle();
            //saveImageToFile(context, sourceImage, DEFAULT_SIZE, DEFAULT_SIZE);
            // Out of Memory while scale or rotate, upload non-scaled image
            return sourceImage;
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        File file = new File(cacheDir, sourceImage.getName());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
            if (scaled != null) {
                scaled.compress(Bitmap.CompressFormat.JPEG, 95, fOut);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null) {
                try {
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (scaled != null) scaled.recycle();
        }

        return file;
    }

    //to calculate the dimensions of the bitmap...
    public static int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public static Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();

            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                ex.printStackTrace();
                System.gc();
            }
        }
        return b;
    }

    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    private static Bitmap decodeSampledBitmapFromFile(String pathName, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        //return BitmapFactory.decodeFile(pathName, options);

        // http://my.oschina.net/jeffzhao/blog/80900
        FileInputStream is = null;
        try {
            is = new FileInputStream(pathName);
            return BitmapFactory.decodeFileDescriptor(is.getFD(), null, options);
        } catch (Exception ex) {
            return BitmapFactory.decodeFile(pathName, options);
        } finally {
            try {
                if (is != null) is.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    /**
     * Returns individual application cache directory (for only image caching
     * from ImageLoader). Cache directory will be created on SD card
     * <i>("/Android/data/[app_package_name]/cache/uploac_cache")</i> if card is
     * mounted and app has appropriate permission. Else - Android defines cache
     * directory on device's file system.
     *
     * @param context Application context
     * @return Cache {@link File directory}
     */
    public static File getIndividualCacheDirectory(Context context) {
        File cacheDir = context.getExternalCacheDir();
        File individualCacheDir = new File(cacheDir, INDIVIDUAL_DIR_NAME);
        if (!individualCacheDir.exists()) {
            if (!individualCacheDir.mkdir()) {
                individualCacheDir = cacheDir;
            }
        }
        return individualCacheDir;
    }


    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, @Nullable int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap getResizedBitmapWithRatio(Bitmap bm, float ratio) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(ratio, ratio);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap combineImages(Bitmap bgd, Bitmap fg) {
        Bitmap bmp;

        int width = bgd.getWidth();
        int height = bgd.getHeight();
        int diff = bgd.getWidth() - fg.getWidth();


        bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));

        Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(bgd, 0, 0, null);
        canvas.drawBitmap(fg, diff / 2, 0, paint);

        return bmp;
    }

    public static File saveBitmapToGallery(Context context, Bitmap bitmap) {

        System.gc();

        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "aiPlaceMaking");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        Bitmap scaled = bitmap;

        FileOutputStream fOut = null;
        File file = new File(dir.getAbsolutePath(), System.currentTimeMillis() + ".jpeg");
        try {
            file.createNewFile();
            fOut = new FileOutputStream(file);
            if (scaled != null) {
                scaled.compress(Bitmap.CompressFormat.JPEG, 90, fOut);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null) {
                try {
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (scaled != null) scaled.recycle();
        }

        return file;
    }

    public static File saveBitmapToCache(Context context, Bitmap bitmap, File fileDir, String fileName) {

        System.gc();
        File dir = fileDir;
        if (!dir.exists()) {
            dir.mkdirs();
        }

        Bitmap scaled = bitmap;

        FileOutputStream fOut = null;
        File file = new File(dir.getAbsolutePath(), fileName);
        try {
            file.createNewFile();
            fOut = new FileOutputStream(file);
            if (scaled != null) {
                scaled.compress(Bitmap.CompressFormat.PNG, 90, fOut);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null) {
                try {
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (scaled != null) scaled.recycle();
        }
        MediaScannerConnection.scanFile(context, new String[]{file.getAbsolutePath()}, null, null);
        return file;
    }

    //https://stackoverflow.com/questions/3035692/how-to-convert-a-drawable-to-a-bitmap
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String getUriPath(Context c, Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = c.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s!=null ? s : null;
    }
}
