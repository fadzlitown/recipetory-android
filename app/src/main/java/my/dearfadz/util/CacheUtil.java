package my.dearfadz.util;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import my.fadzlirazali.recipetory.MyApplication;
import my.fadzlirazali.recipetory.model.Recipe;

public class CacheUtil {
    static List<Recipe> recipeList;
    private static final String RECIPE = "recipe";


    @NonNull
    public static CacheUtil getInstance() {
        return SingletonHolder.instance;
    }

    private static class SingletonHolder {
        public static final CacheUtil instance = new CacheUtil();
    }

    public CacheUtil() {
        recipeList = new ArrayList<>();
    }

    public void addAllRecipe(List<Recipe> list) {
        recipeList = list;
        writeUserToDisk();
    }

    public void updateRecipeListing(Recipe updateRecipe){
        if(recipeList!=null && recipeList.size()>0){
            for (int i = 0; i < recipeList.size(); i++) {
                //update recipe item from list if we have it
                if(recipeList.get(i)!=null && recipeList.get(i).getId().equals(updateRecipe.getId())){
                    recipeList.set(i,updateRecipe);
                    writeUserToDisk();
                }
            }
        }
    }

    public void clear() {
        recipeList.clear();
        writeUserToDisk();
    }

    public List<Recipe> getRecipeList() {
        ensureRecipe();
        return recipeList;
    }

    private static synchronized void writeUserToDisk() {
        final List<Recipe> list = recipeList;

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                String json = Util.getGson().toJson(list);
                writeToDisk(RECIPE, json);
                return null;
            }
        };
        //Executes the task with the specified parameters, allowing multiple tasks to run in parallel
//        AsyncTaskCompat.executeParallel(task);
    }

    private void ensureRecipe() {
        if (recipeList.size() == 0) {
            loadFromDisk(RECIPE);
        }
    }


    private void loadFromDisk(String type) {
        String s = readFromDisk(type);

        if (!TextUtils.isEmpty(s)) {
            switch (type) {
                case RECIPE:
                    List<Recipe> hotspotList = Recipe.toList(s);
                    addAllRecipe(hotspotList);
                    break;
            }
        }
    }

    private static File getFileFor(String type) {
        return new File(
                StorageUtils.getCacheDirectory(MyApplication.getInstance(), false),
                "cache" + type + ".dat");
    }

    public static synchronized void writeToDisk(String type, String json) {
        try {
            StreamUtility.writeFile(getFileFor(type), json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public synchronized String readFromDisk(String type) {
        try {
            return StreamUtility.readFile(getFileFor(type));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
