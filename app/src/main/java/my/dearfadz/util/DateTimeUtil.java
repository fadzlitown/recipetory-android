package my.dearfadz.util;

import org.joda.time.DateTime;
import org.joda.time.Period;

public class DateTimeUtil {
    public static String recipePostedTime(DateTime createdDT){
        Period period = new Period(createdDT,DateTime.now());
        if(period.getDays()>0) return period.getDays()==1 ? period.getDays()+ " day ago": period.getDays()+ " days ago";
        else if(period.getHours()>0) return period.getHours()==1 ? period.getHours()+ " hour ago" : period.getHours()+ " hours ago";
        else if(period.getMinutes()>0) return period.getMinutes()==1? period.getMinutes()+ " min ago" : period.getMinutes()+ " mins ago";
        else return "Just now";
    }
}
