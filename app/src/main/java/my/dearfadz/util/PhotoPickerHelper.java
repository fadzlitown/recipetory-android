package my.dearfadz.util;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.CheckResult;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.annotation.Size;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.util.List;

import my.fadzlirazali.recipetory.R;

/**
 * To use this helper
 * <pre>
 * <code>
 *
 *  public void onEditProfilePicture(View view) {
 *      PhotoPickerHelper.getPickerDialog(this).showOffline();
 *  }
 *
 *  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
 *      if (PhotoPickerHelper.isPhotoPickerResult(requestCode) && resultCode == RESULT_OK) {
 *          Uri[] result = PhotoPickerHelper.onActivityResult(context, requestCode, resultCode, data);
 *          // todo something with result
 *      }
 *  }
 * </code>
 * </pre>
 * <p>
 * Other notes: For apps targeting Android 6.0 Marshmallow (API 23) onwards, {@link MediaStore#ACTION_IMAGE_CAPTURE}
 * requires {@link Manifest.permission#CAMERA} permission. Refer https://code.google.com/p/android/issues/detail?id=188073
 */
public class PhotoPickerHelper {

    private static final String TAG = "PhotoPickerHelper";

    private static boolean SUPPORT_MULTI_PICK = false; //Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;

    private static final String PREF_FILE_NAME = "pref_picker_info.xml";
    private static final String KEY_IMAGE_URI = "latest_image";

    private static final int RESULT_CAMERA_PHOTO = 51337;
    private static final int RESULT_GALLERY_PHOTO = 61337;
    private static final int RESULT_GALLERY_LIBRARY = 61338;


    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public static void launchCamera(Activity activity) {
        handleUserChoice(new StartActivityForResultWrapper(activity), 0);
    }

    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public static void launchCamera(Fragment fragment) {
        handleUserChoice(new StartActivityForResultWrapper(fragment), 0);
    }

    @RequiresPermission(anyOf = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public static void launchGallery(Activity activity) {
        handleUserChoice(new StartActivityForResultWrapper(activity), 1);
    }

    @RequiresPermission(anyOf = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public static void launchGallery(Fragment fragment) {
        handleUserChoice(new StartActivityForResultWrapper(fragment), 1);
    }

    /**
     * @param activity The {@link Activity} to call {@link Activity#startActivityForResult(Intent, int)}
     * @return {@link AlertDialog}, null if {@link Activity} is null. users need to manually call {@link AlertDialog#show()}
     */
    @Nullable
    @CheckResult(suggest = "#showOffline()")
    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public static AlertDialog getPickerDialog(@NonNull Activity activity) {
        return getPicker(new StartActivityForResultWrapper(activity));
    }

    /**
     * @param fragment The {@link Fragment} to call {@link Fragment#startActivityForResult(Intent, int)}
     * @return An {@link AlertDialog}, null if {@link Fragment} is null. but users need to manually call {@link AlertDialog#show()}
     */
    @Nullable
    @CheckResult(suggest = "#showOffline()")
    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    public static AlertDialog getPickerDialog(@NonNull Fragment fragment) {
        return getPicker(new StartActivityForResultWrapper(fragment));
    }

    public static boolean hasRequiredCameraPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasRequiredGalleryPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @Nullable
    private static AlertDialog getPicker(final StartActivityForResultWrapper wrapper) {
        Context context = wrapper.getContext();
        if (context == null) return null;

        String[] addPhoto = new String[]{"Camera", "Gallery"};
        return new AlertDialog.Builder(context)
                .setTitle("Select Photo")
                .setItems(addPhoto, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        handleUserChoice(wrapper, id);
                    }
                })
                .setNeutralButton(android.R.string.cancel, null)
                .create();
    }

    private static void handleUserChoice(StartActivityForResultWrapper wrapper, int id) {
        Context context = wrapper.getContext();
        if (context == null) return;

        Intent intent=null;
        int requestCode;
        if (id == 0) {
            intent = getCameraIntent(context);
            requestCode = RESULT_CAMERA_PHOTO;
        } else {
//            intent = getGalleryIntent();
//            requestCode = RESULT_GALLERY_PHOTO;

            requestCode = RESULT_GALLERY_LIBRARY;


            /**
             * https://stackoverflow.com/questions/24467696/android-file-provider-permission-denial
             * https://stackoverflow.com/questions/18249007/how-to-use-support-fileprovider-for-sharing-content-to-other-apps
             * Capture and write uri to this phone, this lbry still not yet solved for below mashmallow.
             *
             * Issue : https://github.com/zhihu/Matisse/pull/89
             * */
            Matisse.from(wrapper.activity)
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .capture(false)
//                    .captureStrategy(new CaptureStrategy(true,"my.fadzlirazali.recipetory.fileprovider"))
                    .maxSelectable(10)
//                    .addFilter(new GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                    .gridExpectedSize(context.getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                    .thumbnailScale(0.85f)
                    .imageEngine(new GlideEngine())
                    .forResult(requestCode);
        }
        if (intent != null && intent.resolveActivity(context.getPackageManager()) != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            wrapper.startActivityForResult(intent, requestCode);
        }
    }

    /**
     * Checks if the requestCode belongs to PhotoPicker or not
     *
     * @param requestCode Request code
     * @return if requestcode is Photo or Gallery request
     */
    public static boolean isPhotoPickerResult(@IntRange(from = 0, to = 61338) int requestCode) {
        return requestCode == RESULT_CAMERA_PHOTO || requestCode == RESULT_GALLERY_LIBRARY;
    }

    /**
     * Check {@link #isPhotoPickerResult(int)} before calling this methos
     *
     * @param context     Context
     * @param requestCode Request Code
     * @param resultCode  Result Code
     * @param data        The Intent containing the data
     * @return An array of {@link Uri} extracted from {@link Intent} data. It may be a file:// or content://
     */
    @Size(min = 0)
    @NonNull
    @CheckResult
    public static Uri[] onActivityResult(Context context, @IntRange(from = 0, to = 61338) int requestCode,
                                         int resultCode, @Nullable Intent data) {

        if (requestCode != RESULT_CAMERA_PHOTO && requestCode != RESULT_GALLERY_PHOTO && requestCode != RESULT_GALLERY_LIBRARY) {
            Log.w(TAG, "requestCode given: " + requestCode + " is not from PhotoPicker. Call isPhotoPickerResult() before call this method.");
            return new Uri[0];
        }

        if (context == null) return new Uri[0];

        Uri[] uri = null;
        if (resultCode == Activity.RESULT_OK) {
            if(requestCode==RESULT_GALLERY_LIBRARY){
                //init your 3rd party library here
                if(data!=null) {
                    uri = new Uri[Matisse.obtainResult(data).size()];
                    for (int i = 0; i < Matisse.obtainResult(data).size(); i++) {
                        uri[i]=Matisse.obtainResult(data).get(i);
                    }
                }
            }else if (requestCode == RESULT_GALLERY_PHOTO) {
                uri = onGalleryIntentResult(data);
            } else {
                uri = onCameraIntentResult(context, data);
            }
        }

        return (uri == null) ? new Uri[0] : uri;
    }

    private static Intent getGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent = Intent.createChooser(intent, null);
        if (SUPPORT_MULTI_PICK) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }

        return intent;
    }

    private static Intent getGalleryLibraryIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent = Intent.createChooser(intent, null);
        if (SUPPORT_MULTI_PICK) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        }

        return intent;
    }

    @Nullable
    private static Intent getCameraIntent(@NonNull Context context) {

        long currentTimeMillis = System.currentTimeMillis();

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, currentTimeMillis + ".jpg");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        // Add the date meta data to ensure the image is added at the front of the gallery
        values.put(MediaStore.Images.Media.DATE_ADDED, currentTimeMillis);
        values.put(MediaStore.Images.Media.DATE_TAKEN, currentTimeMillis);

        Uri outputUri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (outputUri != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // grant Camera app permission to write on our behalf
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ClipData clip = ClipData.newUri(context.getContentResolver(), "A photo", outputUri);

                intent.setClipData(clip);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            } else {
                List<ResolveInfo> resInfoList;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_ALL);
                } else {
                    resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
                }

                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    context.grantUriPermission(packageName, outputUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
            }

            // store the uri
            context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
                    .edit()
                    .putString(KEY_IMAGE_URI, outputUri.toString())
                    .apply();
        }

        return intent;
    }

    @Nullable
    private static Uri[] onGalleryIntentResult(@Nullable Intent data) {
        Uri[] uri = null;
        if (data != null) {
            Uri itemUri = data.getData();
            if (itemUri == null) {
                if (SUPPORT_MULTI_PICK) {
                    ClipData clipdata = data.getClipData();
                    Uri[] array = new Uri[clipdata.getItemCount()];
                    for (int i = 0; i < clipdata.getItemCount(); i++) {
                        array[i] = clipdata.getItemAt(i).getUri();
                    }
                    uri = array;
                }
            } else {
                uri = new Uri[]{itemUri};
            }
        }
        return uri;
    }

    @Nullable
    private static Uri[] onCameraIntentResult(@NonNull Context context, @Nullable Intent data) {
        if (data == null || data.getData() == null) {
            String string = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
                    .getString(KEY_IMAGE_URI, null);

            if (!TextUtils.isEmpty(string)) {
                try {
                    return new Uri[]{Uri.parse(string)};
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Uri uri = data.getData();
            if (uri != null) return new Uri[]{uri};
        }

        context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
                .edit()
                .remove(KEY_IMAGE_URI)
                .apply();

        return null;
    }

    /**
     * A simple wrapper class that wraps {@link Activity} or {@link Fragment}
     * to easily access common methods
     */
    private static class StartActivityForResultWrapper {

        @Nullable
        private Activity activity;
        @Nullable
        private Fragment fragment;

        StartActivityForResultWrapper(@NonNull Activity activity) {
            this.activity = activity;
            fragment = null;
        }

        StartActivityForResultWrapper(@NonNull Fragment fragment) {
            this.fragment = fragment;
            activity = null;
        }

        void startActivityForResult(Intent intent, int requestCode) {
            if (activity != null) {
                activity.startActivityForResult(intent, requestCode);
            } else if (fragment != null) {
                fragment.startActivityForResult(intent, requestCode);
            }
        }

        @Nullable
        Context getContext() {
            if (activity != null) {
                return activity;
            } else if (fragment != null) {
                return fragment.getContext();
            }
            return null;
        }
    }

}
