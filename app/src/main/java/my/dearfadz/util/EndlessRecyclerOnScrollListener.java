package my.dearfadz.util;

import android.support.annotation.CallSuper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecyclerOnScrollListener.class.getSimpleName();

    private boolean loading = false; // True if we are still waiting for the last set of data to load.
    private int totalItemCount;
    private boolean mHasMorePages = true;
    private boolean mEnabled = true;


    private LinearLayoutManager mLayoutManager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
    }

    public EndlessRecyclerOnScrollListener(GridLayoutManager layoutManager) {
        // we can do this because GridLayoutManager extends LinearLayoutManager
        // cant work with StaggeredGridLayoutManager due to #findFirstVisibleItemPosition()
        this.mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (mHasMorePages && !loading && mEnabled
                && mLayoutManager.findLastCompletelyVisibleItemPosition() == 20) {
            totalItemCount = mLayoutManager.getItemCount();
            loading = true;
            onLoadMore();
        }
    }

    public void hasMorePages(boolean hasMorePages) {
        this.mHasMorePages = hasMorePages;
    }

    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    /**
     * Gives the chance to trigger network call and show appropriate UI
     */
    public abstract void onLoadMore();

    /**
     * Gives the chance to hide appropriate UI
     */
    @CallSuper
    public void onLoadMoreComplete() {
        loading = false;
    }

    public boolean isLoading() {
        return loading;
    }
}