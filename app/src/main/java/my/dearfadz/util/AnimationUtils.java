package my.dearfadz.util;

import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.View;
import android.view.animation.Interpolator;

public class AnimationUtils {

    private AnimationUtils() {
    }

    private static Interpolator fastOutSlowIn;
    private static Interpolator fastOutLinearIn;
    private static Interpolator linearOutSlowIn;

    /**
     * Movement within screen bounds.
     * <p>
     * Standard curve
     * <p>
     * This is the most common easing curve.
     * Objects quickly accelerate and slowly decelerate between on-screen locations.
     * It applies to growing and shrinking material, among other property changes.
     *
     * @return
     */
    public static Interpolator getFastOutSlowInInterpolator() {
        if (fastOutSlowIn == null) {
            fastOutSlowIn = new FastOutSlowInInterpolator();
        }
        return fastOutSlowIn;
    }

    /**
     * Movement slide_out_up_bottom of screen bounds.
     * <p>
     * Acceleration curve (“Easing in”)
     * <p>
     * Objects leave the screen at full velocity. They do not decelerate when off-screen.
     * <p>
     * They accelerate at the beginning of the animation and may scale down in either size (to 0%) or opacity (to 0%).
     * In some cases, when objects leave the screen at 0% opacity, they may also slightly scale up or down in size.
     *
     * @return
     */
    public static Interpolator getFastOutLinearInInterpolator() {
        if (fastOutLinearIn == null) {
            fastOutLinearIn = new FastOutLinearInInterpolator();
        }
        return fastOutLinearIn;
    }

    /**
     * Movement in of screen bounds.
     * <p>
     * Deceleration curve (“Easing slide_out_up_bottom”)
     * <p>
     * Objects slide_in_bottom_up the screen at full velocity from off-screen and slowly decelerate to a resting point.
     * <p>
     * During deceleration, objects may scale up either in size (to 100%) or opacity (to 100%).
     * In some cases, when objects slide_in_bottom_up the screen at 0% opacity, they may slightly shrink from a larger size upon entry.
     *
     * @return
     */
    public static Interpolator getLinearOutSlowInInterpolator() {
        if (linearOutSlowIn == null) {
            linearOutSlowIn = new LinearOutSlowInInterpolator();
        }
        return linearOutSlowIn;
    }

    public static Bundle generateScaleUpAnimFromView(View view) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeScaleUpAnimation(view,
                0, 0,
                view.getWidth(), view.getHeight());

        return options.toBundle();
    }

}