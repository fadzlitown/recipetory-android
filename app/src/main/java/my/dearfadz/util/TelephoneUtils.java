package my.dearfadz.util;

import android.content.Context;
import android.os.Build;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.TelephonyManager;
import android.util.Log;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by fadzlirazali on 13/01/2017.
 * http://stackoverflow.com/questions/18399364/get-signal-strength-of-wifi-and-mobile-data
*/

public class TelephoneUtils {
    public static String signalName="Unknown Signal";

    public static int getSignlaStrengthGsm(Context context){
        final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
//            for (final CellInfo info : telephonyManager.getAllCellInfo()) {
//                if (info instanceof CellInfoGsm) {
//                    final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
//                    signalName = "GSM Signal";
//                    return gsm.getLevel(); //Get signal level as an int from 0..4
//
//                } else if (info instanceof CellInfoCdma) {
//                    final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
//                    signalName = "CDMA Signal";
//                    return cdma.getLevel();
//
//                } else if (info instanceof CellInfoLte) {
//                    final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
//                    signalName = "4G LTE Signal";
//
//                    return lte.getLevel();
//
//                } else {
//                    throw new Exception("Unknown type of cell signal!");
//                }
//            }
        } catch (Exception e) {
            Log.e(TAG, "Unable to obtain cell signal information", e);
        }
//        CellInfoGsm cellinfogsm = (CellInfoGsm)telephonyManager.getAllCellInfo().get(0);
//        CellSignalStrengthGsm cellSignalStrengthGsm = cellinfogsm.getCellSignalStrength();
//        return cellSignalStrengthGsm.getDbm();
        return 0;
    }

    public String getSignalName(){
        return signalName;
    }


    public static String getPhoneManufacturer(){
        return Build.MANUFACTURER;
    }

    public static String getPhoneModel(){
        return Build.MODEL;
    }

}
