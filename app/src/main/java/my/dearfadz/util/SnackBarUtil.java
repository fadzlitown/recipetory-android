package my.dearfadz.util;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import my.fadzlirazali.recipetory.R;

public class SnackBarUtil {
    public static void displayActivity(View view, Context context, String s) {
        Snackbar snack = Snackbar.make(view.findViewById(android.R.id.content), s, Snackbar.LENGTH_LONG);
        View subView = snack.getView();
        subView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        TextView textView = (TextView) subView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setCompoundDrawablesRelative(ContextCompat.getDrawable(context, android.R.drawable.ic_dialog_alert), null, null, null);
        }
        textView.setCompoundDrawablePadding(10);
        snack.show();
    }

    public static void displayWithActionActivity(View view, Context context, String msg, String action, View.OnClickListener onClickListener, BaseTransientBottomBar.BaseCallback<Snackbar> baseCallback) {
        Snackbar.make(view, msg, BaseTransientBottomBar.LENGTH_LONG)
                .setAction(action, onClickListener)
                .addCallback(baseCallback)
                .show();
    }
}