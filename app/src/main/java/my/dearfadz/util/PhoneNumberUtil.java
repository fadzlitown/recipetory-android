package my.dearfadz.util;

public class PhoneNumberUtil {

    public static String ignoredPhoneCountryCode(String phoneNo) {
        //if starting with 0
        if (phoneNo.length() > 0 && phoneNo.substring(0, 1).equals("0"))      //if starting with 012-2345678+
            return phoneNo.substring(1, phoneNo.length());
        else if (phoneNo.length() > 10 && phoneNo.substring(0, 2).equals("60"))    //if starting with 6012-2345678+
            return phoneNo.substring(2, phoneNo.length());
        return phoneNo;
    }

    public static boolean isValidCountryPhone(int countryCode, String phoneNo) {
        switch (countryCode) {
            //malaysia, NSN length	8 to 10 eg. 01x-xxx xxxx & 011-xxxx xxxx
            //https://en.wikipedia.org/wiki/Telephone_numbers_in_Malaysia
            case 60:
                return phoneNo.length() > 7 && phoneNo.length() < 11;

            default:
                return false;
        }
    }


}
