package my.fadzlirazali.recipetory.event;

import my.fadzlirazali.recipetory.model.MyError;
import my.fadzlirazali.recipetory.model.User;

public class AccountEvent {

    public static class OnLogin {

    }

    public static class OnLoginFailed {

    }

    public static class OnFirstLogin {
        private User user;

        public OnFirstLogin(User obj) {
            this.user = obj;
        }

        public User getFirstLogin() {
            return user;
        }
    }


    public static class OnFirstLoginFailed {

        private final MyError error;

        public OnFirstLoginFailed(MyError error) {
            this.error = error;
        }

        public MyError getError() {
            return error;
        }
    }

    public static class OnFirstLoginError {

        private final MyError error;

        public OnFirstLoginError(MyError error) {
            this.error = error;
        }

        public MyError getError() {
            return error;
        }
    }

    public static class OnPasswordChanged {

    }

    public static class OnPasswordChangeFailed {

    }

    public static class OnForgotPassword {
        private final boolean status;

        public OnForgotPassword(boolean status) {
            this.status = status;
        }

        public boolean isStatus() {
            return status;
        }
    }

    public static class OnForgotPasswordFailed {

    }

    public static class OnUploadPhoto {

    }

    public static class OnUploadPhotoFailed {

    }

    public static class OnProfileUpdated {

    }

    public static class OnProfileUpdateFailed {

    }

    public static class OnGetProfile {

        private final User user;

        public OnGetProfile(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }

    public static class OnGetProfileFailed {

    }

    public static class OnUnverifiedUser {

    }

    public static class OnResendVerification {

        private final boolean status;

        public OnResendVerification(boolean result) {
            this.status = result;
        }

        public boolean isStatus() {
            return status;
        }
    }

    public static class OnResendVerificationFailed {

    }

}