package my.fadzlirazali.recipetory.event;

import my.fadzlirazali.recipetory.model.Recipe;

public class RecipeEvent {
    public static class OnNewPosted{}

    public static class OnNotifyRecipe{
        private final Recipe recipe;
        private final int pos;

        public OnNotifyRecipe(Recipe recipe, int position) {
            this.recipe=recipe;
            this.pos=position;
        }

        public Recipe getRecipe() {
            return recipe;
        }

        public int getPos() {
            return pos;
        }
    }

}
