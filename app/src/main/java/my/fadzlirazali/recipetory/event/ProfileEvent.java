package my.fadzlirazali.recipetory.event;

import my.fadzlirazali.recipetory.model.User;

public class ProfileEvent {

    public static class OnUpdate {

    }

    public static class OnUpdateFailed {

    }

    public static class OnFirstUpdate {
        private User user;

        public OnFirstUpdate(User obj) {
            this.user = obj;
        }

    }
}
