package my.fadzlirazali.recipetory.ui.addRecipe;

import android.support.annotation.NonNull;
import android.view.View;

import com.xwray.groupie.Item;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.AddIngradientItemBinding;


/**
 * Created by mohdfadzli on 11/11/2017.
 */

public class AddIngradientItem extends Item<AddIngradientItemBinding> {

    private final AddRecipeActivity.IngradientListenerItem itemListener;
    private final String title;

    public AddIngradientItem(AddRecipeActivity.IngradientListenerItem ingradientChangeListener, @NonNull String title){
        this.title = title;
        this.itemListener=ingradientChangeListener;
    }
    @Override
    public int getLayout() {
        return R.layout.add_ingradient_item;
    }

    @Override
    public void bind(AddIngradientItemBinding vb, int position) {
        vb.addBtn.setText(title);
        vb.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.onItemInserted();
            }
        });
    }


}
