package my.fadzlirazali.recipetory.ui.addRecipe;

import android.support.v4.content.ContextCompat;
import android.view.View;

import com.xwray.groupie.Item;

import java.util.ArrayList;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeTypeListItemBinding;
import my.fadzlirazali.recipetory.model.RecipeType;

/**
 * Created by mohdfadzli on 5/11/2017.
 */

public class TypeItem extends Item<RecipeTypeListItemBinding> {

    private final ArrayList<RecipeType> list;
    private  boolean isViewOnly=false;

    public TypeItem(ArrayList<RecipeType>  typeList) {
        this.list = typeList;
    }

    public TypeItem(ArrayList<RecipeType> typeList, boolean isView) {
        this.isViewOnly = isView;
        this.list = typeList;
    }

    @Override
    public int getLayout() {
        return R.layout.recipe_type_list_item;
    }

    @Override
    public void bind(final RecipeTypeListItemBinding vb, final int position) {
        final RecipeType item = list.get(position);
        vb.typeName.setText(item.getType());

        if(isViewOnly) {
            vb.typeName.setChecked(true);    //if detail page, always true
            vb.typeCard.setCardBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(),R.color.colorPrimaryDark));
            vb.typeCard.setCardElevation(35);
            vb.typeCard.invalidate();
        } else{
            vb.typeName.setChecked(item.isChecked());
            vb.typeCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    item.setChecked(!vb.typeName.isChecked());

                    vb.typeName.setChecked(!vb.typeName.isChecked());
                    if(vb.typeName.isChecked()){
                        vb.typeCard.setCardBackgroundColor(ContextCompat.getColor(v.getContext(),R.color.colorPrimaryDark));
                        vb.typeCard.setCardElevation(35);
                        vb.typeCard.invalidate();
                    } else {
                        vb.typeCard.setCardBackgroundColor(ContextCompat.getColor(v.getContext(),android.R.color.white));
                        vb.typeCard.setCardElevation(4); //def value
                        vb.typeCard.invalidate();
                    }
                }
            });
            if(vb.typeName.isChecked()){
                vb.typeCard.setCardBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(),R.color.colorPrimaryDark));
                vb.typeCard.setCardElevation(35);
                vb.typeCard.invalidate();
            } else {
                vb.typeCard.setCardBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(),android.R.color.white));
                vb.typeCard.setCardElevation(4); //def value
                vb.typeCard.invalidate();
            }
        }
    }

    public ArrayList<RecipeType> getList(){
        return list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
