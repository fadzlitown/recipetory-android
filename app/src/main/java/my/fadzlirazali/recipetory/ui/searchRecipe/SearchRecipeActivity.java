package my.fadzlirazali.recipetory.ui.searchRecipe;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.SearchView;

import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.EndlessRecyclerOnScrollListener;
import my.fadzlirazali.recipetory.MyApplication;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivitySearchRecipeBinding;
import my.fadzlirazali.recipetory.job.SearchRecipeJob;
import my.fadzlirazali.recipetory.model.Hits;
import my.fadzlirazali.recipetory.model.IndexElastic;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.ui.BaseActivity;
import my.fadzlirazali.recipetory.ui.main.FeedRecipeHeader;
import my.fadzlirazali.recipetory.ui.main.FeedRecipeItem;

public class SearchRecipeActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    private static final String HAS_SEARCHED_VALUE = "search_value";
    private ActivitySearchRecipeBinding binding;

    public static String[] columns = new String[]{"_id", "RECIPE_NAME"};
    private SearchRecipeAdapter mSearchAdapter;
    private OnEndlessOnScrollListener endlessListener;
    private GroupAdapter mSearchGroupAdapter;
    private ArrayList<Recipe> recipeDB;


    public static void start(Context context, String search) {
        Intent starter = new Intent(context, SearchRecipeActivity.class);
        starter.putExtra(HAS_SEARCHED_VALUE, search);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search_recipe);
        initUI();
    }

    private void initUI() {
        SearchManager searchManager = ((SearchManager) getSystemService(Context.SEARCH_SERVICE));
        binding.searchViewToolbar.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        binding.searchViewToolbar.setIconifiedByDefault(false);
        binding.searchViewToolbar.setQueryHint("nak masak apa tu?");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.searchViewToolbar.setNestedScrollingEnabled(true);
        }
        binding.searchViewToolbar.setOnQueryTextListener(this);
        mSearchAdapter = new SearchRecipeAdapter(this, R.layout.search_recipe_list_item, null,columns , null,-1000);
        binding.searchViewToolbar.setSuggestionsAdapter(mSearchAdapter);

        recipeDB = new ArrayList<>();
        endlessListener = new SearchRecipeActivity.OnEndlessOnScrollListener((LinearLayoutManager) binding.rv.getLayoutManager());
        mSearchGroupAdapter = new GroupAdapter();
        binding.rv.addOnScrollListener(endlessListener);
        binding.rv.setAdapter(mSearchGroupAdapter);

        endlessListener.setEnabled(true);
//        endlessListener.hasMorePages(list.size() >= TAKE_LIMIT);
        endlessListener.onLoadMoreComplete();


        if(getIntent().hasExtra(HAS_SEARCHED_VALUE)){
            binding.searchViewToolbar.setQuery(getIntent().getStringExtra(HAS_SEARCHED_VALUE),true);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if(query.length()>2)
            loadData(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        return false;
    }

    public String mQuery;
    private void loadData(final String query) {
        timer.cancel();
        timer.start();
        this.mQuery=query;
    }

    //https://stackoverflow.com/questions/48088508/searchview-onquerytextchange-ignore-previous-characters-and-only-use-last-typed
    CountDownTimer timer = new CountDownTimer(1000,500) {
        @Override
        public void onTick(long millisUntilFinished) {}

        @Override
        public void onFinish() {
            //call network
            callSearchFirebase(mQuery);
        }
    };

    private void callSearchFirebase(String mQuery) {
        if(recipeDB.size()>0){
            recipeDB.clear();
        }
        MyApplication.addJobInBackground(new SearchRecipeJob(mQuery));

//        FirebaseDatabase.getInstance().getReference().child("recipe").getRef().orderByChild("title")
//                .startAt(mQuery)
//                .endAt(mQuery+"\uf8ff")
//                //.equalTo(mQuery)
//                .addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if(mSearchGroupAdapter.getItemCount()>=1){      //always get latest recipes
//                    mSearchGroupAdapter.remove(mSearchGroupAdapter.getItem(1));
//                    mSearchGroupAdapter.notifyItemRemoved(1);
//                }
//
//                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
//                    //Mapping data dari snapshot ke dlm object Recipe kita, & simpan primary key pd object recipe untuk handle edit & delete data
//                    Recipe recipe = snapshot.getValue(Recipe.class);
//                    if(recipe!=null) recipe.setId(snapshot.getKey());
//                    recipeDB.add(recipe);
//                }
//
//                List<Item> itemList = new ArrayList<>();
//                itemList.add(new FeedRecipeHeader());
//                itemList.add(new FeedRecipeItem(recipeDB));
//                mSearchGroupAdapter.addAll(itemList);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(IndexElastic elastic) {


                if(elastic!=null && elastic.getHits().getHits().size()>0){
                    List<Hits> hitsList = elastic.getHits().getHits();
                    for (int i = 0; i <hitsList.size() ; i++) {
                        Hits hit = hitsList.get(i);
                        if(hit.get_source()!=null){
                            Recipe recipe = hit.get_source();
                            recipe.setId(hit.get_id());
                            recipeDB.add(recipe);
                        }
                    }
                }
                List<Item> itemList = new ArrayList<>();
                itemList.add(new FeedRecipeHeader());
                itemList.add(new FeedRecipeItem(recipeDB));

                if(mSearchGroupAdapter.getItemCount()==0){      //always get latest recipes
                    mSearchGroupAdapter.addAll(itemList);
                } else {
                    mSearchGroupAdapter.notifyDataSetChanged();
                }

    }


    private class OnEndlessOnScrollListener extends EndlessRecyclerOnScrollListener {

        public OnEndlessOnScrollListener(LinearLayoutManager llm) {
            super(llm);
        }

        @Override
        public void onLoadMore() {
            if(!isLoading()){
                /*                isLoading = true;
                //TODO
                SKIP = groupAdapter.getItemCount(); - itemHeader
                callUpcomingJob(SKIP, TAKE);*/
//                groupAdapter.add(loadingItem);
            }
        }

        @Override
        public void onLoadMoreComplete() {
            super.onLoadMoreComplete();
            //hide progress bar
        }
    }


}
