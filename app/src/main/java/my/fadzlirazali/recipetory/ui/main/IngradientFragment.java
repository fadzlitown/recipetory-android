package my.fadzlirazali.recipetory.ui.main;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;
import my.dearfadz.widget.SpacesItemDecoration;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.FragmentIngradientBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class IngradientFragment extends Fragment {

    private int [][] ingDrawables = { //int[13][2]
        { R.drawable.beef_ing_inactive,R.drawable.beef_ing_active},
        { R.drawable.bread_ing_inactive, R.drawable.bread_ing_active},
        { R.drawable.chicken_ing_inactive,R.drawable.chicken_ing_active},
        { R.drawable.chilli_ing_inactive, R.drawable.chilli_ing_active},
        { R.drawable.egg_ing_inactive,R.drawable.egg_ing_active},
        { R.drawable.fish_ing_inactive,R.drawable.fish_ing_active},
        { R.drawable.fruit_ing_inactive, R.drawable.fruit_ing_active},
        { R.drawable.herbs_ing_inactive,R.drawable.herbs_ing_active},
        { R.drawable.noodle_ing_inactive,R.drawable.noodle_ing_active},
        { R.drawable.rice_ing_inactive,R.drawable.rice_ing_active},
        { R.drawable.peanut_ing_inactive,R.drawable.peanut_ing_active},
        { R.drawable.salad_ing_inactive,R.drawable.salad_ing_inactive},
        { R.drawable.soup_ing_inactive,R.drawable.soup_ing_active},
        { R.drawable.spice_ing_inactive,R.drawable.spice_ing_active}};

    private String [] ingTitles = new String[]{"Beef","Bread","Chicken", "Chili", "Egg","Fish","Fruit","Herbs","Noodle","Rice","Peanut","Salad","Soup","Spice"};

    /** TODO
     * Replace above with this
     * https://stackoverflow.com/questions/6945678/storing-r-drawable-ids-in-xml-array?noredirect=1&lq=1
     * */
    public IngradientFragment() {
        // Required empty public constructor
    }

    public static IngradientFragment newInstance() {
        return  new IngradientFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentIngradientBinding binding = DataBindingUtil.inflate(inflater,R.layout.fragment_ingradient, container, false);
        View v = binding.getRoot().getRootView();

        GroupAdapter groupAdapter = new GroupAdapter();
        binding.rv.addItemDecoration(new SpacesItemDecoration(Util.dpToPx(getContext(),1)));
        binding.rv.setAdapter(groupAdapter);
        binding.rv.setItemViewCacheSize(ingDrawables.length);

        List<Item> itemList = new ArrayList<>();
        itemList.add(new IngradientItem(ingDrawables,ingTitles));
        groupAdapter.addAll(itemList);
        return v;
    }

}
