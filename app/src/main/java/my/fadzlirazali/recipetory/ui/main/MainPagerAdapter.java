package my.fadzlirazali.recipetory.ui.main;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohdfadzli on 23/10/2017.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {

    private final String[] titles;
    private  List<Fragment> fragmentList = new ArrayList<>();

    public MainPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragmentList) {
        super(fm);
        this.fragmentList=fragmentList;
        titles=null;
    }

    public MainPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragmentList, @NonNull String [] titles) {
        super(fm);
        this.fragmentList=fragmentList;
        this.titles=titles;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }


    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(titles!=null && titles.length>0)
            return titles[position];
        else return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
