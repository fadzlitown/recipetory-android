package my.fadzlirazali.recipetory.ui.addRecipe;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.xwray.groupie.Section;
import com.xwray.groupie.TouchCallback;

import my.fadzlirazali.recipetory.R;

public abstract class SwipeTouchCallback extends TouchCallback {

    private final Paint paint;
    @ColorInt
    private final int backgroundColor;
    private  Section groupAdapter2;
    private boolean isDragged=false;

    public SwipeTouchCallback(@ColorInt int backgroundColor) {
        super();
        this.backgroundColor = backgroundColor;
        paint = new Paint();
    }

    public void setCurrentAdapter(@NonNull Section adapter){
        this.groupAdapter2=adapter;
    }

    public boolean isDragged(){
        return isDragged;
    }

    @Override
    public int getBoundingBoxMargin() {
        return super.getBoundingBoxMargin();
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return super.isLongPressDragEnabled();
    }

    //https://stackoverflow.com/questions/35920584/android-how-to-catch-drop-action-of-itemtouchhelper-which-is-used-with-recycle
    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        Log.d("SwipeTouchCallback", "onSelectedChanged actionState: "+actionState);
        if (groupAdapter2!=null && actionState == ItemTouchHelper.ACTION_STATE_IDLE) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    groupAdapter2.notifyChanged();
                }
            });
        }
    }

    @Override
    public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder current, RecyclerView.ViewHolder target) {
        return super.canDropOver(recyclerView, current, target);
    }


    @Override public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View child = viewHolder.itemView;
        Log.d("SwipeTouchCallback", "onChildDraw actionState: "+actionState+" isCurrentlyActive: "+isCurrentlyActive);
        ImageView icon = ((ImageView) child.findViewById(R.id.delete_btn));
        if (ItemTouchHelper.ACTION_STATE_DRAG == actionState && isCurrentlyActive) {
            child.setBackgroundColor(ContextCompat.getColor(child.getContext(), R.color.lightGray));
//            RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();
            icon.setVisibility(View.GONE);
            // Fade out the item
//            child.setAlpha(1 - (Math.abs(dX) / (float) child.getWidth()));
//
//            // Draw gray "behind" the item, if it's being moved horizontally (swiped)
//            paint.setColor(backgroundColor);
//            if (dX > 0) {
//                c.drawRect(
//                        lm.getDecoratedLeft(child),
//                        lm.getDecoratedTop(child),
//                        lm.getDecoratedLeft(child) + dX,
//                        lm.getDecoratedBottom(child), paint);
//            } else if (dX < 0) {
//                c.drawRect(
//                        lm.getDecoratedRight(child) + dX,
//                        lm.getDecoratedTop(child),
//                        lm.getDecoratedRight(child),
//                        lm.getDecoratedBottom(child), paint);
//            }
        } else {
            icon.setVisibility(View.VISIBLE);
            child.setBackgroundColor(ContextCompat.getColor(child.getContext(), R.color.white));
            child.invalidate();
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}