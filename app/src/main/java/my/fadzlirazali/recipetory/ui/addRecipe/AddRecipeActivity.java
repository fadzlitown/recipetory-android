package my.fadzlirazali.recipetory.ui.addRecipe;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;
import com.xwray.groupie.Section;
import com.xwray.groupie.TouchCallback;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.ImageUtils;
import my.dearfadz.util.PhotoPickerHelper;
import my.dearfadz.util.Util;
import my.dearfadz.widget.SpacesItemDecoration;
import my.fadzlirazali.recipetory.MyApplication;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityAddRecipeBinding;
import my.fadzlirazali.recipetory.event.RecipeEvent;
import my.fadzlirazali.recipetory.model.Ingradient;
import my.fadzlirazali.recipetory.model.Method;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.model.RecipeType;
import my.fadzlirazali.recipetory.ui.BaseActivity;

public class AddRecipeActivity extends BaseActivity {

    private static final String JSON_UPDATE = "json_update";
    private static final String JSON_ID = "json_id";
    private static final String TAG = AddRecipeActivity.class.getSimpleName();

    private ActivityAddRecipeBinding binding;
    private GroupAdapter groupAdapter;
    public GroupAdapter recipeAdapter;
    private int darkGreen;
    private Section ingSection;
    private RecipeIngradientItem2 ingItems;
    private RecipeMethodItem methodItems;
    public PhotoPagerAdapter photoPagerAdapter;
    private Section methodSection;
    private TypeItem typeList;
    private boolean isUpdateRecipe=false;
    private Recipe mRecipe;
    private long mRecipeId;
    private DatabaseReference db;
    private StorageReference storageRef;
    private ArrayList<String> selectedImageUrls;


    public static void start(Context context) {
        Intent intent = new Intent(context, AddRecipeActivity.class);
//        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        context.startActivity(intent);
    }

    public static void update(Context context,long recipeId,String json) {
        Intent intent = new Intent(context, AddRecipeActivity.class);
        intent.putExtra(JSON_ID,recipeId);
        intent.putExtra(JSON_UPDATE,json);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_recipe);
        setToolbar();
        setCollapsingToolbar();
        initFirebase();

        /** Check if a new or update recipe with basic info */
        if(getIntent().hasExtra(JSON_UPDATE) && getIntent().hasExtra(JSON_ID)){
            isUpdateRecipe = true;
            mRecipeId = getIntent().getLongExtra(JSON_ID,0);
            mRecipe = Recipe.fromJson(getIntent().getStringExtra(JSON_UPDATE));
            binding.recipeTitle.setText(mRecipe.getTitle());
            binding.recipeDesc.setText(mRecipe.getDesc());
            binding.servings.setText(mRecipe.getServing()+" people");
            binding.duration.setText(mRecipe.getDuration());
            binding.cuisine.setText(mRecipe.getCuisine());
        } else {
            isUpdateRecipe = false;
        }

        mSelectedImages = new ArrayList<>();
        photoPagerAdapter = new PhotoPagerAdapter(this);
        binding.photoPager.setAdapter(photoPagerAdapter);
        binding.indicator.setViewPager(binding.photoPager);
        final float density = getResources().getDisplayMetrics().density;
        binding.indicator.setRadius(5 * density); //Set circle indicator radius


        /** Updated Photo Recipe*/
        if(isUpdateRecipe && mRecipe.getUriList().size()>0){
            for (int i = 0; i < mRecipe.getUriList().size(); i++) {
                mSelectedImages.add(Uri.parse(mRecipe.getUriList().get(i)));
            }
            addPhotoPager();
        }


        darkGreen = ContextCompat.getColor(this, R.color.darkGreen);

        String [] typeArr = getResources().getStringArray(R.array.IngredientType);
        groupAdapter = new GroupAdapter();
        binding.typeRV.addItemDecoration(new SpacesItemDecoration(Util.dpToPx(this,1)));
        binding.typeRV.setAdapter(groupAdapter);
        binding.typeRV.setItemViewCacheSize(typeArr.length);  //Number of views to cache offscreen before returning them to the general recycled view pool

        ArrayList<RecipeType> recipeTypes = new ArrayList<>();
        for (int i = 0; i < typeArr.length; i++) {
            RecipeType rt = new RecipeType();
            /** Updated Type Recipe*/
            rt.setChecked((isUpdateRecipe && mRecipe.getTypeList().get(i) != null) && mRecipe.getTypeList().get(i).isChecked());
            rt.setId(String.valueOf(i));
            rt.setType(typeArr[i]);
            recipeTypes.add(rt);
        }


        List<Item> recipeTypeList = new ArrayList<>();
        typeList = new TypeItem(recipeTypes);
        recipeTypeList.add(typeList);
        groupAdapter.addAll(recipeTypeList);


        recipeAdapter = new GroupAdapter();
        binding.addRecipeRv.setAdapter(recipeAdapter);

        ArrayList<Ingradient> ingList = new ArrayList<>();
        Ingradient ingradient1 = new Ingradient("","");
        Ingradient ingradient2 = new Ingradient("","");
        ingList.add(ingradient1);
        ingList.add(ingradient2);

        /** Updated Recipe's Ingradient*/
        if(isUpdateRecipe && mRecipe.getIngredient().size()>0) {
            ingList.clear();
            for (Ingradient item: mRecipe.getIngredient()) {
                ingList.add( new Ingradient(item.getId(),item.getIngradientName()));
            }
        }

        ingSection = new Section(new AddRecipeHeader("ingredient"));
//        ingSection = new Section();
        ingItems = new RecipeIngradientItem2(ingList,ingradientChangeListener );
        ingSection.add(ingItems);
        ingSection.setFooter(new AddIngradientItem(ingradientChangeListener, "INGREDIENT"));
        recipeAdapter.add(ingSection);


        ArrayList<Method> methodList = new ArrayList<>();
        Method method = new Method("","");
        methodList.add(method);

        /** Updated Recipe's Ingradient*/
        if(isUpdateRecipe && mRecipe.getMethod().size()>0) {
            methodList.clear();
            for (Method item: mRecipe.getMethod()) {
                methodList.add( new Method(item.getId(),item.getMethodName()));
            }
        }

        methodSection = new Section(new AddRecipeHeader("METHOD"));
//        ingSection = new Section();
        methodItems = new RecipeMethodItem(methodList,methodListener);
        methodSection.add(methodItems);
        methodSection.setFooter(new AddIngradientItem(methodListener, "METHOD"));
        recipeAdapter.add(methodSection);


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(touchCallback);
        itemTouchHelper.attachToRecyclerView(binding.addRecipeRv);

        binding.uploadMsgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onUploadPhoto();
            }
        });

        binding.postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.isListShown(binding.scrollView,binding.progress,false,true);
                if(mSelectedImages.size()>0) uploadPhotoInStorage();
                else {
                   final String recipeKey = db.child("recipe").push().getKey();
                   final Recipe recipe = postRecipeFirebase();

                   final Recipe selfRecipe = new Recipe();
                   selfRecipe.setTitle(recipe.getTitle());

                    db.child("recipe").child(recipeKey).setValue(recipe).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                db.child("self").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(recipeKey).setValue(selfRecipe).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(AddRecipeActivity.this, "Posted!", Toast.LENGTH_SHORT).show();
                                        Util.isListShown(binding.scrollView,binding.progress,true,true);
                                        finish();
                                        EventBus.getDefault().postSticky(new RecipeEvent.OnNewPosted());
                                    }
                                });

                            }
                        }
                    });
                }
            }
        });
    }

    private void uploadPhotoInStorage() {
        selectedImageUrls = new ArrayList<String>();
//        File path = Environment.getExternalStorageDirectory();

        for (int i = 0; i < mSelectedImages.size(); i++) {
//            final File file = new File(ImageUtils.getIndividualCacheDirectory(this), System.currentTimeMillis()+".jpg");
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File resizedFile = null;
            if(!TextUtils.isEmpty(ImageUtils.getUriPath(this, mSelectedImages.get(i)))){
                resizedFile= ImageUtils.saveImageToFile(this, new File(ImageUtils.getUriPath(this, mSelectedImages.get(i))));
            }
            Uri uri = Uri.fromFile(resizedFile);
            // Create the file metadata
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("image/*")
                    .build();

            UploadTask fileRef = storageRef.child("images/recipes/"+SystemClock.currentThreadTimeMillis()+".jpg").putFile(uri,metadata);
            Log.d(TAG, "uploadPhotoInStorage: "+mSelectedImages.get(i).getPath());    //getting-> /storageRef/emulated/0/external/images/media/1769

            final String recipeKey = db.child("recipe").push().getKey();
            final Recipe recipe = postRecipeFirebase();
            fileRef.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return task.getResult().getStorage().getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();

                        if(downloadUri!=null) selectedImageUrls.add(downloadUri.toString());

                        /** If photo has been uploaded */
                        if(mSelectedImages.size()==selectedImageUrls.size()){

                            final Recipe selfRecipe = new Recipe();
                            if(selectedImageUrls!=null && selectedImageUrls.size()>0) selfRecipe.setSmallPhoto(selectedImageUrls.get(0));
                            selfRecipe.setTitle(recipe.getTitle());
                            db.child("recipe").child(recipeKey).setValue(recipe).addOnSuccessListener(AddRecipeActivity.this, new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    db.child("self").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(recipeKey).setValue(selfRecipe).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(AddRecipeActivity.this, "Posted!", Toast.LENGTH_SHORT).show();
                                            Util.isListShown(binding.scrollView,binding.progress,true,true);
                                            finish();
                                            EventBus.getDefault().postSticky(new RecipeEvent.OnNewPosted());
                                        }
                                    });
                                }
                            });
                        }

                    } else {
                        selectedImageUrls.add("");
                        /** If photo hasn't been uploaded */
                        if(mSelectedImages.size()==selectedImageUrls.size()){
                            db.child("recipe").child(recipeKey).setValue(recipe).addOnSuccessListener(AddRecipeActivity.this, new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    final Recipe selfRecipe = new Recipe();
                                    selfRecipe.setTitle(recipe.getTitle());
                                    db.child("self").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(recipeKey).setValue(selfRecipe).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(AddRecipeActivity.this, "Completed", Toast.LENGTH_SHORT).show();
                                            Util.isListShown(binding.scrollView,binding.progress,true,true);
                                            finish();
                                        }
                                    });
                                    Toast.makeText(AddRecipeActivity.this, "Your recipe has been posted", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            });
        }
    }

    private void initFirebase() {
        db = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();
    }

    public int ingSectionSize(){
          return ingSection.getItemCount();
    }

    public void onServing(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.serving));
        onAlertDialogList(adapter, "Select your serving", binding.servings, true);
    }

    public void onAlertDialogList(final ArrayAdapter<String> arrayAdapter, String title, final TextView textView, final boolean isCountry) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String item = arrayAdapter.getItem(which);
                textView.setText(item);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void onCuisine(View view) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.cuisine));
        onAlertDialogList(adapter, "Select your cuisine", binding.cuisine, true);
    }

    public void onUploadPhoto() {
        if (PhotoPickerHelper.hasRequiredCameraPermission(this) && PhotoPickerHelper.hasRequiredGalleryPermission(this)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            PhotoPickerHelper.getPickerDialog(this).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 23);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (PhotoPickerHelper.hasRequiredCameraPermission(this) && PhotoPickerHelper.hasRequiredGalleryPermission(this)) {
            onUploadPhoto();
        }
    }

    // let stored in memory, to avoid null from Library Activity result
   public static ArrayList<Uri> mSelectedImages;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri[] result = PhotoPickerHelper.onActivityResult(this, requestCode, resultCode, data);

            if(PhotoPickerHelper.isPhotoPickerResult(requestCode) && result.length>0){
                for (int i = 0; i < result.length; i++) {
                    mSelectedImages.add(result[i]);
                }
                addPhotoPager();
                Log.d("Matisse", "Gal mSelected: " + mSelectedImages.size());
            }
        }
    }

    public void addPhotoPager(){
        photoPagerAdapter.addAll(mSelectedImages);
        photoPagerAdapter.notifyDataSetChanged();
        binding.photoPager.setCurrentItem(photoPagerAdapter.getCount());        //always move to latest uploaded photo
    }

    public void OnPostRecipe(View view) {

    }


    public interface IngradientListenerItem {
       void onItemChanged(int no);
        void onItemInserted();
        void onItemRemoved(int no);
    }

    IngradientListenerItem ingradientChangeListener = new IngradientListenerItem() {

        @Override
        public void onItemChanged(int no) {
            recipeAdapter.onItemChanged(ingItems,no);
        }

        @Override
        public void onItemInserted() {
//            recipeAdapter.onItemInserted(ingItems,no);
            ingItems.getList().add(new Ingradient("",""));

            ingSection.onItemRangeInserted(ingItems, ingItems.getList().size(),1);
            ingSection.notifyItemChanged(ingItems.getList().size()-1, ingItems); /** after added key has null val plss check */

//            recipeAdapter.setSpanCount(ingItems.getList().size());
        }

        @Override
        public void onItemRemoved(int no) {
            ingSection.onItemRemoved(ingItems,no);
            ingSection.notifyItemChanged(no, ingItems);

            ingSection.notifyChanged();
//            recipeAdapter.notifyDataSetChanged();

        }
    };

    IngradientListenerItem methodListener = new IngradientListenerItem() {

        @Override
        public void onItemChanged(int no) {
//            recipeAdapter.onItemChanged(methodItems,no);
        }

        @Override
        public void onItemInserted() {
//            recipeAdapter.onItemInserted(ingItems,no);
            methodItems.getList().add(new Method("",""));

            methodSection.onItemRangeInserted(methodItems, methodItems.getList().size(),1);
            methodSection.notifyItemChanged(methodItems.getList().size(),methodItems);
//
//            Item item = recipeAdapter.getItem(methodItems.getItemCount());
//            item.notifyChanged();
//            recipeAdapter.setSpanCount(methodItems.getList().size());
        }

        @Override
        public void onItemRemoved(int no) {
            methodSection.onItemRemoved(methodItems,no);
            methodSection.notifyItemChanged(no,methodItems);

            methodSection.notifyChanged();
//            recipeAdapter.notifyDataSetChanged();
        }
    };


    private TouchCallback touchCallback = new SwipeTouchCallback(darkGreen) {

        public boolean isAllowedToDrop=true;

        @Override public int getDragDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            Log.d("ingredient", "getDragDirs");
            recyclerView.clearFocus();
            return super.getDragDirs(recyclerView, viewHolder);
        }

        @Override public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder current, RecyclerView.ViewHolder target) {
            Log.d("ingredient", "canDropOver isAllowedToDrop"+isAllowedToDrop);
            return super.canDropOver(recyclerView, current, target);
        }

        @Override
        public RecyclerView.ViewHolder chooseDropTarget(RecyclerView.ViewHolder selected, List<RecyclerView.ViewHolder> dropTargets, int curX, int curY) {
            Log.d("ingredient", "chooseDropTarget: curX"+curX+" curY"+curY);
            isAllowedToDrop = curY >= -90;
            return super.chooseDropTarget(selected, dropTargets, curX, curY);
        }

        @Override public void onMoved(RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final int fromPos, final RecyclerView.ViewHolder target, final int toPos, int x, int y) {
            Log.d("ingredient", "onMoved: toPos"+toPos+" cur"+viewHolder.getAdapterPosition()+" target"+target.getAdapterPosition());
            isAllowedToDrop=true;
            Item item = recipeAdapter.getItem(viewHolder.getAdapterPosition());

            if(item instanceof RecipeIngradientItem2) {
                if (ingSection != null)
                    setCurrentAdapter(ingSection);
            } else if(item instanceof RecipeMethodItem){
                if (methodSection != null)
                    setCurrentAdapter(methodSection);
            }
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
        }

        @Override public int interpolateOutOfBoundsScroll(RecyclerView recyclerView, int viewSize, int viewSizeOutOfBounds, int totalSize, long msSinceStartScroll) {
            Log.d("ingredient", "interpolateOutOfBoundsScroll: viewSize "+viewSize+" totalSize "+totalSize+" viewSizeOutOfBounds "+viewSizeOutOfBounds+" msSinceStartScroll "+msSinceStartScroll+" "+isAllowedToDrop);
            return super.interpolateOutOfBoundsScroll(recyclerView, viewSize, viewSizeOutOfBounds, totalSize, msSinceStartScroll) ;
        }

        @Override
        public int getBoundingBoxMargin() {
            return super.getBoundingBoxMargin();
        }


        @Override public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            Item item = recipeAdapter.getItem(viewHolder.getAdapterPosition());
            Log.d("ingredient", "onMove "+viewHolder.getAdapterPosition()+" "+target.getAdapterPosition());
            Log.d("ingredient", "getBoundingBoxMargin "+getBoundingBoxMargin());

            if(isAllowedToDrop && canDropOver(recyclerView,viewHolder,target) && target.getAdapterPosition()!=0){
                if(item instanceof RecipeIngradientItem2) {
                    ingSection.notifyItemMoved(viewHolder.getAdapterPosition(),target.getAdapterPosition());

                    if(target.getAdapterPosition()<=item.getItemCount() && viewHolder.getAdapterPosition()<=item.getItemCount()){
                        ingItems.move(viewHolder.getAdapterPosition()-1,target.getAdapterPosition()-1);
                    }
                }
                if(item instanceof RecipeMethodItem){
                    int fromPos = viewHolder.getAdapterPosition() - (ingSection.getItemCount() + 1);
                    int toPos = target.getAdapterPosition() - (ingSection.getItemCount() + 1);
                    methodSection.notifyItemMoved(viewHolder.getAdapterPosition()-ingSection.getItemCount(), target.getAdapterPosition()-ingSection.getItemCount());
                    if(target.getAdapterPosition()-ingSection.getItemCount()<=item.getItemCount() && viewHolder.getAdapterPosition()-ingSection.getItemCount()<=item.getItemCount() && fromPos>=0 && toPos>=0) {
                        methodItems.move(AddRecipeActivity.this, fromPos,toPos);
                    }
                }

                viewHolder.itemView.clearFocus();
                target.itemView.clearFocus();
                Log.d("ingredient", "true");
                return true;
            } else {
                Log.d("ingredient", "false"); //if 2times false just abort this function by break or disable on top
                isAllowedToDrop=true;
                return false;
            }
        }
        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {}
    };


    @Override
    public int getRequestedOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    private void setCollapsingToolbar() {
        binding.collapsingToolbar.setTitleEnabled(false);
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setElevation(200);

            TextView textView = new TextView(this);
            textView.setText("What's your recipe?");
            textView.setTextSize(20);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setGravity(Gravity.CENTER);
            textView.setTextColor(ContextCompat.getColor(this,R.color.black));
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(textView);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        } else if(item.getItemId()==R.id.action_delete){
            finish();
        }else if(item.getItemId()==R.id.action_save){
            onSaveRecipe();
        }
        return super.onOptionsItemSelected(item);
    }

    public Recipe savedRecipeDoa(){
        ArrayList<String> uriString = new ArrayList<>();
        for (int i = 0; i < mSelectedImages.size(); i++) {
            uriString.add(mSelectedImages.get(i).toString());
        }
        return new Recipe( FirebaseAuth.getInstance().getCurrentUser()!=null ? FirebaseAuth.getInstance().getCurrentUser().getUid() : "" ,binding.recipeTitle.getText().toString(),binding.recipeDesc.getText().toString(), binding.servings.getText().toString(),
                binding.duration.getText().toString(), binding.cuisine.getText().toString(), typeList.getList(),uriString, selectedImageUrls ,ingItems.getList(),methodItems.getList());
    }

    public Recipe postRecipeFirebase(){
        ArrayList<String> uriString = new ArrayList<>();
        for (int i = 0; i < mSelectedImages.size(); i++) {
            uriString.add(mSelectedImages.get(i).toString());
        }
        String typeCheckedStr = "";
        for (int i = 0; i < typeList.getList().size(); i++) {
            if(typeList.getList().get(i).isChecked())
                typeCheckedStr=typeCheckedStr+String.valueOf(i)+" ";
        }

        return new Recipe(FirebaseAuth.getInstance().getCurrentUser()!=null ? FirebaseAuth.getInstance().getCurrentUser().getUid() : "" ,
                FirebaseAuth.getInstance().getCurrentUser().getDisplayName(),
                FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl()!=null ? FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString() : "",
                -DateTime.now().getMillis(),
                binding.recipeTitle.getText().toString(),binding.recipeDesc.getText().toString(), binding.servings.getText().toString(),
                binding.duration.getText().toString(), binding.cuisine.getText().toString(), typeCheckedStr ,uriString, selectedImageUrls ,ingItems.getList(),methodItems.getList(), 0,0,0);
    }

    private void onSaveRecipe() {
        Recipe recipe = savedRecipeDoa();

        my.fadzlirazali.recipetory.orm.Recipe recipeOrm = new my.fadzlirazali.recipetory.orm.Recipe();
        recipeOrm.setRecipeJson(recipe.toJson());

        if(isUpdateRecipe && mRecipeId!=0){
            recipeOrm.setId(mRecipeId);
            MyApplication.getInstance().getDaoSession().getRecipeDao().update(recipeOrm);
        } else MyApplication.getInstance().getDaoSession().getRecipeDao().insert(recipeOrm);

        if(recipeOrm.getId()!=null && recipeOrm.getId()>0) finish();
        Log.d("DaoExample", "Inserted new recipe, ID: " + recipeOrm.getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail,menu);
        return true;
    }


}
