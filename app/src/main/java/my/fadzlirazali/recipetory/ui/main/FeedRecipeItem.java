package my.fadzlirazali.recipetory.ui.main;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.xwray.groupie.Item;

import org.joda.time.DateTime;

import java.util.ArrayList;

import my.dearfadz.util.CircleTransform;
import my.dearfadz.util.DateTimeUtil;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeListItemBinding;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.ui.RecipeDetailActivity;

/**
 * Created by mohdfadzli on 2/11/2017.
 */

public class FeedRecipeItem extends Item<RecipeListItemBinding> {

    private final ArrayList<Recipe> list;

    public FeedRecipeItem(ArrayList<Recipe> recipes){
        this.list=recipes;
    }

    @Override
    public int getLayout() {
        return R.layout.recipe_list_item;
    }

    @Override
    public void bind(final RecipeListItemBinding vb, int position) {
        //if(position>=list.size()) return;

        final int positionExcludedHeader = position - 1;
        final Recipe recipe = list.get(positionExcludedHeader);

        //clear views
        vb.loveCheckableTv.setChecked(false);
        vb.recipePhoto.setImageResource(R.color.black);

        //vb.likeLayout.setVisibility(recipe.getLikes()>0 ? View.VISIBLE : View.GONE);
        final String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //NETWORK check is liked
        FirebaseDatabase.getInstance().getReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("likes")){
                    if(dataSnapshot.child("likes").hasChild(UID)){
                        if(dataSnapshot.child("likes").child(UID).hasChild(recipe.getId())){
                            FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    vb.loveCheckableTv.setChecked(dataSnapshot.exists());
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } else {
                            vb.loveCheckableTv.setChecked(false);
                        }
                    } else {
                        vb.loveCheckableTv.setChecked(false);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        vb.loveCheckableTv.setText(recipe.getLikes()>0? String.valueOf(recipe.getLikes()): "0");
        vb.loveCheckableTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(vb.loveCheckableTv.getText().toString());
                vb.loveCheckableTv.setChecked(!vb.loveCheckableTv.isChecked());

                if(vb.loveCheckableTv.isChecked()){
                    count=++count;
                    FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).setValue(true);
                } else{
//                    int decrement = --count;
//                    if(decrement<0) count=0;
//                    else count=decrement;
                    count=--count;
                    FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).removeValue();
                }

                vb.loveCheckableTv.setText(String.valueOf(count>0? count : 0));

                final int countPost=count;
                recipe.setLikes(countPost>0? (long)countPost : 0);
                FirebaseDatabase.getInstance().getReference().child("recipe").child(recipe.getId()).child("likes").setValue(countPost>0? countPost : 0).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(vb.getRoot().getContext(), "Liked / unliked", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        vb.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecipeDetailActivity.start(v.getContext(),recipe, vb.recipePhoto,vb.name,positionExcludedHeader);
            }
        });

        if(!TextUtils.isEmpty(recipe.getTitle())) vb.name.setText(recipe.getTitle());
        if(!TextUtils.isEmpty(recipe.getDesc())) vb.userDetail.setText(recipe.getDesc());
        if(!TextUtils.isEmpty(recipe.getDuration())) vb.timePosted.setText(DateTimeUtil.recipePostedTime(new DateTime(Math.abs(recipe.getCreatedBy()))));

            Glide.with(vb.getRoot().getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getPathList()!=null && recipe.getPathList().size()>0
                            ? recipe.getPathList().get(0) : ContextCompat.getColor(vb.getRoot().getContext(),R.color.black))
                    .centerCrop()
                    .error(R.color.black)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(vb.recipePhoto);

            vb.recipePhoto.invalidate();
            //vb.recipePhoto.setBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(),R.color.black));


        //solved duplicate type ui
        if(vb.typeLayout.getChildCount()>0){
            vb.typeLayout.removeAllViews();
        }

        if(recipe.getTypeChecked()!=null && vb.typeLayout.getChildCount()==0){
            String [] typeArr = vb.getRoot().getContext().getResources().getStringArray(R.array.IngredientType);
            for (int i = 0; i < typeArr.length; i++) {
                if(recipe.getTypeChecked().contains(String.valueOf(i)))    //if string contain the id
                    addedTypeView(vb.getRoot().getContext(),vb,typeArr[i]);
            }
            vb.typeLayout.invalidate();
        }

        if(!TextUtils.isEmpty(recipe.getFullname())) vb.userDetail.setText(recipe.getFullname());

        if(!TextUtils.isEmpty(recipe.getUserPhotoUrl())){
            Glide.with(vb.getRoot().getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getUserPhotoUrl())
                    .centerCrop()
                    .error(R.color.black)
                    .placeholder(R.mipmap.ic_launcher)
                    .transform(new CircleTransform(vb.getRoot().getContext()))
                    .into(vb.userPhoto);

            vb.userPhoto.invalidate();
        }
    }

    private void addedTypeView(Context c, RecipeListItemBinding vb ,@NonNull String title){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) params.setMarginEnd(5);
        else params.setMargins(0,0,5,0);

        TextView tv = new TextView(c);
        tv.setLayoutParams(params);
        tv.setText(title);
        tv.setTextSize(12);
        tv.setTextColor(ContextCompat.getColor(c,R.color.white));
        tv.setBackgroundColor(ContextCompat.getColor(c,R.color.lightGreen));
        tv.setPadding(7,7,7,7);
        vb.typeLayout.addView(tv);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public void clear(){
        list.clear();
    }
}
