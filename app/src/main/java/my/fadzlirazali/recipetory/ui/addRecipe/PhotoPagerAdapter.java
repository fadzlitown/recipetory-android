package my.fadzlirazali.recipetory.ui.addRecipe;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import my.fadzlirazali.recipetory.R;

/*
* http://codetheory.in/android-image-slideshow-using-viewpager-pageradapter/
* */
public class PhotoPagerAdapter extends PagerAdapter {
 
    Context mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<Object> mPhotos;
    private boolean isDisplayOnly=false;

    public PhotoPagerAdapter(Context context) {
        mContext = context;
        mPhotos = new ArrayList<>();
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void add(Uri  uri){
        mPhotos.add(uri);
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<Uri> uris){
        if(mPhotos.size()>0) mPhotos.clear();

        mPhotos.addAll(uris);
        notifyDataSetChanged();
    }

    public void addAllPath(ArrayList<String> paths){
        isDisplayOnly=true;
        if(mPhotos.size()>0) mPhotos.clear();

        mPhotos.addAll(paths);
        notifyDataSetChanged();
    }

    public void clear(){
        mPhotos.clear();
    }
 
    @Override
    public int getCount() {
        return mPhotos.size();
    }
 
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    /**
     * POSITION_NONE Called when the host view is attempting to determine if an item's position
     * has changed. Returns {@link #POSITION_UNCHANGED} if the position of the given
     * item has not changed
     * */
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.photo_pager_item, container, false);
        ImageView upload = (ImageView) itemView.findViewById(R.id.upload);
        ImageView delete = (ImageView) itemView.findViewById(R.id.delete);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        Glide.with(imageView.getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                .load(mPhotos.get(position))
                .override(300, 250)
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhotos.remove(position);
                ((AddRecipeActivity) mContext).mSelectedImages.remove(position);
                notifyDataSetChanged();
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AddRecipeActivity) mContext).onUploadPhoto();
            }
        });

        if(isDisplayOnly) {
            upload.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
        }

        container.addView(itemView);
        return itemView;
    }
 
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}