package my.fadzlirazali.recipetory.ui.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import my.dearfadz.util.CircleTransform;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityUserProfileBinding;
import my.fadzlirazali.recipetory.model.Profile;
import my.fadzlirazali.recipetory.ui.BaseActivity;
import my.fadzlirazali.recipetory.ui.main.MainActivity;
import my.fadzlirazali.recipetory.ui.main.MainPagerAdapter;
import my.fadzlirazali.recipetory.util.Constant;
import my.fadzlirazali.recipetory.util.ProfileSharedPref;

public class UserProfileActivity extends BaseActivity {

    private static final String MEMBER_UID = "member_uid";
    public static final int UPDATE_PROFILE = 100;
    private ActivityUserProfileBinding binding;
    private  String[] profileTitleList = new String[]{"PROFILE","RECIPES","BOOKMARKS"};
    private String mUID;
    private MainPagerAdapter mAdapter;

    public static void start(Context context) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        ((MainActivity) context).startActivityForResult(intent,UPDATE_PROFILE);
    }

    public static void start(Context context, String memberUID) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(MEMBER_UID,memberUID);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_user_profile);
        setToolbar();

        if(getIntent().hasExtra(MEMBER_UID)) mUID = getIntent().getStringExtra(MEMBER_UID);
        else mUID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Profile profile = Profile.fromJson(ProfileSharedPref.getProfile(this));
        if(profile!=null && mUID.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))  //if store owned profile && is owned uid
            setViewPager(profile);
        else {
            FirebaseDatabase.getInstance().getReference().child("users").child(mUID).child("profile")
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //use the .getValue() method, passing in the .class of the person.
                            //Firebase will use reflection to deserialize the JSON content, and populate the properties of the instantiated obj's
                            Profile profile = dataSnapshot.getValue(Profile.class);
//                            for (DataSnapshot data: dataSnapshot.getChildren()) {
//                                if(data.getKey().equals("fullname")) profile.setFullname((String) data.getValue()); //wajib
//                                if(data.getKey().equals("photoThumbnail")) profile.setPhotoThumbnail((String) data.getValue()); //wajib
//                                if(data.getKey().equals("about")) profile.setAbout((String) data.getValue());
//                                if(data.getKey().equals("facebook")) profile.setFacebook((String) data.getValue());
//                                if(data.getKey().equals("instagram")) profile.setInstagram((String) data.getValue());
//                                if(data.getKey().equals("web")) profile.setWeb((String) data.getValue());
//                                if(data.getKey().equals("state")) profile.setState(((String) data.getValue()));
//                                if(data.getKey().equals("location")){   //wajib
//                                    profile.setLocation((String) data.getValue());
//                                    //loadCountry();
//                                }
//                            }
                            if(profile!=null) setViewPager(profile);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Util.isListShown(binding.scrollView,binding.progress,true,true);
                            Toast.makeText(UserProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void setupUI(Profile profile) {
        if(!TextUtils.isEmpty(profile.getFullname()))
            binding.title.setText(profile.getFullname());

        if(!TextUtils.isEmpty(profile.getPhotoThumbnail()) && isValidContextForGlide(this)){
            Glide.with(this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(Uri.parse(profile.getPhotoThumbnail()))
                    .override(140, 140)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .transform(new CircleTransform(this))
                    .into(binding.userPhoto);
        }

        if(profile.getState()!=null && profile.getLocation() != null){
            binding.address.setText(profile.getState()+", "+profile.getLocation());
        }
        setRecipeWithPointsSP();
    }

    //https://github.com/bumptech/glide/issues/1484
    public static boolean isValidContextForGlide(final Context context) {
        if (context == null) {
            return false;
        }
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.isDestroyed() || activity.isFinishing()) {
                return false;
            }
        }
        return true;
    }

    public void setRecipeWithPointsSP(){
        if(!TextUtils.isEmpty(ProfileSharedPref.getTotalRecipe(this)))
            binding.totalUserRecipe.setText(ProfileSharedPref.getTotalRecipe(this)+"\nRecipes");
        if(!TextUtils.isEmpty(ProfileSharedPref.getTotalRecooked(this)))
            binding.totalCookedRecipe.setText(ProfileSharedPref.getTotalRecooked(this)+"\nRecooked");


        /** POINTS CALCULATION
         *
         * Points = (totalRecipe * 2) + (totalRecooked *3)
         *
         * */
        int totalRecipe = Integer.valueOf(ProfileSharedPref.getTotalRecipe(this));
        int totalRecooked = Integer.valueOf(ProfileSharedPref.getTotalRecooked(this));
        int points = (totalRecipe*2)+(totalRecooked*3);
        binding.points.setText(String.valueOf(points)+"\nPoints");

        if(points<=30)
            binding.userLevel.setText("Begineer");
        else if(points<100)
            binding.userLevel.setText("Intermidiate");
        else if(points>100 && points<300)
            binding.userLevel.setText("Advance");
        else
            binding.userLevel.setText("Master");
    }

    private void setViewPager(Profile profile) {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(ProfileInfoFragment.newInstance(profile.toJson(),mUID));
        fragments.add(UserRecipeFragment.newInstance(Constant.RecipeType.SELF,mUID));
        fragments.add(UserRecipeFragment.newInstance(Constant.RecipeType.BOOKMARK,mUID));
        mAdapter = new MainPagerAdapter(getSupportFragmentManager(),fragments, profileTitleList);
        binding.viewpager.setAdapter(mAdapter);
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               // if(position>=1) setRecipeWithPointsSP(); //update TOTAL RECIPE & RECOOKED IN SP & UPDATE TO UI
                if(position==1 || position==2){

                }
            }

            @Override
            public void onPageSelected(int position) {}

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        binding.tabs.setupWithViewPager(binding.viewpager);
        setupUI(profile);
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setElevation(200);
        }
        binding.collapsingToolbar.setTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==UserProfileActivity.UPDATE_PROFILE && resultCode==RESULT_OK){
            //if update user profile, then update
            Profile profile = Profile.fromJson(ProfileSharedPref.getProfile(this));
            if(profile!=null) {
                setupUI(profile);
                if(mAdapter!=null)  //update Profile fragment
                    ((ProfileInfoFragment) mAdapter.getItem(0)).updateProfile(profile);
            }
        }
    }

}
