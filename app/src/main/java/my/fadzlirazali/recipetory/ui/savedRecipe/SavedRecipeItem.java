package my.fadzlirazali.recipetory.ui.savedRecipe;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.bumptech.glide.Glide;
import com.xwray.groupie.Item;

import java.util.List;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.SavedRecipeListItemBinding;

/**
 * Created by mohdfadzli on 21/12/2017.
 */

class SavedRecipeItem extends Item<SavedRecipeListItemBinding> {
    private final List<my.fadzlirazali.recipetory.orm.Recipe> list;
    private final SavedRecipeActivity.SavedRecipeListener savedRecipeListener;

    public SavedRecipeItem(List<my.fadzlirazali.recipetory.orm.Recipe> recipes, SavedRecipeActivity.SavedRecipeListener savedRecipeListener) {
        this.list=recipes;
        this.savedRecipeListener=savedRecipeListener;
    }

    @Override
    public int getLayout() {
        return R.layout.saved_recipe_list_item;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void bind(SavedRecipeListItemBinding vb, final int position) {
        final my.fadzlirazali.recipetory.orm.Recipe item = list.get(position);
        final my.fadzlirazali.recipetory.model.Recipe recipe = my.fadzlirazali.recipetory.model.Recipe.fromJson(item.getRecipeJson());

        if(recipe.getUriList().size()>0 && recipe.getUriList().get(0)!=null){
            Glide.with(vb.getRoot().getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getUriList().get(0))
                    .override(300, 250)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .into(vb.recipePhoto);
        }

        vb.name.setText(recipe.getTitle());
        vb.time.setText(recipe.getDuration()+"m");
        vb.userDetail.setText(recipe.getDesc());
        vb.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(v.getContext())
                        .setTitle("Warning").setMessage("Are you sure want to delete?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                savedRecipeListener.onRemove(item);
                                list.remove(position);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {}
                        })
                        .show();
            }
        });
        vb.updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savedRecipeListener.onUpdate(item);
            }
        });
    }
}
