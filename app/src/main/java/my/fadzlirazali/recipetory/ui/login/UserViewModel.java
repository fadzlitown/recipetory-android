package my.fadzlirazali.recipetory.ui.login;

import android.databinding.BaseObservable;
import android.support.annotation.Nullable;

import my.fadzlirazali.recipetory.model.User;

public class UserViewModel extends BaseObservable {
    @Nullable
    User user;

    public UserViewModel() {
    }

    public UserViewModel(@Nullable User user) {
        this.user = user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public long getUserId() {
        if (user != null) {
            return user.getUserId();
        }
        return -1L;
    }

    public String getUserName() {
        if (user != null) {
            return user.getUserName();
        }
        return null;
    }

    public String getFullname() {
        if (user != null) {
            return user.getUserFullName();
        }
        return null;
    }

    public String getPhoneNumber() {
        if (user != null) {
            return user.getPhoneNumber();
        }
        return null;
    }

    public String getEmail() {
        if (user != null) {
            return user.getEmail();
        }
        return null;
    }

}
