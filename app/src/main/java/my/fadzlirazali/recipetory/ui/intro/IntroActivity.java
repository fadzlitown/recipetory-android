package my.fadzlirazali.recipetory.ui.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.MessageButtonBehaviour;
import agency.tango.materialintroscreen.SlideFragmentBuilder;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.ui.login.LoginActivity;

public class IntroActivity extends MaterialIntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        enableLastSlideAlphaExitTransition(true);

        Bundle bundle  = new Bundle();
        bundle.putInt(IntroCustomSlide.IMAGE_ARG,R.drawable.intro1_low);
        bundle.putString(IntroCustomSlide.TITLE_ARG,"POST YOUR RECIPE");
        bundle.putString(IntroCustomSlide.SUBTITLE_ARG,"Let the whole world know your recipe!");
        IntroCustomSlide frag = new IntroCustomSlide();
        frag.setArguments(bundle);
        addSlide(frag);

        Bundle bundle2  = new Bundle();
        bundle2.putInt(IntroCustomSlide.IMAGE_ARG,R.drawable.intro2_low);
        bundle2.putString(IntroCustomSlide.TITLE_ARG,"SEARCH RECIPE");
        bundle2.putString(IntroCustomSlide.SUBTITLE_ARG,"Look other people recipe!");
        IntroCustomSlide frag2 = new IntroCustomSlide();
        frag2.setArguments(bundle2);
        addSlide(frag2);

        addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.colorPrimary)
                        .buttonsColor(R.color.colorPrimaryDark)
                        .image(R.drawable.intro2_low)
                        .title("SEARCH RECIPE")
                        .description("Look other people recipe!")
                        .build(),
                new MessageButtonBehaviour(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMessage("We provide solutions to make you love your work");
                        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }
                }, "Start now!"));

    }
}
