package my.fadzlirazali.recipetory.ui.profile;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.FragmentProfileInfoBinding;
import my.fadzlirazali.recipetory.model.Profile;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileInfoFragment extends Fragment {

    private static final String PROFILE = "profile";
    private static final String UID = "user_id";
    private FragmentProfileInfoBinding binding;
    private String usernameFB, usernameIG, website;
    private Profile profile;

    public ProfileInfoFragment() {
        // Required empty public constructor
    }

    public static ProfileInfoFragment newInstance(String profile, String mUID) {
        Bundle bundle = new Bundle();
        bundle.putString(PROFILE,profile);
        bundle.putString(UID,mUID);
        ProfileInfoFragment fragment = new ProfileInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null && getArguments().containsKey(UID) && getArguments().containsKey(PROFILE)){
            profile = Profile.fromJson(getArguments().getString(PROFILE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_info, container, false);
        View v = binding.getRoot();
        if(profile!=null)
            updateUI(profile);
        return v;
    }

    private void updateUI(final Profile profile) {
        if(!TextUtils.isEmpty(profile.getAbout()))
            binding.aboutUser.setText(profile.getAbout());

        binding.editAboutBtn.setVisibility(getArguments().getString(UID).equals(FirebaseAuth.getInstance().getCurrentUser().getUid()) ? View.VISIBLE : View.GONE);
        binding.editAboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateUserProfileActivity.start(getActivity(),profile.toJson());
            }
        });
    }


    public void updateProfile(Profile profile) {
        this.profile=profile;
        updateUI(profile);
    }
}
