package my.fadzlirazali.recipetory.ui.main;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import my.fadzlirazali.recipetory.databinding.RecipeListItemBinding;

/**
 * Created by mohdfadzli on 23/10/2017.
 */

class RecipeCardBindingHolder extends RecyclerView.ViewHolder {
     public RecipeListItemBinding binding;

    public RecipeCardBindingHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public void setTitle(String title){
        binding.name.setText(title);
    }
}
