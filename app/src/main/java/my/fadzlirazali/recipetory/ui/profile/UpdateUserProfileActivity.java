package my.fadzlirazali.recipetory.ui.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.HashMap;
import java.util.Map;

import my.dearfadz.util.CircleTransform;
import my.dearfadz.util.Util;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityUpdateUserProfileBinding;
import my.fadzlirazali.recipetory.model.Profile;
import my.fadzlirazali.recipetory.ui.main.MainActivity;
import my.fadzlirazali.recipetory.util.ProfileSharedPref;


public class UpdateUserProfileActivity extends AppCompatActivity {

    private static final String IS_NEW_USER = "is_new_user";
    private static final int REQUEST_CODE_PICK_IMAGE = 1;
    private static final int PERMISSION_READ_EXTERNAL_STORAGE = 2;
    private static final String PROFILE = "profile";

    private ActivityUpdateUserProfileBinding binding;
    private Uri mUri;
    private FirebaseUser mUser;
    private String locationCode;
    private HashMap<String,String> mCountryMaps;


    public static void start(Context context, String profile) {
        Intent starter = new Intent(context, UpdateUserProfileActivity.class);
        starter.putExtra(PROFILE,profile);
        ((UserProfileActivity) context).startActivityForResult(starter,UserProfileActivity.UPDATE_PROFILE);
    }

    public static void start(Context context, boolean isNewUser) {
        Intent starter = new Intent(context, UpdateUserProfileActivity.class);
        starter.putExtra(IS_NEW_USER,isNewUser);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_update_user_profile);
        initUI();
    }

    private void initUI() {
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        Util.isListShown(binding.scrollView,binding.progress,false,true);

        if(mUser!=null){
            if(!TextUtils.isEmpty(mUser.getDisplayName()))
                binding.fullname.setText(mUser.getDisplayName());

            if(!TextUtils.isEmpty(mUser.getEmail()))
                binding.email.setText(mUser.getEmail());

            binding.verifyEmailBtn.setText(mUser.isEmailVerified() ? "Verified":"Not Verify");
            binding.verifyEmailBtn.setTextColor(mUser.isEmailVerified() ? getResources().getColor(R.color.lightGreen) : getResources().getColor(R.color.red_heart));

            if(mUser.getPhotoUrl()!=null){
                mUri=mUser.getPhotoUrl();
            Glide.with(this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(mUser.getPhotoUrl())
                    .override(250, 250)
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .transform(new CircleTransform(this))
                    .into(binding.userPhoto);
            }
        }


        if(getIntent().hasExtra(PROFILE)) {
            Profile profile = Profile.fromJson(getIntent().getStringExtra(PROFILE));
            binding.about.setText(profile.getAbout());
            binding.fbLink.setText(profile.getFacebook());
            binding.igLink.setText(profile.getInstagram());
            binding.webLink.setText(profile.getWeb());
            binding.state.setText(profile.getState());
            if (!TextUtils.isEmpty(profile.getLocation())) {
                locationCode = profile.getLocation();
            }
        } else if(getIntent().hasExtra(IS_NEW_USER)){
            //todo
            //locationCode="MS"; //default first user is Malaysia
        }
        loadCountry();
    }

    private void loadCountry() {
        Ion.with(this)
                .load("http://country.io/names.json")
                .addHeader("Content-Type", "application/json")
                .asString()
                .withResponse().setCallback(new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                if(result.getHeaders().code()==200){
                    mCountryMaps = new Gson().fromJson(result.getResult(), new TypeToken<HashMap<String, String>>(){}.getType());

                    //if location stored before
                    if(locationCode!=null && mCountryMaps!=null){
                        for (Map.Entry<String,String> map:  mCountryMaps.entrySet()) {
                            if(locationCode.equals(map.getKey())){
                                binding.location.setText(map.getValue());
                            }
                        }
                    }
                    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(UpdateUserProfileActivity.this,android.R.layout.simple_list_item_1, mCountryMaps.values().toArray(new String[mCountryMaps.size()]));
                    binding.location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OnDialogList(adapter,binding.location);
                        }
                    });
                } else {
                    if(result.getException().getMessage()!=null)
                        Toast.makeText(UpdateUserProfileActivity.this, result.getException().getMessage() , Toast.LENGTH_SHORT).show();
                }
                Util.isListShown(binding.scrollView,binding.progress,true,true);
            }
        });


//        Locale[] locales = Locale.getAvailableLocales();
//        ArrayList<String> countries = new ArrayList<>();
//        final ArrayList<String> codes = new ArrayList<>();
//
//        for (int i = 0; i < locales.length; i++) {
//            String code = locales[i].getCountry();
//            String country = locales[i].getDisplayCountry();
//            if (country.trim().length()>0 && !countries.contains(country)) {
//                countries.add(country);
//            }
//            if (code.trim().length()>0 && !codes.contains(code)) {
//                codes.add(code);
//            }
//
//            if(locationCode!=null && code.trim().length()>0 && code.equals(locationCode)){
//                binding.location.setText(country);
//            }
//        }
//        Collections.sort(countries);


    }

    private void OnDialogList(final ArrayAdapter<String> adapter, final TextView location) {
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setTitle("Select Country");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String item = adapter.getItem(which);
                location.setText(item);
                for (Map.Entry<String,String> map:  mCountryMaps.entrySet()) {
                    if(item!=null && item.equals(map.getValue())){
                        locationCode = map.getKey();        //update the latest clicked locationCode
                    }
                }
                Log.d("test", "onClick: "+locationCode+" "+item);
                dialogInterface.dismiss();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public void OnUpdate(View view) {
        updateFirebaseProfile();
    }

    //firebase fullname, email, uriPhoto
    private void updateFirebaseProfile() {
        if(TextUtils.isEmpty(binding.location.getText())){
            Toast.makeText(this, "Please select your country", Toast.LENGTH_SHORT).show();
            return;
        }

        UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
        if(mUser.getDisplayName()!=null && !mUser.getDisplayName().equals(binding.fullname.getText().toString()))
            builder.setDisplayName(binding.fullname.getText().toString());
        if (mUri != null && mUser.getPhotoUrl()!=null && !mUser.getPhotoUrl().equals(mUri)) {
            builder.setPhotoUri(mUri);
        }
        UserProfileChangeRequest profileUpdates = builder.build();
        mUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UpdateUserProfileActivity.this, "ProfileEvent Updated", Toast.LENGTH_SHORT).show();
                            updateFirebaseProfileInfo();
                        } else {
                            Toast.makeText(UpdateUserProfileActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                });


        //check if firebase email match with updated one
        if(mUser.getEmail()!=null && !mUser.getEmail().equals(binding.email.getText().toString())) {
            mUser.updateEmail(binding.email.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(UpdateUserProfileActivity.this, "Please verify your email", Toast.LENGTH_SHORT).show();
                        mUser.sendEmailVerification();
                    } else {
                        Toast.makeText(UpdateUserProfileActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

//                        if(task.getException() instanceof FirebaseAuthRecentLoginRequiredException){
//                            //getCredential(String email, String password)
//                            AuthCredential credential = EmailAuthProvider.getCredential(binding.email.getText().toString(),"abc123");
//                            mUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isSuccessful()) {
//                                        Toast.makeText(UpdateUserProfileActivity.this, "Email Updated", Toast.LENGTH_SHORT).show();
//                                    } else {
//                                        Toast.makeText(UpdateUserProfileActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                        }
                    }
                }
            });
        }
    }

    private void updateFirebaseProfileInfo() {
        final Profile profile = new Profile();
        profile.setFullname(mUser.getDisplayName());
        profile.setPhotoThumbnail(mUser.getPhotoUrl()!=null ? mUser.getPhotoUrl().toString() : "");
        profile.setLocation(locationCode);
        profile.setState(binding.state.getText().toString());
        profile.setAbout(binding.about.getText().toString());
        profile.setFacebook(binding.fbLink.getText().toString());
        profile.setInstagram(binding.igLink.getText().toString());
        profile.setWeb(binding.webLink.getText().toString());

        Util.isListShown(binding.scrollView,binding.progress,false,true);
        FirebaseDatabase.getInstance().getReference().child("users").child(mUser.getUid()).child("profile").setValue(profile).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Util.isListShown(binding.scrollView,binding.progress,true,true);
                ProfileSharedPref.setProfile(UpdateUserProfileActivity.this,profile.toJson());
                Toast.makeText(UpdateUserProfileActivity.this, "Your profile has been updated", Toast.LENGTH_SHORT).show();
                if(mUser!=null && getIntent().hasExtra(IS_NEW_USER)) MainActivity.start(UpdateUserProfileActivity.this);
                else {
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }


    public void onProfilePicClick(View view) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_STORAGE);
        else {
            pickImage();
        }
    }

    @Override
    public void onBackPressed() {
        //if they try to avoid from updating profile for first installed user, just push it
        if(mUser!=null && getIntent().hasExtra(IS_NEW_USER)) updateFirebaseProfile();
        else super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        }
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK_IMAGE) {
                mUri = data.getData();
                Glide.with(this)
                        .loadFromMediaStore(data.getData())
                        .transform(new CircleTransform(this))
                        .into(binding.userPhoto);
            }
        }
    }
}
