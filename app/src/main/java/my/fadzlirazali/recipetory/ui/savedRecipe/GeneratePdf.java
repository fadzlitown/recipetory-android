package my.fadzlirazali.recipetory.ui.savedRecipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import my.dearfadz.util.ImageUtils;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.model.RecipeType;

import static com.itextpdf.text.PageSize.A4;

/**
 * Created by mohdfadzli on 26/1/2018.
 */

public class GeneratePdf {

    private static final String IMAGE_1 = "drawable://" + R.drawable.intro2_low;

    private static final String USER_ICON = "user_icon.png";
    private static final String DURATION_ICON = "duration_icon.png";
    private static final String CUISINE_ICON = "cuisine_icon.png";

    private final Context context;
    private PdfWriter pdfWriter;
    private ArrayList<Recipe> json;
    private File pdfFile;

    public GeneratePdf(Context c) {
        this.context=c;
    }

    public File getPdfFile() {
        return pdfFile;
    }

    public boolean OnGenerate(String filename, ArrayList<Recipe> list){
        this.json=list;
        try {
            //creating a directory in SD card
            File mydir = new File(Environment.getExternalStorageDirectory()
                    + StaticValue.PATH_PRODUCT_REPORT); //PATH_PRODUCT_REPORT="/SIAS/REPORT_PRODUCT/"
            if (!mydir.exists()) {
                mydir.mkdirs();
            }

            //getting the full path of the PDF report name
            String mPath = Environment.getExternalStorageDirectory().toString()
                    + StaticValue.PATH_PRODUCT_REPORT //PATH_PRODUCT_REPORT="/SIAS/REPORT_PRODUCT/"
                    + filename+".pdf"; //reportName could be any name

            //constructing the PDF file
            pdfFile = new File(mPath);

            //Creating a Document with size A4. Document class is available at  com.itextpdf.text.Document
            final Document document = new Document(A4);

            //assigning a PdfWriter instance to pdfWriter
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

            //PageFooter is an inner class of this class which is responsible to create Header and Footer
            PageHeaderFooter event = new PageHeaderFooter();
            pdfWriter.setPageEvent(event);

            //Before writing anything to a document it should be opened first
            document.open();

            //Adding meta-data to the document
            addMetaData(document);

            //Cover page
            addCoverPage(pdfWriter,document);

            //List of recipes
//            addBulletContent(document);

            //Adding Title(s) of the document
//            addTitleSubtitle(document);
            //Adding main contents of the document


            addContent(document);

            //Closing the document
            document.close();

        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
        return true;
    }

    private void addCoverPage(PdfWriter pdfWriter, Document document) {
        document.newPage();

        addFrontTitle(pdfWriter,document,json.size()+" Recipe Pilihan Anda!","kjabsdkjabskhdjbaksjdbkajsbnd askjdhaojsdnlkjasd","by Siti Cek Molek");

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.intro1_low);

//        String fileName ="img1.jpg";
//        File resizedFile= ImageUtils.savePdfImageToFile(context,
//                new File(Uri.parse("android.resource://"+R.class.getPackage().getName()+"/"+R.drawable.intro1_low).toString()),
//                PageSize.A4.getHeight(),PageSize.A4.getWidth());

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_pdf_images");
        myDir.mkdirs();

        String fname = "frontPdf.jpg";
        File file = new File (myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        Image image = null;
        try {
            image = Image.getInstance(file.getAbsolutePath());
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        if(image!=null){
//            image.scaleAbsolute(PageSize.A4.rotate());
            image.setAbsolutePosition(0, 0);
            canvas.saveState();

            PdfGState state = new PdfGState();
            state.setFillOpacity(0.6f);
            canvas.setGState(state);
            try {
                canvas.addImage(image);
                canvas.restoreState();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

    }

    public void addRecipeImage(Document document, ArrayList<String> imgFilePath, String title) throws DocumentException {
        document.newPage();

        // Create and add a Paragraph
        Paragraph p = new Paragraph(title,  StaticValue.FONT_TITLE);
        p.setAlignment(Element.ALIGN_CENTER);
        document.add(p);

        Uri[] uris = new Uri[imgFilePath.size()];
        for (int i = 0; i < imgFilePath.size(); i++) {
             uris[i] = Uri.parse(imgFilePath.get(i));
        }

        String path = ImageUtils.getUriPath(context,uris[0]);
        File file = new File(path!=null ? path : "");
        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        Image image = null;
        try {
            image = Image.getInstance(file.getAbsolutePath());
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        if(image!=null){
            if(image.getWidth()>image.getHeight()) image.scaleToFit(595*0.9f,842*0.9f);
            else image.scaleToFit(595*0.6f,842*0.6f);

            image.setAbsolutePosition(
                    (PageSize.A4.getWidth() - image.getScaledWidth()) / 2,
                    (PageSize.A4.getHeight() - (image.getScaledHeight()-150)) / 2);

            image.setAlignment(Element.ALIGN_CENTER);
            canvas.saveState();

            PdfGState state = new PdfGState();
            canvas.setGState(state);
            try {
                canvas.addImage(image);
                canvas.restoreState();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

        //multiple imges TODO
        PdfPTable table = new PdfPTable(new float[]{1,1,1,1,1});
        PdfPCell row = new PdfPCell();
        Image[] images = new Image[uris.length];
        for (int i = 0; i <uris.length ; i++) {
            try {
                String path2 = ImageUtils.getUriPath(context,uris[i]);
                images[i] = Image.getInstance(new File(path2!=null ? path2 : "").getAbsolutePath());
            } catch (BadElementException | IOException e) {
                e.printStackTrace();
            }
            float percentCell = 100/images.length;
           // images[i].scalePercent(20f);
           // images[i].setScaleToFitHeight(true);
            images[i].setWidthPercentage(20);
            //images[i].scalePercent(0.3f,0.3f);
            row.addElement(images[i]);
        }
        table.setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.addCell(row);
        document.add(table);
    }


    private void addFrontTitle(PdfWriter pdfWriter, Document document, String text, String desc, String author) {
        Paragraph p = new Paragraph(20 ,text, StaticValue.FONT_BOOK_TITLE);
        p.setAlignment(Element.ALIGN_MIDDLE);
        p.setLeading(0, 1);

        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);
        table.setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPCell cell1 = new PdfPCell();
        cell1.setBorder(Rectangle.NO_BORDER);
        cell1.setMinimumHeight(A4.getHeight()-200);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.addElement(p);
        cell1.addElement(Chunk.NEWLINE);

        Paragraph d = new Paragraph(new Chunk(desc, StaticValue.FONT_BOOK_DESC));
        d.setAlignment(Element.ALIGN_MIDDLE);
        d.setLeading(0, 1);
        cell1.addElement(d);
        cell1.addElement(Chunk.NEWLINE);
        cell1.addElement(Chunk.NEWLINE);

        Paragraph a = new Paragraph(new Chunk(author, StaticValue.FONT_BOOK_AUTHOR));
        a.setAlignment(Element.ALIGN_RIGHT);
        a.setLeading(0, 1);
        cell1.addElement(a);

        cell1.setRowspan(4);
        table.addCell(cell1);

//        PdfPCell cell2 = new PdfPCell();
//        cell1.setBorder(Rectangle.NO_BORDER);
//        cell2.setMinimumHeight(A4.getHeight()/4);
//        cell2.addElement(d);
//        table.addCell(cell2);


        try {
            document.add(table);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public void addColumn(PdfWriter writer, String text , boolean useAscender) throws DocumentException {
        Rectangle rect = new Rectangle(595/5, 842/3, 475, 562);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(3.5f);
        rect.setBorderColor(BaseColor.BLACK);
        PdfContentByte cb = writer.getDirectContent();
        cb.rectangle(rect);

        Phrase p = new Phrase();
        p.add(text);
        p.setFont(StaticValue.FONT_BOOK_TITLE);
        ColumnText ct = new ColumnText(cb);
        ct.setSimpleColumn(rect);
        ct.setUseAscender(useAscender);
        ct.addText(p);
//        ct.setSpaceCharRatio(PdfWriter.NO_SPACE_CHAR_RATIO);
        ct.go();
    }


    /**
     *  iText allows to add metadata to the PDF which can be viewed in your Adobe Reader. If you right click
     *  on the file and to to properties then you can see all these information.
     * @param document
     */
    private static void addMetaData(Document document) {
        document.addTitle("All Product Names");
        document.addSubject("none");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("SIAS ERP");
//        document.addCreator(StaticValue.USER_MODEL.getName());
    }

    /**
     * In this method title(s) of the document is added.
     * @param document
     * @throws DocumentException
     */
    private static void addTitleSubtitle(Document document)
            throws DocumentException {

        //End of adding several titles

    }

    /**
     * In this method the main contents of the documents are added
     * @param document
     * @throws DocumentException
     */

    private void addContent(final Document document) throws DocumentException, IOException {

        final Paragraph reportBody = new Paragraph();
        reportBody.setFont(StaticValue.FONT_BODY); //public static Font FONT_BODY = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.NORMAL);

        createTable(reportBody,document);
        // now add all this to the document
        document.add(reportBody);

//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    document.add(reportBody);
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }
//            }
//        },13000);

//        java.util.Timer timer =new java.util.Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                try {
//                    createTable(reportBody,document);
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        },10000);
//
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                // now add all this to the document
//                try {
//                    document.add(reportBody);
//                } catch (DocumentException e) {
//                    e.printStackTrace();
//                }
//            }
//        },13000);

    }

    /**
     * This method is responsible to add table using iText
     * @param reportBody
     * @param document
     * @throws BadElementException
     */
    @WorkerThread
    private void createTable(Paragraph reportBody, Document document)
            throws DocumentException, IOException {


        for (int i = 0; i < json.size(); i++) {

            my.fadzlirazali.recipetory.model.Recipe recipe = json.get(i);
            //if(json.listLazy().isClosed()) json.listLazy().close();

            if(recipe.getUriList()!=null && recipe.getUriList().size()>0) {
                addRecipeImage(document, recipe.getUriList(), recipe.getTitle());
            }

            document.newPage();
            reportBody = new Paragraph();
            createTitleSubtitle(recipe.getTitle(),recipe.getDesc(), DateTime.now().toString(),document);

            PdfPTable table2 = new PdfPTable(new float[]{1,1,1,1,1,1});
            table2.setWidthPercentage(90);
            table2.setWidths(new int[]{1,1,1,1,1,1});
            PdfPCell cellNoBorder = new PdfPCell();
            cellNoBorder.setBorder(Rectangle.NO_BORDER);

            table2.addCell(createImageCell(USER_ICON));
            Phrase p1 = new Phrase(recipe.getServing());
            PdfPCell pc1 = new PdfPCell(p1);
//            pc1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pc1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pc1.setBorder(Rectangle.NO_BORDER);
//            pc1.addElement(Chunk.NEWLINE);
            table2.addCell(pc1);
//            table2.addElement(cellNoBorder);

            table2.addCell(createImageCell(DURATION_ICON));
            Phrase p2 = new Phrase(recipe.getDuration()+" mins");
            PdfPCell pc2 = new PdfPCell(p2);
//            pc2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pc2.setHorizontalAlignment(Element.ALIGN_CENTER);
            pc2.setBorder(Rectangle.NO_BORDER);
//            pc2.addElement(Chunk.NEWLINE);
            table2.addCell(pc2);
//            table2.addCell(cellNoBorder);


            table2.addCell(createImageCell(CUISINE_ICON));
            Phrase p3 = new Phrase(recipe.getCuisine());
            PdfPCell pc3 = new PdfPCell(p3);
//            pc3.setVerticalAlignment(Element.ALIGN_MIDDLE);
            pc3.setHorizontalAlignment(Element.ALIGN_CENTER);
            pc3.setBorder(Rectangle.NO_BORDER);
            table2.addCell(pc3);
            reportBody.add(table2);
            addEmptyLine(reportBody, 2);


            if(recipe.getTypeList()!=null){
                PdfPTable table = null;
                ArrayList<Float> floatArrayList = new ArrayList<Float>();
                for (int z=0; z<recipe.getTypeList().size(); z++) {
                    if(recipe.getTypeList().get(z).isChecked())
                        floatArrayList.add(1.0f);
                }

                int no = 0;
                float[] totalTypeNum = new float[floatArrayList.size()];
                for (float n : floatArrayList) {
                    totalTypeNum[no++] = n;
                }

//            float[] columnWidths = {0,totalTypeNum-1}; //total 4 columns and their width. The first three columns will take the same width and the fourth one will be 5/2.
                table = new PdfPTable(totalTypeNum);
//            float totalSpaceWidth = 70/totalTypeNum;
                table.setWidthPercentage(70); //set table with 100% (full page)
                table.setTotalWidth(70/totalTypeNum.length);
//            table.getDefaultCell().setUseAscender(true);

                for (RecipeType item: recipe.getTypeList()) {
                    if(item.isChecked()){
                        PdfPCell cell = new PdfPCell(new Phrase(item.getType(), //Table Header
                                StaticValue.FONT_TABLE_HEADER)); //Public static Font FONT_TABLE_HEADER = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER); //alignment
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

                        BaseColor color = new BaseColor(ContextCompat.getColor(context,R.color.lightGreen));
                        cell.setBackgroundColor(color); //cell background color
                        cell.setBorderColor( BaseColor.WHITE);
                        cell.setBorder(55);
                        cell.setFixedHeight(40); //cell height
                        table.addCell(cell);
                    }
                }
                reportBody.add(table);
            }


            List list1 = new List(List.ORDERED);

            if(recipe.getIngredient()!=null){
                /*ingredient*/
                for (int j = 0; j < recipe.getIngredient().size(); j++) {
                    list1.setAutoindent(false);
                    list1.setSymbolIndent(42);
                    list1.add(new ListItem(recipe.getIngredient().get(j).getIngradientName()));

//                cell = new PdfPCell(new Phrase("- "+recipe.getIngredient().get(j).getIngradientName()));
//                cell.setFixedHeight(28);
//                table.addCell(cell);

//                if(j==recipe.getIngredient().size()-1) currentTotalHeight = (PaperSize.FOLIO_HEIGHT - 250) + (j*spaceLine);
//                Log.d("pdf", "OnGeneratePdf: j"+currentTotalHeight);
                }
            }


            List list2 = new List(List.ORDERED);
            /*Method*/
            if(recipe.getMethod()!=null) {
                for (int j = 0; j < recipe.getMethod().size(); j++) {
                    list2.setAutoindent(false);
                    list2.setSymbolIndent(42);
                    list2.add(new ListItem(recipe.getMethod().get(j).getMethodName()));
                }
            }

            addEmptyLine(reportBody, 3);

            reportBody.add("Ingredients \n");
            if(list1.size()>0) reportBody.add(list1);
            reportBody.setExtraParagraphSpace(30);
            addEmptyLine(reportBody, 1);

            reportBody.add("Methods \n");
            if(list2.size()>0) reportBody.add(list2);
            addEmptyLine(reportBody, 2);
            document.add(reportBody);
        }

//        reportBody.add(table);

    }

    public PdfPCell createImageCell(String path) throws DocumentException, IOException {
        // load image
        Image image;
        // get input stream
        InputStream ims =  context.getAssets().open(path);
        Bitmap bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        image = Image.getInstance(stream.toByteArray());

        PdfPCell cell =  new PdfPCell(image);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setFixedHeight(7);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }



    private static void createTitleSubtitle(String recipeTitle, String recipeDesc, String date, Document document) throws DocumentException {
        Paragraph paragraph = new Paragraph();

        // Adding several title of the document. Paragraph class is available in  com.itextpdf.text.Paragraph
        Paragraph childParagraph = new Paragraph(recipeTitle, StaticValue.FONT_TITLE); //public static Font FONT_TITLE = new Font(Font.FontFamily.TIMES_ROMAN, 22,Font.BOLD);
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(childParagraph);

        childParagraph = new Paragraph(recipeDesc, StaticValue.FONT_SUBTITLE); //public static Font FONT_SUBTITLE = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(childParagraph);

        childParagraph = new Paragraph(date , StaticValue.FONT_SUBTITLE);
        childParagraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(childParagraph);

        addEmptyLine(paragraph, 2);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
    }

    /**
     * This method is used to add empty lines in the document
     * @param paragraph
     * @param number
     */
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    /**
     * This is an inner class which is used to create header and footer
     * @author XYZ
     *
     */

    class PageHeaderFooter extends PdfPageEventHelper {
        Font ffont = new Font(Font.FontFamily.UNDEFINED, 5, Font.ITALIC);

        public void onEndPage(PdfWriter writer, Document document) {

            /**
             * PdfContentByte is an object containing the user positioned text and graphic contents
             * of a page. It knows how to apply the proper font encoding.
             */
            PdfContentByte cb = writer.getDirectContent();

            /**
             * In iText a Phrase is a series of Chunks.
             * A chunk is the smallest significant part of text that can be added to a document.
             *  Most elements can be divided in one or more Chunks. A chunk is a String with a certain Font
             */
            Phrase footer_poweredBy = new Phrase("Powered By SIAS ERP", StaticValue.FONT_HEADER_FOOTER); //public static Font FONT_HEADER_FOOTER = new Font(Font.FontFamily.UNDEFINED, 7, Font.ITALIC);
            Phrase footer_pageNumber = new Phrase("Page " + document.getPageNumber(), StaticValue.FONT_HEADER_FOOTER);

            // Header
            // ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, header,
            // (document.getPageSize().getWidth()-10),
            // document.top() + 10, 0);

            // footer: show page number in the bottom right corner of each age
            ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                    footer_pageNumber,
                    (document.getPageSize().getWidth() - 10),
                    document.bottom() - 10, 0);
//			// footer: show page number in the bottom right corner of each age
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                    footer_poweredBy, (document.right() - document.left()) / 2
                            + document.leftMargin(), document.bottom() - 10, 0);
        }
    }

    /**
     * Generate static data for table
     */

//    private static void generateTableData(Query<Recipe> jsonList){
//        for (int i = 0; i < jsonList.list().size(); i++) {
//            my.fadzlirazali.recipetory.model.Recipe recipe = my.fadzlirazali.recipetory.model.Recipe.fromJson(jsonList.list().get(i).getRecipeJson());
//
//            /*title Recipe*/
//            writer.addText(50, PaperSize.FOLIO_HEIGHT - 100 , 24, recipe.getTitle());
//
//            /*desc Recipe*/
//            writer.addText(50, PaperSize.FOLIO_HEIGHT - 130 , 21, recipe.getDesc());
//
//            writer.addText(70, (PaperSize.FOLIO_HEIGHT - 180) , 19, "Ingredients");
//            /*ingredient*/
//            for (int j = 0; j < recipe.getIngredient().size(); j++) {
//                writer.addText(70, (PaperSize.FOLIO_HEIGHT - 250) + (j*spaceLine) , 19, "- "+recipe.getIngredient().get(j).getIngradientName());
//                ingredientHeight = ingredientHeight + (j*spaceLine)+5;
////                if(j==recipe.getIngredient().size()-1) currentTotalHeight = (PaperSize.FOLIO_HEIGHT - 250) + (j*spaceLine);
////                Log.d("pdf", "OnGeneratePdf: j"+currentTotalHeight);
//            }
//
//            writer.addText(70, PaperSize.FOLIO_HEIGHT - (210+ingredientHeight) , 19, "Methods");
//            /*method*/
//            for (int k = 0; k < recipe.getMethod().size(); k++) {
//                writer.addText(70, (PaperSize.FOLIO_HEIGHT - (300+ingredientHeight) + (k*spaceLine)) , 19, "- "+recipe.getMethod().get(k).getMethodName());
//
//                if(k==recipe.getMethod().size()-1) currentTotalHeight = 300+ingredientHeight + (k*spaceLine);
////                Log.d("pdf", "OnGeneratePdf: k"+currentTotalHeight);
//            }
//
//            if(recipe.getUriList().size()>1 && recipe.getUriList().get(0)!=null){
//                Uri uri = Uri.parse(recipe.getUriList().get(0));
//                Bitmap logo = null;
//                Bitmap scaledLogo = null;
//                try {
//                    logo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//                    scaledLogo = Bitmap.createScaledBitmap(logo, 200, 200, false);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                if(scaledLogo!=null){
//                    //            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.big_user_placeholder_icon);
//                    // center images ...
//                    int left = (PaperSize.FOLIO_WIDTH - scaledLogo.getWidth()) / 2;
//                    writer.addImage(left, currentTotalHeight-200, scaledLogo);
//
////            left = (PaperSize.FOLIO_WIDTH - icon.getWidth()) / 2;
////            writer.addImage(left, PaperSize.FOLIO_HEIGHT - currentTotalHeight, icon);
//                }
//            }
//            currentTotalHeight=0;
//            ingredientHeight=0;
//            writer.newPage();
//        }
//    }

}
