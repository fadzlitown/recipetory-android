package my.fadzlirazali.recipetory.ui.main;

import com.xwray.groupie.Item;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeListHeaderBinding;

public class FeedRecipeHeader extends Item<RecipeListHeaderBinding> {
    @Override
    public int getLayout() {
        return R.layout.recipe_list_header;
    }

    @Override
    public void bind(RecipeListHeaderBinding viewBinding, int position) {
    }
}
