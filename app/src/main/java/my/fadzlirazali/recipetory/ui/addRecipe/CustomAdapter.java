package my.fadzlirazali.recipetory.ui.addRecipe;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import my.fadzlirazali.recipetory.R;

public class CustomAdapter extends ArrayAdapter<String> {

    Context context;
    int resource, textViewResourceId;
    ArrayList<String> items, tempItems, suggestions;

    public CustomAdapter(Context context, int resource, int textViewResourceId, ArrayList<String> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<String>(items); // this makes the difference.
        suggestions = new ArrayList<String>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        String names = items.get(position);
        if (names != null) {
            TextView lblName = (TextView) view.findViewById(android.R.id.text1);
            if (lblName != null){
                lblName.setText(names);
                lblName.setTextColor(ContextCompat.getColor(context, R.color.black));
            }
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return resultValue.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();

                // Here you can add your pattern like L* for starting string with 'L'
                for (String item : tempItems) {
//                    if (item.toLowerCase().contains((constraint.toString().toLowerCase()))) {
//                        suggestions.add(item);
//                    } else if(item.toLowerCase().contains(((String) constraint).split(" ",0)[1])){
//                        suggestions.add(item);
//                    }


                    if (item.toLowerCase().startsWith(constraint.toString().toLowerCase()) ||
                            item.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(item);
                    } else {
                        final String[] words = ((String) constraint).split(" ");
                        final int wordCount = words.length;
                        // Start at index 0, in case valueText starts with space(s)
                        for (int k = 0; k < wordCount; k++) {
                            if (words[k].startsWith(constraint.toString().toLowerCase()) || words[k].contains(constraint.toString().toLowerCase())) {
                                suggestions.add(item);
                                break;
                            } else if (item.toLowerCase().startsWith(words[1]) || item.toLowerCase().contains(words[1])) {
                                suggestions.add(words[0]+" "+item);
                                break;
                            }
                        }
                    }
                }



                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if(results.count > 0)
                addAll(((ArrayList<String>) results.values));
            else
                notifyDataSetInvalidated();

        }
    };
}