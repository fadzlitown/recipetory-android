package my.fadzlirazali.recipetory.ui.intro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.facebook.Profile;
import com.google.firebase.auth.FirebaseAuth;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.ui.BaseActivity;
import my.fadzlirazali.recipetory.ui.main.MainActivity;

public class SplashScreenActivity extends BaseActivity implements Runnable {
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(this, 1000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        launchFlowNormally(getIntent().getExtras());
    }

    private void launchFlowNormally(@Nullable Bundle extras) {
        Intent i = null;
        if (FirebaseAuth.getInstance().getCurrentUser()!=null || Profile.getCurrentProfile()!=null) {
            i = new Intent(this, MainActivity.class);
        } else {
            i = new Intent(this, IntroActivity.class);
        }
        if (extras != null) {
            i.putExtras(extras);
        }
        startActivity(i);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }
}
