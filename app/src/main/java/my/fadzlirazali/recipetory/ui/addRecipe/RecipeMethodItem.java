package my.fadzlirazali.recipetory.ui.addRecipe;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.xwray.groupie.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeMethodListItemBinding;
import my.fadzlirazali.recipetory.model.Method;

/**
 * Created by mohdfadzli on 7/11/2017.
 */

public class RecipeMethodItem extends Item<RecipeMethodListItemBinding> {

    private final AddRecipeActivity.IngradientListenerItem itemListener;

    /**
     * Use a LinkedHashMap and when you need to retrieve by position, convert the values into an ArrayList.
     *
     * */
    private ArrayList<Method> list;
    private int prevViewId;
    private boolean isMethodReached=false;

    public RecipeMethodItem(ArrayList<Method> list, AddRecipeActivity.IngradientListenerItem ingradientChangeListener) {
        this.list=list;
        this.itemListener = ingradientChangeListener;
    }


    @Override
    public int getPosition(Item item) {
        Log.d("fadzli", "getPosition: "+item.getId());
        return super.getPosition(item);
    }

    @Override
    public int getDragDirs() {
        return ViewDragHelper.DIRECTION_ALL ;
    }

    public ArrayList<Method> getList(){
        return list;
    }

    //https://stackoverflow.com/questions/4938626/moving-items-around-in-an-arraylist
    public void move(Context context, int fromPos, int toPos){
//        if(i > 0) {
//            Ingradient toMove = list.get(fromPos);
//            list.set(fromPos, arrayList.get(i-1));
//            list.set(i-1, toMove);
//        }
//        Collections.swap(list,fromPos,toPos);
//        Collections.rotate(list.subList(fromPos, toPos), 0);

        if (fromPos <= toPos) {
            Collections.rotate(list.subList(fromPos, toPos + 1), -1);
        } else {
            Collections.rotate(list.subList(toPos, fromPos + 1), 1);
        }
    }


    @Override
    public int getLayout() {
        return R.layout.recipe_method_list_item;
    }

    @Override
    public void bind(RecipeMethodListItemBinding vb, int position, List<Object> payloads) {
        super.bind(vb, position, payloads);
    }

    @Override
    public void bind(final RecipeMethodListItemBinding vb, final int position) {
        final int pos=position -(((AddRecipeActivity) vb.getRoot().getContext()).ingSectionSize()+1);
        vb.method.setId(pos);
        vb.method.setTag(pos);
        vb.itemLayout.setBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(), R.color.white));
        vb.deleteBtn.setVisibility(View.VISIBLE);
        //update UI by getting value from the list
        if(pos<list.size() && list.get(pos)!=null){
            if(!TextUtils.isEmpty(list.get(pos).getMethodName())){
                vb.method.setText(list.get(pos).getMethodName());
            } else {
                vb.method.setText("");
//                vb.method.requestFocus();
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    vb.method.setShowSoftInputOnFocus(true);
//                }
            }
            Log.d("fadzli", "bind: "+pos +" view "+vb.method.getText().toString()+" list item"+list.get(pos).getMethodName());
        }


//        setIngradientAC(vb.ingradient,vb.getRoot().getContext());


        vb.method.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable text) {
                // if edittext has 30chars & this is not called yet, add new line
                if(text.toString().length() == 40 && !isMethodReached) {
                    text.append(System.getProperty("line.separator"));
                    isMethodReached = true;
                }
                // if edittext has less than 10chars & boolean has changed, reset
                if(text.toString().length() < 40 && isMethodReached) isMethodReached = false;

                if(!vb.method.isFocusableInTouchMode()) vb.method.setFocusableInTouchMode(true);
                Log.d("fadzli", "afterTextChanged: pos"+pos+" list "+list.size()+" text length "+text.toString().length());
                if(pos<list.size() && vb.method.getTag().equals(pos)){
                    if(list.get(pos)!=null)
                        list.get(pos).setMethodName(vb.method.getText().toString());
                }
            }
        });

        //https://stackoverflow.com/questions/25391632/setting-nextfocus-for-edittexts-not-working
        vb.method.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d("fadzli",event+" event" +actionId +" actionId");
                if(actionId == EditorInfo.IME_ACTION_DONE){
//                    vb.ingradient.clearFocus();
                    // Gets the id of the view to use when the next focus
//                    if(pos+1  <list.size()){
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            vb.method.setShowSoftInputOnFocus(true);
//                            vb.method.setHint("Please add your method lah");
////                            Util.hideKeyboard(vb.ingradient);
//                        }
//                    }else
//                        if(list.size()==pos+1){
                        // enter new ingradient line
                        itemListener.onItemInserted();
                        v.requestFocus();
//                    }
                }
                return false;
            }
        });


        vb.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new AlertDialog.Builder(vb.getRoot().getContext())
                        .setTitle("Warning")
                        .setMessage("Are you sure want to delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                list.remove(pos);

                                //https://stackoverflow.com/questions/7100555/preventing-catching-illegalargumentexception-parameter-must-be-a-descendant-of
                                View currentFocus = ((AddRecipeActivity) v.getContext()).getCurrentFocus();
                                if(currentFocus!=null)
                                    currentFocus.clearFocus();

//                                list.remove((int) vb.ingradient.getTag()-1);
//                                itemListener.onItemRemoved((int) vb.ingradient.getTag()-1);
                                for (int i = 0; i < list.size(); i++) {
                                    if(i== pos){
                                        //https://stackoverflow.com/questions/10387290/how-to-get-position-of-key-value-in-linkedhashmap-using-its-key
                                        list.remove(i);
                                        itemListener.onItemRemoved(i);
                                        break;
                                    }
                                }

                                //remove based on view id
//                                for (int i = 0; i < list.size() ; i++){
//                                    if(list.get(i).getUserId().equals(vb.ingradient.getTag())){
//                                        list.remove(i);
//                                        itemListener.onItemRemoved(i);
//                                    }
//                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

    }

    public void setIngradientAC(AutoCompleteTextView v ,Context context){
//        String[] units = context.getResources().getStringArray(R.array.units);
        List<String> units = Arrays.asList(context.getResources().getStringArray(R.array.units));
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(units);

        CustomAdapter adapter = new CustomAdapter(context, android.R.layout.simple_list_item_1, v.getId() , arrayList);
        v.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
