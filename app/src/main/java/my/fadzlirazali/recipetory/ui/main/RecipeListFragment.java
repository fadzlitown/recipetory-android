package my.fadzlirazali.recipetory.ui.main;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.CacheUtil;
import my.dearfadz.util.EndlessRecyclerOnScrollListener;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.FragmentRecipeListBinding;
import my.fadzlirazali.recipetory.event.RecipeEvent;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.ui.addRecipe.AddRecipeActivity;

import static com.facebook.login.widget.ProfilePictureView.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeListFragment extends Fragment {

    private static final int RECIPE_SIZE = 20;
    private FragmentRecipeListBinding binding;
    private GroupAdapter mGroupAdapter;
    private OnEndlessOnScrollListener endlessListener;
    private DatabaseReference db;
    private ArrayList<Recipe> recipeDB;
    private long mNodeId;

    public RecipeListFragment() {
        // Required empty public constructor
    }

    public static RecipeListFragment newInstance() {
        return new RecipeListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recipe_list, container, false);
        View v = binding.getRoot();

        endlessListener = new OnEndlessOnScrollListener((LinearLayoutManager) binding.rv.getLayoutManager());
        mGroupAdapter = new GroupAdapter();
        binding.rv.addOnScrollListener(endlessListener);
        binding.rv.setAdapter(mGroupAdapter);

        //https://stackoverflow.com/questions/33208613/hide-floatingactionbutton-on-scroll-of-recyclerview
        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy >0) {
                    // Scroll Down
                    if (binding.addFab.isShown()) {
                        binding.addFab.hide();
                    }
                }
                else if (dy <0) {
                    // Scroll Up
                    if (!binding.addFab.isShown()) {
                        binding.addFab.show();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    binding.addFab.show();
//                   animateFab(binding.addFab,true);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        binding.addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRecipeActivity.start(getActivity());
            }
        });


        if(mGroupAdapter.getItemCount()==0){
            recipeDB = new ArrayList<Recipe>();
            queryFromFirebase();
            List<Item> itemList = new ArrayList<>();
            itemList.add(new FeedRecipeHeader());
            itemList.add(new FeedRecipeItem(recipeDB));
            mGroupAdapter.addAll(itemList);
        }

        binding.swipeContainer.setEnabled(true);
        binding.swipeContainer.setRefreshing(false);
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clearAdapterList();
                queryFromFirebase();
            }
        });

        endlessListener.setEnabled(true);

        return v;
    }

    private void clearAdapterList() {
        mGroupAdapter.remove(mGroupAdapter.getItem(1));
        mGroupAdapter.notifyItemRemoved(1);
        recipeDB = new ArrayList<Recipe>();
        mNodeId=0;
    }

    private void queryFromFirebase() {
        Query query;
        //todo 28 aug : handle the load more, change into single listener event : except for UX if like from detail page then go back.
        //used addListenerForSingleValueEvent instead, if  addValueEventListener, onDataChange will always called from listen to getting & pushing data into db
           if(mNodeId==0)
            query = db.child("recipe").orderByChild("createdBy").limitToFirst(RECIPE_SIZE);
           else
               query = db.child("recipe").orderByChild("createdBy").startAt(mNodeId).limitToFirst(RECIPE_SIZE);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    //Mapping data dari snapshot ke dlm object Recipe kita, & simpan primary key pd object recipe untuk handle edit & delete data
                    Recipe recipe = snapshot.getValue(Recipe.class);
                    if(recipe!=null){
                        recipe.setId(snapshot.getKey());
                        if(recipe.getCreatedBy()!=mNodeId) recipeDB.add(recipe);
                    }
                }

                mNodeId = recipeDB.get(recipeDB.size()-1).getCreatedBy();

                //Collections.reverse(recipeDB);
                endlessListener.hasMorePages(dataSnapshot.getChildrenCount() >= RECIPE_SIZE);
                endlessListener.onLoadMoreComplete();

                CacheUtil.getInstance().addAllRecipe(recipeDB);

                //if recipe is removed, add back else notify group item 1
                notifyGroupItem(recipeDB);
                binding.swipeContainer.setRefreshing(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "onCancelled: "+databaseError.getMessage());
                binding.swipeContainer.setRefreshing(false);
            }
        });
    }

    private void notifyGroupItem(ArrayList<Recipe> recipeList) {
        if(mGroupAdapter.getItemCount()==1){
            mGroupAdapter.add(new FeedRecipeItem(recipeList));
            mGroupAdapter.getItem(0).notifyChanged();
        } else mGroupAdapter.getItem(1).notifyChanged();
    }


    private class OnEndlessOnScrollListener extends EndlessRecyclerOnScrollListener {

        public OnEndlessOnScrollListener(LinearLayoutManager llm) {
            super(llm);
        }

        @Override
        public void onLoadMore() {
            if(isLoading()){
                binding.swipeContainer.setRefreshing(true);
                queryFromFirebase();
            }
        }

        @Override
        public void onLoadMoreComplete() {
            super.onLoadMoreComplete();
            //hide progress bar
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEventMainThread(RecipeEvent.OnNotifyRecipe item) {
        if (mGroupAdapter != null && mGroupAdapter.getItemCount() > 1 && item.getPos()!=-1) {
            recipeDB.set(item.getPos(),item.getRecipe());
            mGroupAdapter.onItemChanged(mGroupAdapter.getItem(1),item.getPos());
            //mGroupAdapter.notifyItemChanged(item.getPos()+1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onEventMainThread(RecipeEvent.OnNewPosted event) {
        clearAdapterList();
        queryFromFirebase();
    }
}
