package my.fadzlirazali.recipetory.ui.savedRecipe;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;

/**
 * Created by mohdfadzli on 26/1/2018.
 */

public class StaticValue {

    public static final String PATH_PRODUCT_REPORT = "/RECIPE/REPORT_PRODUCT/";
    public static Font FONT_BOOK_TITLE = new Font(Font.FontFamily.HELVETICA, 40, Font.BOLD, BaseColor.DARK_GRAY);
    public static Font FONT_BOOK_DESC = new Font(Font.FontFamily.HELVETICA, 30, Font.NORMAL, BaseColor.WHITE);
    public static Font FONT_BOOK_AUTHOR = new Font(Font.FontFamily.COURIER, 22, Font.ITALIC, BaseColor.WHITE);

    public static Font FONT_TITLE = new Font(Font.FontFamily.TIMES_ROMAN, 23,Font.BOLD,new BaseColor( 167, 218, 65,255));
    public static Font FONT_SUBTITLE = new Font(Font.FontFamily.TIMES_ROMAN, 16,Font.NORMAL);
    public static Font FONT_BODY = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.NORMAL);
    public static Font FONT_TABLE_HEADER = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD, BaseColor.WHITE);
    public static Font FONT_HEADER_FOOTER = new Font(Font.FontFamily.UNDEFINED, 7, Font.ITALIC);
}
