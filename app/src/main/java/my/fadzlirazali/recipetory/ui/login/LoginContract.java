package my.fadzlirazali.recipetory.ui.login;

import android.view.View;

public interface LoginContract {

    void onForgotPassword(View view);

    void onRegister(View view);

    void onLogin(View view);

    void onLoginFacebook(View v);

    void onLoginInstagram(View v);

}