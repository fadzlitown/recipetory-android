package my.fadzlirazali.recipetory.ui.searchRecipe;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import my.fadzlirazali.recipetory.R;

class SearchRecipeAdapter extends SimpleCursorAdapter {

    public SearchRecipeAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView recipeTv = ((TextView)view.findViewById(R.id.recipe_name));
        recipeTv.setText(cursor.getString(1));
    }
}
