package my.fadzlirazali.recipetory.ui.savedRecipe;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Section;

import org.greenrobot.greendao.query.Query;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import my.dearfadz.pdfwriter.PDFWriter;
import my.dearfadz.pdfwriter.PaperSize;
import my.dearfadz.pdfwriter.StandardFonts;
import my.fadzlirazali.recipetory.MyApplication;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivitySavedRecipeBinding;
import my.fadzlirazali.recipetory.orm.Recipe;
import my.fadzlirazali.recipetory.orm.RecipeDao;
import my.fadzlirazali.recipetory.ui.addRecipe.AddRecipeActivity;

public class SavedRecipeActivity extends AppCompatActivity {

    private ActivitySavedRecipeBinding binding;
    private GroupAdapter adapter;
    private Query<Recipe> jsonList;

    public static void start(Context context) {
        Intent intent = new Intent(context, SavedRecipeActivity.class);
        context.startActivity(intent);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if(adapter!=null) adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_saved_recipe);

        final RecipeDao recipeQuery = MyApplication.getInstance().getDaoSession().getRecipeDao();
        jsonList = recipeQuery.queryBuilder().build();

        adapter = new GroupAdapter();
        binding.rv.setAdapter(adapter);

        Section savedSection = new Section();
        SavedRecipeItem item = new SavedRecipeItem(jsonList.list(), new SavedRecipeListener() {

            @Override
            public void onRemove(Recipe item) {
                recipeQuery.delete(item);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onUpdate(Recipe item) {
                finish();
                final my.fadzlirazali.recipetory.model.Recipe recipe = my.fadzlirazali.recipetory.model.Recipe.fromJson(item.getRecipeJson());
                AddRecipeActivity.update(SavedRecipeActivity.this, item.getId(), recipe.toJson());
            }
        });
        savedSection.add(item);
        adapter.add(savedSection);
    }


    @WorkerThread
    public void OnGenerateIText(View view){

        //will be time consuming working task
        // update the content
//        final GeneratePdf pdf = new GeneratePdf(SavedRecipeActivity.this);
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                 pdf.OnGenerate("asdasd",jsonList);
//
//                if(pdf.getPdfFile()!=null){
//                    //https://stackoverflow.com/questions/17453105/android-open-pdf-file
//                    Intent target = new Intent(Intent.ACTION_VIEW);
//                    target.setDataAndType(Uri.fromFile( pdf.getPdfFile()),"application/pdf");
//                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//
//                    Intent intent = Intent.createChooser(target, "Open File");
//                    try {
//                        startActivity(intent);
//                    } catch (ActivityNotFoundException e) {
//                        Log.d("pdf", "outputToFile: "+e.getMessage());
//                        // Instruct the user to install a PDF reader here, or something
//                        Toast.makeText(SavedRecipeActivity.this, "Please install a PDF reader", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//        },2000*jsonList.list().size());

        // todo comment jap nk test generate recipe kat profile
    }

    private class GeneratePdfWorker implements Runnable{

        @Override
        public void run() {
            // update the content
//            GeneratePdf pdf = new GeneratePdf(SavedRecipeActivity.this);
//            boolean isGenerated = pdf.OnGenerate("asdasd",jsonList);
//
//            if(isGenerated && pdf.getPdfFile()!=null){
//                //https://stackoverflow.com/questions/17453105/android-open-pdf-file
//                Intent target = new Intent(Intent.ACTION_VIEW);
//                target.setDataAndType(Uri.fromFile( pdf.getPdfFile()),"application/pdf");
//                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//
//                Intent intent = Intent.createChooser(target, "Open File");
//                try {
//                    startActivity(intent);
//                } catch (ActivityNotFoundException e) {
//                    Log.d("pdf", "outputToFile: "+e.getMessage());
//                    // Instruct the user to install a PDF reader here, or something
//                    Toast.makeText(SavedRecipeActivity.this, "Please install a PDF reader", Toast.LENGTH_SHORT).show();
//                }
//            }

        }
    }

    public void OnGeneratePdf(View view) {
        //https://medium.com/@ssaurel/learn-to-generate-pdf-documents-for-your-android-application-8abd3de7fe3b

        int spaceLine = 20;
        int currentTotalHeight=0;
        int ingredientHeight=0;

        //a4 size in 72 dpi = 595 w and 842 h
        PDFWriter writer = new PDFWriter(PaperSize.FOLIO_WIDTH,PaperSize.FOLIO_HEIGHT);
        writer.setFont(StandardFonts.SUBTYPE, StandardFonts.TIMES_BOLD,StandardFonts.WIN_ANSI_ENCODING );

        /*title Pdf*/
        writer.addText(50, PaperSize.FOLIO_HEIGHT - 50 , 20, " Recipetory Cookbook");


        for (int i = 0; i < jsonList.list().size(); i++) {
            my.fadzlirazali.recipetory.model.Recipe recipe = my.fadzlirazali.recipetory.model.Recipe.fromJson(jsonList.list().get(i).getRecipeJson());

            /*title Recipe*/
            writer.addText(50, PaperSize.FOLIO_HEIGHT - 100 , 24, recipe.getTitle());

            /*desc Recipe*/
            writer.addText(50, PaperSize.FOLIO_HEIGHT - 130 , 21, recipe.getDesc());

            writer.addText(70, (PaperSize.FOLIO_HEIGHT - 180) , 19, "Ingredients");
            /*ingredient*/
            for (int j = 0; j < recipe.getIngredient().size(); j++) {
                writer.addText(70, (PaperSize.FOLIO_HEIGHT - 250) + (j*spaceLine) , 19, "- "+recipe.getIngredient().get(j).getIngradientName());
                ingredientHeight = ingredientHeight + (j*spaceLine)+5;
//                if(j==recipe.getIngredient().size()-1) currentTotalHeight = (PaperSize.FOLIO_HEIGHT - 250) + (j*spaceLine);
//                Log.d("pdf", "OnGeneratePdf: j"+currentTotalHeight);
            }

            writer.addText(70, PaperSize.FOLIO_HEIGHT - (210+ingredientHeight) , 19, "Methods");
            /*method*/
            for (int k = 0; k < recipe.getMethod().size(); k++) {
                writer.addText(70, (PaperSize.FOLIO_HEIGHT - (300+ingredientHeight) + (k*spaceLine)) , 19, "- "+recipe.getMethod().get(k).getMethodName());

                if(k==recipe.getMethod().size()-1) currentTotalHeight = 300+ingredientHeight + (k*spaceLine);
//                Log.d("pdf", "OnGeneratePdf: k"+currentTotalHeight);
            }

            if(recipe.getUriList().size()>1 && recipe.getUriList().get(0)!=null){
                Uri uri = Uri.parse(recipe.getUriList().get(0));
                Bitmap logo = null;
                Bitmap scaledLogo = null;
                try {
                    logo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    scaledLogo = Bitmap.createScaledBitmap(logo, 200, 200, false);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(scaledLogo!=null){
                    //            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.big_user_placeholder_icon);
                    // center images ...
                    int left = (PaperSize.FOLIO_WIDTH - scaledLogo.getWidth()) / 2;
                    writer.addImage(left, currentTotalHeight-200, scaledLogo);

//            left = (PaperSize.FOLIO_WIDTH - icon.getWidth()) / 2;
//            writer.addImage(left, PaperSize.FOLIO_HEIGHT - currentTotalHeight, icon);
                }
            }
            currentTotalHeight=0;
            ingredientHeight=0;
            writer.newPage();
        }

        outputToFile("MyRecipe.pdf", writer.asString(), "ISO-8859-1");
    }

    public interface SavedRecipeListener {
        void onRemove(Recipe recipe);
        void onUpdate(Recipe recipe);
    }

//http://codetheory.in/android-saving-files-on-internal-and-external-storage/
    public void outputToFile(String fileName, String pdfContent, String encoding) {
        File newFile = new File(Environment.getExternalStorageDirectory() + "/" + fileName);
        try {
            newFile.createNewFile();
            try {
                FileOutputStream pdfFile = new FileOutputStream(newFile);
                pdfFile.write(pdfContent.getBytes(encoding));
                pdfFile.close();
            } catch(FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }

        //https://stackoverflow.com/questions/17453105/android-open-pdf-file
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(newFile),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.d("pdf", "outputToFile: "+e.getMessage());
            // Instruct the user to install a PDF reader here, or something
            Toast.makeText(this, "Please install a PDF reader", Toast.LENGTH_SHORT).show();
        }
    }
}
