package my.fadzlirazali.recipetory.ui.main;

import android.support.annotation.NonNull;
import android.view.View;

import com.xwray.groupie.Item;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.IngradientListItemBinding;

/**
 * Created by mohdfadzli on 4/11/2017.
 */

public class IngradientItem extends Item<IngradientListItemBinding> {
    private final int[][] drawableList;
    private final String[] titleList;


    public IngradientItem(@NonNull int[][] ingDrawables, @NonNull String[] ingTitles) {
        drawableList = ingDrawables;
        titleList = ingTitles;
    }

    @Override
    public int getLayout() {
        return R.layout.ingradient_list_item;
    }

    @Override
    public void bind(final IngradientListItemBinding vb, final int position) {

        vb.ingIcon.setChecked(false);
        vb.ingIcon.setImageResource(drawableList[position][0]);
        vb.ingName.setText(titleList[position]);

        vb.ingCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vb.ingIcon.setChecked(!vb.ingIcon.isChecked());
                vb.ingName.setChecked(!vb.ingName.isChecked());

                if(vb.ingIcon.isChecked()){
                    vb.ingIcon.setImageResource(drawableList[position][1]);
                    vb.ingCard.setCardElevation(35);
                } else {
                    vb.ingIcon.setImageResource(drawableList[position][0]);
                    vb.ingCard.setCardElevation(4); //def value
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return titleList.length;
    }
}
