package my.fadzlirazali.recipetory.ui.profile;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.WorkerThread;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import my.dearfadz.util.EndlessRecyclerOnScrollListener;
import my.dearfadz.util.Util;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.FragmentUserRecipeBinding;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.ui.addRecipe.AddRecipeActivity;
import my.fadzlirazali.recipetory.ui.savedRecipe.GeneratePdf;
import my.fadzlirazali.recipetory.util.Constant;
import my.fadzlirazali.recipetory.util.ProfileSharedPref;

public class UserRecipeFragment extends Fragment {


    private static final String RECIPE_TYPE = "recipe_type";
    private static final String UID = "user_id";
    private FragmentUserRecipeBinding binding;
    private GroupAdapter groupAdapter;
    private OnEndlessOnScrollListener endlessListener;
    private int mRecipeType;
    private String mUID;
    private String _transitionName;
    private ArrayList<Recipe> mSelfRecipeList;

    public UserRecipeFragment() {
        // Required empty public constructor
    }

    public static UserRecipeFragment newInstance(int type, String mUID) {
        Bundle bundle = new Bundle();
        bundle.putInt(RECIPE_TYPE,type);
        bundle.putString(UID,mUID);
        UserRecipeFragment fragment = new UserRecipeFragment();
        fragment.setArguments(bundle);
        return  fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_recipe, container, false);
        View v = binding.getRoot();

        if(getArguments()!=null && getArguments().containsKey(RECIPE_TYPE) && getArguments().containsKey(UID)){
            mRecipeType = getArguments().getInt(RECIPE_TYPE);
            mUID = getArguments().getString(UID);
        }

        endlessListener = new OnEndlessOnScrollListener((LinearLayoutManager) binding.rv.getLayoutManager());
        groupAdapter = new GroupAdapter();
        binding.rv.addOnScrollListener(endlessListener);
        binding.rv.setAdapter(groupAdapter);
//        animateFab(binding.addFab,false);
        //https://stackoverflow.com/questions/33208613/hide-floatingactionbutton-on-scroll-of-recyclerview
        binding.rv.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if (dy >0) {
                    // Scroll Down
                    if (binding.addFab.isShown()) {
                        binding.addFab.hide();
                    }
                }
                else if (dy <0) {
                    // Scroll Up
                    if (!binding.addFab.isShown()) {
                        binding.addFab.show();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState)
            {
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    binding.addFab.show();
//                   animateFab(binding.addFab,true);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        binding.addFab.setVisibility(mUID.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()) ? View.VISIBLE : View.GONE);
        binding.addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddRecipeActivity.start(getActivity());
            }
        });
        binding.generatePDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.isListShown(binding.rv,binding.progress,false,true);
                OnGenerateIText();
            }
        });

        callNetwork(false);
        binding.swipeContainer.setEnabled(true);
        binding.swipeContainer.setRefreshing(false);
        endlessListener.setEnabled(true);
//        endlessListener.hasMorePages(list.size() >= TAKE_LIMIT);
        endlessListener.onLoadMoreComplete();
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callNetwork(true);
            }
        });
        return v;
    }


    @WorkerThread
    public void OnGenerateIText(){

        //will be time consuming working task
        // update the content
        final GeneratePdf pdf = new GeneratePdf(getActivity());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pdf.OnGenerate("asdasd", mSelfRecipeList);

                if(pdf.getPdfFile()!=null){
                    Util.isListShown(binding.rv,binding.progress,true,true);

                    //https://stackoverflow.com/questions/17453105/android-open-pdf-file
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile( pdf.getPdfFile()),"application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                    Intent intent = Intent.createChooser(target, "Open File");
                    try {

                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Log.d("pdf", "outputToFile: "+e.getMessage());
                        // Instruct the user to install a PDF reader here, or something
                        Toast.makeText(getActivity(), "Please install a PDF reader", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        },1000* mSelfRecipeList.size());


//        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//        ScheduledFuture<?> future = executor.scheduleAtFixedRate(new GeneratePdfWorker(),0,1, TimeUnit.MINUTES);
//        future.cancel(true);
//        executor.shutdown();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setTransitionName(String transitionName) {
        _transitionName = transitionName;
    }

    public void callNetwork(final boolean isRefresh){

        Query query;

        if(mRecipeType==Constant.RecipeType.SELF)
            query = FirebaseDatabase.getInstance().getReference().child("recipe").orderByChild("userId").equalTo(mUID);
        else
            query = FirebaseDatabase.getInstance().getReference().child("bookmarks").child(mUID);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(isRefresh && groupAdapter.getItemCount()>0)
                            groupAdapter.clear();

                        int totalRecooked = 0;
                        ArrayList<Recipe> bookmarkRecipeList = new ArrayList<>();
                        mSelfRecipeList = new ArrayList<>();
                        for (DataSnapshot data :dataSnapshot.getChildren()) {
                            HashMap<String, Object> map = ((HashMap<String, Object>) data.getValue());
//                            Recipe recipe = new Recipe();
//                            recipe.setId(data.getKey());

                            Recipe recipe = data.getValue(Recipe.class);

//                            if(map!=null && map.containsKey("title")) {
//                                recipe.setTitle((String)map.get("title"));
//                            }
//                            if(map!=null && map.containsKey("smallPhoto")) {
//                                recipe.setSmallPhoto((String)map.get("smallPhoto"));
//                            }
//                            if(map!=null && map.containsKey("hasCooked")) {
//                                boolean recooked = (boolean)map.get("hasCooked");
//                                recipe.setHasCooked(recooked);
//                                if(recooked) totalRecooked++;
//                            }
//
//                            recipes.add(recipe);

                            if(mRecipeType==Constant.RecipeType.SELF){
                                if(recipe!=null){
                                    recipe.setId(data.getKey());
                                    mSelfRecipeList.add(recipe);
                                }
                            } else {
                                if(recipe!=null){
                                    recipe.setId(data.getKey());
                                    bookmarkRecipeList.add(recipe);
                                    totalRecooked++;
                                }
                            }
                        }
                        Collections.reverse(mRecipeType==Constant.RecipeType.SELF ? mSelfRecipeList : bookmarkRecipeList);
                        List<Item> itemList = new ArrayList<>();
                        itemList.add(new FeedRecipeSmallItem(mRecipeType, mRecipeType==Constant.RecipeType.SELF ? mSelfRecipeList : bookmarkRecipeList));
                        groupAdapter.addAll(itemList);

                        /* ProfileEvent SP update */
                        if(mRecipeType==Constant.RecipeType.SELF)
                            ProfileSharedPref.setTotalRecipe(getActivity(),String.valueOf(mSelfRecipeList.size()));
                        else if(mRecipeType==Constant.RecipeType.BOOKMARK)
                            ProfileSharedPref.setTotalRecooked(getActivity(),String.valueOf(totalRecooked));

                        if(binding.swipeContainer.isRefreshing())
                            binding.swipeContainer.setRefreshing(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
    }





    private class OnEndlessOnScrollListener extends EndlessRecyclerOnScrollListener {

        public OnEndlessOnScrollListener(LinearLayoutManager llm) {
            super(llm);
        }

        @Override
        public void onLoadMore() {
            if(!isLoading()){
                /*                isLoading = true;
                //TODO
                SKIP = groupAdapter.getItemCount(); - itemHeader
                callUpcomingJob(SKIP, TAKE);*/
//                groupAdapter.add(loadingItem);
            }
        }

        @Override
        public void onLoadMoreComplete() {
            super.onLoadMoreComplete();
            //hide progress bar
        }
    }



}
