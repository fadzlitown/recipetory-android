package my.fadzlirazali.recipetory.ui.main;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import my.fadzlirazali.recipetory.R;

/**
 * Created by mohdfadzli on 23/10/2017.
 */

public class RecipeAdapter extends RecyclerView.Adapter<RecipeCardBindingHolder> {

    private final FragmentManager fm;
    private final ArrayList<String> list;

    public RecipeAdapter(FragmentManager fm, ArrayList<String> list) {
        this.fm = fm;
        this.list=list;
    }

    @Override
    public RecipeCardBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recipe_list_item,parent,false);
        return new RecipeCardBindingHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipeCardBindingHolder holder, int position) {
        String title = list.get(position);
        holder.setTitle(title);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
