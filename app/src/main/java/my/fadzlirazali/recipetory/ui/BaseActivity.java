package my.fadzlirazali.recipetory.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.birbit.android.jobqueue.TagConstraint;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;
import java.util.UUID;

import my.fadzlirazali.recipetory.MyApplication;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.event.ExceptionEvent;
import my.fadzlirazali.recipetory.job.JobSession;
import my.fadzlirazali.recipetory.model.MyException;
import my.fadzlirazali.recipetory.util.UserUtil;

public class BaseActivity extends AppCompatActivity implements JobSession {

    private Toolbar mActionBarToolbar;

    // a random string identifier that is generated when Activity becomes visible
    private String sessionId;
    private boolean isRegisteredOnStart;


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        getActionBarToolbar();
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionId = UUID.randomUUID().toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
            isRegisteredOnStart = true;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isRegisteredOnStart && EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onDestroy() {
        // this MUST be async to avoid blocking the main thread
        MyApplication.getInstance().getJobManager().cancelJobsInBackground(null, TagConstraint.ANY, sessionId);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (UserUtil.isLogin(this)) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
//                SettingActivity.start(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void logout() {
//        new AlertDialog.Builder(this)
//                .setMessage(R.string.confirm_logout)
//                .setNegativeButton(android.R.string.cancel, null)
//                .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                        MyApplication.addJobInBackground(new LogoutJob(UserUtil.getToken(BaseActivity.this)));
//                        UserUtil.resetUser(BaseActivity.this);
////                        LoginActivity.makeActivityRestartTask(BaseActivity.this);
//                    }
//                })
//                .show();
    }

    @Nullable
    protected Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
            if (mActionBarToolbar != null) {
                setSupportActionBar(mActionBarToolbar);
            }
        }
        return mActionBarToolbar;
    }


    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ExceptionEvent event) {
        Exception exception = event.getException();
        if (exception != null) {
            if (exception instanceof MyException) {
                int errorCode = ((MyException) exception).getError().getErrorCode();
                switch (errorCode) {
                    case 5:
                        if (((MyException) exception).getError().getErrorMessage().toLowerCase(Locale.ENGLISH).contains("token")) {
//                            UserUtil.resetUser(this);
//                            PrefUtil.reset(this);
//                            LoginActivity.makeRestartActivityTask(this);
                            return;
                        }
                }
            }
            String msg = exception.getMessage();
            if (TextUtils.isEmpty(msg)) {
                msg = getString(R.string.unexpected_error);
            }
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }


}
