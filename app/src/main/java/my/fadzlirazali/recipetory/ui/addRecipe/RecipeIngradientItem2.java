package my.fadzlirazali.recipetory.ui.addRecipe;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.xwray.groupie.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeIngradientListItemBinding;
import my.fadzlirazali.recipetory.model.Ingradient;

/**
 * Created by mohdfadzli on 7/11/2017.
 */

public class RecipeIngradientItem2 extends Item<RecipeIngradientListItemBinding> {

    private final AddRecipeActivity.IngradientListenerItem itemListener;

    /**
     * Use a LinkedHashMap and when you need to retrieve by position, convert the values into an ArrayList.
     *
     * */
    private ArrayList<Ingradient> list;
    private int prevViewId;

    public RecipeIngradientItem2(ArrayList<Ingradient> list, AddRecipeActivity.IngradientListenerItem ingradientChangeListener) {
        this.list=list;
        this.itemListener = ingradientChangeListener;
    }


    @Override
    public int getPosition(Item item) {
        Log.d("fadzli", "getPosition: "+item.getId());
        return super.getPosition(item);
    }

    @Override
    public int getDragDirs() {
        return ViewDragHelper.DIRECTION_ALL ;
    }

    @Override
    public Item getItem(int position) {

        return super.getItem(position);
    }

    public ArrayList<Ingradient> getList(){
        return list;
    }

    //https://stackoverflow.com/questions/4938626/moving-items-around-in-an-arraylist
    public void move(int fromPos, int toPos){
//        if(i > 0) {
//            Ingradient toMove = list.get(fromPos);
//            list.set(fromPos, arrayList.get(i-1));
//            list.set(i-1, toMove);
//        }
//        Collections.swap(list,fromPos,toPos);
//        Collections.rotate(list.subList(fromPos, toPos), 0);

        if (fromPos <= toPos) {
            Collections.rotate(list.subList(fromPos, toPos + 1), -1);
        } else {
            Collections.rotate(list.subList(toPos, fromPos + 1), 1);
        }
    }


    @Override
    public int getLayout() {
        return R.layout.recipe_ingradient_list_item;
    }

    @Override
    public void bind(RecipeIngradientListItemBinding vb, int position, List<Object> payloads) {
        super.bind(vb, position, payloads);
    }

    @Override
    public void bind(final RecipeIngradientListItemBinding vb, final int position) {
       final int pos=position-1;
        vb.ingradient.setId(pos);
        vb.ingradient.setTag(pos);
        vb.itemLayout.setBackgroundColor(ContextCompat.getColor(vb.getRoot().getContext(), R.color.white));
        vb.deleteBtn.setVisibility(View.VISIBLE);
        //update UI by getting value from the list
        if(list.get(pos)!=null){
            if(!TextUtils.isEmpty(list.get(pos).getIngradientName())){
                vb.ingradient.setText(list.get(pos).getIngradientName());
            } else {
                vb.ingradient.setText("");
                //vb.ingradient.requestFocus();
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    vb.ingradient.setShowSoftInputOnFocus(true);
//                }
            }
        }
        Log.d("fadzli", "bind: "+pos +" view "+vb.ingradient.getText().toString()+" list item"+list.get(pos).getIngradientName());

        setIngradientAC(vb.ingradient,vb.getRoot().getContext());


        vb.ingradient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(!vb.ingradient.isFocusableInTouchMode()) vb.ingradient.setFocusableInTouchMode(true);
                Log.d("fadzli", "afterTextChanged: pos"+pos+" list "+list.size());
                if(pos<list.size() && vb.ingradient.getTag().equals(pos)){
                    if(list.get(pos)!=null)
                        list.get(pos).setIngradientName(vb.ingradient.getText().toString());
                }
            }
        });

        //https://stackoverflow.com/questions/25391632/setting-nextfocus-for-edittexts-not-working
        vb.ingradient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d("fadzli",event+" event" +actionId +" actionId");
                if(actionId == EditorInfo.IME_ACTION_NEXT){
//                    vb.ingradient.clearFocus();
                    // Gets the id of the view to use when the next focus
//                    if(pos+1 <list.size()){
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                            vb.ingradient.setShowSoftInputOnFocus(true);
//                            v.setHint("Please add your ingredient lah");
////                            Util.hideKeyboard(vb.ingradient);
//                        }
//                    } else
                        if(list.size()==pos+1){
                        // enter new ingradient line
                        itemListener.onItemInserted();
                        v.requestFocus();
                    }
                }
                return false;
            }
        });


        vb.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new AlertDialog.Builder(vb.getRoot().getContext())
                        .setTitle("Warning")
                        .setMessage("Are you sure want to delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                list.remove(pos);

                                //https://stackoverflow.com/questions/7100555/preventing-catching-illegalargumentexception-parameter-must-be-a-descendant-of
                                View currentFocus = ((AddRecipeActivity) v.getContext()).getCurrentFocus();
                                if(currentFocus!=null)
                                    currentFocus.clearFocus();

//                                list.remove((int) vb.ingradient.getTag()-1);
//                                itemListener.onItemRemoved((int) vb.ingradient.getTag()-1);
                                for (int i = 0; i < list.size(); i++) {
                                    if(i== pos){
                                        //https://stackoverflow.com/questions/10387290/how-to-get-position-of-key-value-in-linkedhashmap-using-its-key
                                        list.remove(i);
                                        itemListener.onItemRemoved(i);
                                        break;
                                    }
                                }

                                //remove based on view id
//                                for (int i = 0; i < list.size() ; i++){
//                                    if(list.get(i).getUserId().equals(vb.ingradient.getTag())){
//                                        list.remove(i);
//                                        itemListener.onItemRemoved(i);
//                                    }
//                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

    }

    public void setIngradientAC(AutoCompleteTextView v ,Context context){
//        String[] units = context.getResources().getStringArray(R.array.units);
        List<String> units = Arrays.asList(context.getResources().getStringArray(R.array.units));
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(units);

        CustomAdapter adapter = new CustomAdapter(context, android.R.layout.simple_list_item_1, v.getId() , arrayList);
        v.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
