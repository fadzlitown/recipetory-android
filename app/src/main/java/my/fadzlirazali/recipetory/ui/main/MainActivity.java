package my.fadzlirazali.recipetory.ui.main;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import my.dearfadz.util.CircleTransform;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityMainBinding;
import my.fadzlirazali.recipetory.ui.BaseActivity;
import my.fadzlirazali.recipetory.ui.login.LoginActivity;
import my.fadzlirazali.recipetory.ui.profile.UserProfileActivity;
import my.fadzlirazali.recipetory.ui.savedRecipe.SavedRecipeActivity;
import my.fadzlirazali.recipetory.ui.searchRecipe.SearchRecipeActivity;
import my.fadzlirazali.recipetory.util.ProfileSharedPref;

public class MainActivity extends BaseActivity {

    private static final String PROVIDER = "provider";
    private ActivityMainBinding binding;
    private int maxVerticalOffset;
    private boolean hasAnimated=false;

    int[] iconsUnSelected = new int[]{R.drawable.recipe_inactive, R.drawable.food_serving_inactive};
    int[] iconsSelected = new int[]{R.drawable.recipe_active, R.drawable.food_serving_active};
    private final String[] title = {"Recipe", "Ingradient"};

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setToolbar();
        setCollapsingToolbar();
        setNavDrawer();
        setViewPager();
        initEmailProfile();

        SearchManager searchManager = ((SearchManager) getSystemService(Context.SEARCH_SERVICE));
        binding.searchViewToolbar.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        binding.searchView.setIconifiedByDefault(false);
        binding.searchView.setQueryHint("nak masak apa tu?");
        binding.searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchRecipeActivity.start(MainActivity.this,binding.searchView.getQuery().toString());
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.searchViewToolbar.setNestedScrollingEnabled(true);
            binding.searchViewToolbar.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SearchRecipeActivity.start(MainActivity.this,binding.searchViewToolbar.getQuery().toString());
                }
            });
        }


        binding.appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.d("appbar", "onOffsetChanged: "+verticalOffset);
//                Log.d("appbar", "getTotalScrollRange: "+appBarLayout.getTotalScrollRange());
                if(Math.abs(verticalOffset)==appBarLayout.getTotalScrollRange()){
//                    animateFab(binding.addFab,true);
                    maxVerticalOffset=appBarLayout.getTotalScrollRange();
                    hasAnimated=false;
                    binding.toolbar.setContentInsetsAbsolute(0,0);
                    binding.toolbar.setContentInsetsRelative(0,0);
                    binding.toolbar.setContentInsetStartWithNavigation(0);
                    binding.searchViewToolbar.setVisibility(View.VISIBLE);
                    Log.d("appbar", "Math.abs(verticalOffset)== Math.abs(verticalOffset): "+Math.abs(verticalOffset)+"=="+appBarLayout.getTotalScrollRange());

                } else if(Math.abs(verticalOffset)>appBarLayout.getTotalScrollRange()){  // Collapsed
                    Log.d("appbar", "Math.abs(verticalOffset)> Math.abs(verticalOffset): "+Math.abs(verticalOffset)+">"+appBarLayout.getTotalScrollRange());
                }
                else if (Math.abs(verticalOffset)<50) {   //Expanded
                    if(!hasAnimated) {
//                        animateFab(binding.addFab,false);
                        binding.appBar.setExpanded(false, true);
                        hasAnimated = true;
                    }
                    binding.searchViewToolbar.setVisibility(View.GONE);
                    binding.toolbar.setContentInsetsAbsolute(binding.toolbar.getWidth()/3,0);
                    binding.toolbar.setContentInsetsRelative(binding.toolbar.getWidth()/3,0);
                    Log.d("appbar", "Math.abs(verticalOffset)==0: "+Math.abs(verticalOffset)+"==0");
                }
                else if(appBarLayout.getTotalScrollRange()>Math.abs(verticalOffset)) {
                    Log.d("appbar", "appBarLayout.getTotalScrollRange()> Math.abs(verticalOffset): "+appBarLayout.getTotalScrollRange()+">"+Math.abs(verticalOffset));

                    if(!hasAnimated){
                        if(Math.abs(verticalOffset)<maxVerticalOffset-50){
//                            animateFab(binding.addFab,false);
                            binding.appBar.setExpanded(true,true);
                            hasAnimated =true;
                        }
                        binding.searchViewToolbar.setVisibility(View.GONE);
                        binding.toolbar.setContentInsetsAbsolute(binding.toolbar.getWidth()/3,0);
                        binding.toolbar.setContentInsetsRelative(binding.toolbar.getWidth()/3,0);
                    }
                }
            }
        });
    }

    private void initEmailProfile() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        binding.greeting.setText("Good Morning, "+user.getDisplayName());

        LinearLayout ll = (LinearLayout)binding.navView.getHeaderView(0);

        if(user.getPhotoUrl()!=null){
            Glide.with(this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(user.getPhotoUrl())
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher)
                    .transform(new CircleTransform(this))
                    .into(((ImageView) ll.getChildAt(0)));
        }
        ((TextView)  ll.getChildAt(1)).setText(user.getDisplayName());
        ((TextView)  ll.getChildAt(2)).setText(user.getEmail());
    }


    private void setViewPager() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(RecipeListFragment.newInstance());
        fragments.add(IngradientFragment.newInstance());
        MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(),fragments);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if(position==0) binding.appBar.setExpanded(true,true);
                else if(position==1) binding.appBar.setExpanded(false,true);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        binding.tabs.setupWithViewPager(binding.viewpager);
        if(binding.tabs.getTabAt(0)!=null) binding.tabs.getTabAt(0).setIcon(iconsSelected[0]);
        if(binding.tabs.getTabAt(1)!=null) binding.tabs.getTabAt(1).setIcon(iconsUnSelected[1]);
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.tabs.getTabAt(tab.getPosition()).setIcon(iconsSelected[tab.getPosition()]);
                binding.viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                binding.tabs.getTabAt(tab.getPosition()).setIcon(iconsUnSelected[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_filter:
                return true;
            case R.id.action_profile:
                UserProfileActivity.start(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setNavDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        binding.navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        break;
                    case R.id.nav_saved_recipe:
                        SavedRecipeActivity.start(MainActivity.this);
                        break;
                    case R.id.nav_logout:
                        ProfileSharedPref.setProfile(MainActivity.this,null);
                        FirebaseAuth.getInstance().signOut();
                        LoginManager.getInstance().logOut();
                        LoginActivity.makeActivityRestartTask(MainActivity.this);
                }
                return true;
            }
        });
    }

    private void setCollapsingToolbar() {
        binding.collapsingToolbar.setTitleEnabled(false);
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(200);

            TextView textView = new TextView(this);
            textView.setText("Recipetory");
            textView.setTextSize(20);
            textView.setTypeface(null, Typeface.BOLD);
            textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setTextColor(ContextCompat.getColor(this,R.color.black));
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(textView);
        }
    }

    public void animateFab(View v,boolean isShow){
        ScaleAnimation anim = new ScaleAnimation(0, isShow ? 1: 0,0,isShow ? 1: 0);
        anim.setFillBefore(true);
        anim.setFillAfter(true);
        anim.setFillEnabled(true);
        anim.setDuration(800);
        anim.setInterpolator(getInterpolator());
        v.startAnimation(anim);

    }

    private static final int FAB_ANIM_DURATION = 200;

    public void hide(View v) {
        // Only use scale animation if FAB is visible
        if (v.getVisibility() == View.VISIBLE) {
            // Pivots indicate where the animation begins from
            float pivotX = v.getPivotX() + v.getTranslationX();
            float pivotY = v.getPivotY() + v.getTranslationY();

            // Animate FAB shrinking
            ScaleAnimation anim = new ScaleAnimation(1, 0, 1, 0, pivotX, pivotY);
            anim.setDuration(FAB_ANIM_DURATION);
            anim.setInterpolator(getInterpolator());
            v.startAnimation(anim);
        }
        v.setVisibility(View.INVISIBLE);
    }


    public void show(View v) {
        show(v,0, 30);
    }

    public void show(View v, float translationX, float translationY) {

        // Set FAB's translation
        setTranslation(v, translationX, translationY);

        // Only use scale animation if FAB is hidden
        if (v.getVisibility() != View.VISIBLE) {
            // Pivots indicate where the animation begins from
            float pivotX = v.getPivotX() + translationX;
            float pivotY = v.getPivotY() + translationY;

            ScaleAnimation anim;
            // If pivots are 0, that means the FAB hasn't been drawn yet so just use the
            // center of the FAB
            if (pivotX == 0 || pivotY == 0) {
                anim = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);
            } else {
                anim = new ScaleAnimation(0, 1, 0, 1, pivotX, pivotY);
            }

            // Animate FAB expanding
            anim.setDuration(FAB_ANIM_DURATION);
            anim.setInterpolator(getInterpolator());
            v.startAnimation(anim);
        }
        v.setVisibility(View.VISIBLE);
    }

    private void setTranslation(View v, float translationX, float translationY) {
        v.animate().setInterpolator(getInterpolator()).setDuration(FAB_ANIM_DURATION)
                .translationX(translationX).translationY(translationY);
    }

    private Interpolator getInterpolator() {
        return AnimationUtils.loadInterpolator(this,  android.R.anim.bounce_interpolator);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== UserProfileActivity.UPDATE_PROFILE && resultCode==RESULT_OK){
            initEmailProfile();
        }
    }
}
