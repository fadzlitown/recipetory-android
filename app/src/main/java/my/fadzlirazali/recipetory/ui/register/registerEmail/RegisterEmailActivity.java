package my.fadzlirazali.recipetory.ui.register.registerEmail;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.HashMap;
import java.util.Map;

import my.dearfadz.util.CircleTransform;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityRegisterEmailBinding;
import my.fadzlirazali.recipetory.model.Profile;
import my.fadzlirazali.recipetory.ui.BaseActivity;

public class RegisterEmailActivity extends BaseActivity {

    private static final String TAG = RegisterEmailActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_IMAGE = 1;
    private static final int PERMISSION_READ_EXTERNAL_STORAGE = 2;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ActivityRegisterEmailBinding binding;
    private Uri mUri;
    private String locationCode;
    private HashMap<String, String> mCountryMaps;

    public static void start(Context context) {
        Intent intent = new Intent(context, RegisterEmailActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_register_email);

        /** Init fiarebase auth */
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
        loadCountry();
    }


    public void OnSignUp(View view) {
        if(TextUtils.isEmpty(binding.etEmail.getText().toString()) || TextUtils.isEmpty(binding.etPassword.getText().toString()) || TextUtils.isEmpty(binding.etFullname.getText().toString()) || TextUtils.isEmpty(binding.location.getText().toString())){
            Toast.makeText(this, "Please fill in the fields", Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.createUserWithEmailAndPassword(binding.etEmail.getText().toString(), binding.etPassword.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        /**
                         * Jika sign in gagal, tampilkan pesan ke user. Jika sign in sukses
                         * maka auth state listener akan dipanggil dan logic untuk menghandle
                         * signed in user bisa dihandle di listener.
                         */
                        if (!task.isSuccessful()) {
                            if(task.getException()!=null){
                                Toast.makeText(RegisterEmailActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            updateProfile(task.getResult().getUser());
                        }
                    }
                });
    }

    private void updateProfile(FirebaseUser user) {
        UserProfileChangeRequest.Builder builder = new UserProfileChangeRequest.Builder();
        builder.setDisplayName(binding.etFullname.getText().toString());    //update fullname & userPhoto
        if (mUri != null) builder.setPhotoUri(mUri);
        UserProfileChangeRequest profileUpdates = builder.build();
        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(RegisterEmailActivity.this, "Proses Kemaskini profil Berhasil\n" + "Email "+binding.etEmail.getText().toString(), Toast.LENGTH_SHORT).show();
                            if(FirebaseAuth.getInstance().getCurrentUser()!=null)
                                updateFirebaseProfileInfo(FirebaseAuth.getInstance().getCurrentUser());
                        }
                    }
                });
    }

    private void updateFirebaseProfileInfo(FirebaseUser currentUser) {
        Profile profile = new Profile();
        profile.setFullname(binding.etFullname.getText().toString());
        profile.setPhotoThumbnail(currentUser.getPhotoUrl()!=null ? currentUser.getPhotoUrl().toString() : "");
        profile.setLocation(locationCode);
        FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("profile").setValue(profile).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                finish();
                Toast.makeText(RegisterEmailActivity.this, "Your profile has been updated", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadCountry() {
        Ion.with(this)
                .load("http://country.io/names.json")
                .addHeader("Content-Type", "application/json")
                .asString()
                .withResponse().setCallback(new FutureCallback<Response<String>>() {
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                if(result.getHeaders().code()==200){
                    mCountryMaps = new Gson().fromJson(result.getResult(), new TypeToken<HashMap<String, String>>(){}.getType());

                    //if location stored before
                    if(locationCode!=null && mCountryMaps!=null){
                        for (Map.Entry<String,String> map:  mCountryMaps.entrySet()) {
                            if(locationCode.equals(map.getKey())){
                                binding.location.setText(map.getValue());
                            }
                        }
                    }
                    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(RegisterEmailActivity.this,android.R.layout.simple_list_item_1, mCountryMaps.values().toArray(new String[mCountryMaps.size()]));
                    binding.location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            OnDialogList(adapter,binding.location);
                        }
                    });
                } else {
                    if(result.getException().getMessage()!=null)
                        Toast.makeText(RegisterEmailActivity.this, result.getException().getMessage() , Toast.LENGTH_SHORT).show();
                }
                //Util.isListShown(binding.scrollView,binding.progress,true,true);
            }
        });
    }

    private void OnDialogList(final ArrayAdapter<String> adapter, final TextView location) {
        AlertDialog.Builder builder =  new AlertDialog.Builder(this);
        builder.setTitle("Select Country");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String item = adapter.getItem(which);
                location.setText(item);
                for (Map.Entry<String,String> map:  mCountryMaps.entrySet()) {
                    if(item!=null && item.equals(map.getValue())){
                        locationCode = map.getKey();        //update the latest clicked locationCode
                    }
                }
                Log.d("test", "onClick: "+locationCode+" "+item);
                dialogInterface.dismiss();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    public void onProfilePicClick(View view) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_READ_EXTERNAL_STORAGE);
        else {
            pickImage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        }
    }

    private void pickImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK_IMAGE) {
                mUri = data.getData();
                Glide.with(this)
                        .loadFromMediaStore(data.getData())
                        .transform(new CircleTransform(this))
                        .into(binding.userPhoto);
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}
