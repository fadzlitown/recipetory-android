package my.fadzlirazali.recipetory.ui.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import agency.tango.materialintroscreen.SlideFragment;
import my.fadzlirazali.recipetory.R;

public class IntroCustomSlide extends SlideFragment {

    public static final String IMAGE_ARG = "image_id";
    public static final String TITLE_ARG = "title";
    public static final String SUBTITLE_ARG = "subtitle";

    public IntroCustomSlide(){
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_slide, container, false);
        ImageView imageView = ((ImageView) view.findViewById(R.id.image_slide));
        TextView title = ((TextView) view.findViewById(R.id.title));
        TextView subtitle = ((TextView) view.findViewById(R.id.subtitle));

        if(getArguments().containsKey(IMAGE_ARG) && getArguments().containsKey(TITLE_ARG) && getArguments().containsKey(SUBTITLE_ARG)){
            if(getArguments().getInt(IMAGE_ARG)!=0)
                imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),getArguments().getInt(IMAGE_ARG)));

            if(!TextUtils.isEmpty(getArguments().getString(TITLE_ARG)))
                title.setText((getArguments().getString(TITLE_ARG)));

            if(!TextUtils.isEmpty(getArguments().getString(SUBTITLE_ARG)))
                subtitle.setText((getArguments().getString(SUBTITLE_ARG)));

        }
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.white;
    }

    @Override
    public int buttonsColor() {
        return R.color.darkGreen;
    }

    @Override
    public boolean canMoveFurther() {
        return true;
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
        return "";
    }
}