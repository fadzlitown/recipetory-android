package my.fadzlirazali.recipetory.ui.profile;

import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.xwray.groupie.Item;

import java.util.ArrayList;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeSmallListItemBinding;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.ui.RecipeDetailActivity;
import my.fadzlirazali.recipetory.util.Constant;

/**
 * Created by mohdfadzli on 2/11/2017.
 */

public class FeedRecipeSmallItem extends Item<RecipeSmallListItemBinding> {

    private final int type;
    private final ArrayList<Recipe> recipes;

    public FeedRecipeSmallItem(int mRecipeType, ArrayList<Recipe> recipes) {
        this.type=mRecipeType;
        this.recipes=recipes;
    }

    @Override
    public int getLayout() {
        return R.layout.recipe_small_list_item;
    }

    @Override
    public void bind(final RecipeSmallListItemBinding vb, int position) {
        final Recipe recipe =recipes.get(position);

        vb.name.setText(recipe.getTitle());
        vb.photo.setImageResource(R.color.black);

        if(type==Constant.RecipeType.BOOKMARK && !TextUtils.isEmpty(recipe.getSmallPhoto())){
            Glide.with(vb.getRoot().getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getSmallPhoto())
                    .centerCrop()
                    .error(R.color.black)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(vb.photo);
        } else if(recipe.getPathList()!=null && recipe.getPathList().size()>0){
            Glide.with(vb.getRoot().getContext())  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getPathList().get(0))
                    .centerCrop()
                    .error(R.color.black)
                    .placeholder(R.mipmap.ic_launcher)
                    .into(vb.photo);
        }
        vb.photo.invalidate();

        vb.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecipeDetailActivity.startOwnedByRecipeId(v.getContext(), recipe.getId(), vb.photo, vb.name);
            }
        });

        if(type== Constant.RecipeType.SELF) vb.recipeTagged.setVisibility(View.INVISIBLE);
        else {
            vb.recipeTagged.setVisibility(View.VISIBLE);
            vb.recipeTagged.setText(recipe.isHasCooked() ?"Cooked" : "Not Yet Cook");
            vb.recipeTagged.setBackgroundResource(recipe.isHasCooked() ? R.color.darkGreen : R.color.red_heart);
        }

    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }
}
