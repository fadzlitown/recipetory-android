package my.fadzlirazali.recipetory.ui.login;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Collections;

import my.dearfadz.util.Util;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityLoginBinding;
import my.fadzlirazali.recipetory.ui.BaseActivity;
import my.fadzlirazali.recipetory.ui.ForgotPasswordActivity;
import my.fadzlirazali.recipetory.ui.main.MainActivity;
import my.fadzlirazali.recipetory.ui.profile.UpdateUserProfileActivity;
import my.fadzlirazali.recipetory.ui.register.registerEmail.RegisterEmailActivity;

public class LoginActivity extends BaseActivity implements LoginContract {
    private static final String TAG = LoginActivity.class.getSimpleName();

    private ActivityLoginBinding binding;
    private FirebaseAuth mAuth;
    private com.facebook.CallbackManager callbackManager;

    public static void makeActivityRestartTask(Context context) {
        Intent intent = new Intent("my.fadzlirazali.recipetory.ui.login.LoginActivity");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        binding.setHandler(this);
        binding.setUser(new UserViewModel());

        // init firebase auth
        mAuth = FirebaseAuth.getInstance();

        callbackManager = com.facebook.CallbackManager.Factory.create();
        binding.fbBtn.setReadPermissions("email", "public_profile");
        LoginManager.getInstance().logInWithPublishPermissions(LoginActivity.this, Collections.singletonList("publish_actions"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }

    @Override
    public void onForgotPassword(View view) {
        ForgotPasswordActivity.start(this);}

    @Override
    public void onRegister(View view) {
        RegisterEmailActivity.start(this);
    }

    @Override
    public void onLogin(View view) {
        if(TextUtils.isEmpty(binding.username.getText().toString()) || TextUtils.isEmpty(binding.password.getText().toString())){
            Toast.makeText(this, "Please fill in the fields", Toast.LENGTH_SHORT).show();
            return;
        }
        Util.isListShown(binding.container,binding.loginProgress,false,true);

        mAuth.signInWithEmailAndPassword(binding.username.getText().toString(), binding.password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        Util.isListShown(binding.container,binding.loginProgress,true,true);
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(LoginActivity.this, R.string.auth_failed, Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user!=null){
                                Log.d(TAG, "onComplete: "+user.getDisplayName()+" "+user.getEmail()+" "+user.getPhotoUrl());
                            }
                            if(task.getResult().getAdditionalUserInfo().isNewUser()) UpdateUserProfileActivity.start(LoginActivity.this,true);
                            else MainActivity.start(LoginActivity.this);
                        }
                    }
                });
    }

    @Override
    public void onLoginFacebook(View v) {

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        Util.isListShown(binding.container,binding.loginProgress,false,true);


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Util.isListShown(binding.container,binding.loginProgress,true,true);
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if(user!=null){
                                Log.d(TAG, "onComplete: "+user.getDisplayName()+" "+user.getEmail()+" "+user.getPhotoUrl());
                            }
                            if(task.getResult().getAdditionalUserInfo().isNewUser()) UpdateUserProfileActivity.start(LoginActivity.this,true);
                            else MainActivity.start(LoginActivity.this);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed."+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        // Pass the activity result back to the Facebook SDK
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onLoginInstagram(View v) {}
}
