package my.fadzlirazali.recipetory.ui.addRecipe;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.xwray.groupie.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.RecipeIngradientListItemBinding;
import my.fadzlirazali.recipetory.model.Ingradient;

/**
 * Created by mohdfadzli on 7/11/2017.
 */

public class RecipeIngradientItem extends Item<RecipeIngradientListItemBinding> {

    private final AddRecipeActivity.IngradientListenerItem itemListener;

    /**
     * Use a LinkedHashMap and when you need to retrieve by position, convert the values into an ArrayList.
     *
     * */
    private LinkedHashMap<String,Ingradient> list;
    private int prevViewId;

    public RecipeIngradientItem(LinkedHashMap<String,Ingradient> list, AddRecipeActivity.IngradientListenerItem ingradientChangeListener) {
        this.list=list;
        this.itemListener = ingradientChangeListener;
    }


    @Override
    public int getPosition(Item item) {
        Log.d("fadzli", "getPosition: "+item.getId());
        return super.getPosition(item);
    }

    @Override
    public int getDragDirs() {
        return ViewDragHelper.DIRECTION_ALL ;
    }

    @Override
    public Item getItem(int position) {

        return super.getItem(position);
    }

    public LinkedHashMap<String,Ingradient> getList(){
        return list;
    }

    public void onMoveItem(int fromKey, int toKey){
        Ingradient temp = null;
        String tempKey=null;
        Ingradient tempA = null;
        String tempKeyA=null;
        for (String key : list.keySet()) {
            int position = new ArrayList<String>(list.keySet()).indexOf(key);
            if(fromKey-1==position){
                //https://stackoverflow.com/questions/10387290/how-to-get-position-of-key-value-in-linkedhashmap-using-its-key
                temp = list.get(key);
                tempKey=key;
//                list.remove(key);
                break;
            }
        }
        for (String key : list.keySet()) {
            int position = new ArrayList<String>(list.keySet()).indexOf(key);
            if(toKey-1==position){
                //https://stackoverflow.com/questions/10387290/how-to-get-position-of-key-value-in-linkedhashmap-using-its-key
                tempA = list.get(key);
                tempKeyA=key;
                break;
            }
        }

        //swap item
        list.put(tempKey,tempA);
        list.put(tempKeyA,temp);

//        notifyChanged(list);
//        synchronized(list){
//            list.notify();
//        }
    }


    @Override
    public int getLayout() {
        return R.layout.recipe_ingradient_list_item;
    }

    @Override
    public void bind(RecipeIngradientListItemBinding vb, int position, List<Object> payloads) {
        super.bind(vb, position, payloads);
    }

    @Override
    public void bind(final RecipeIngradientListItemBinding vb, final int position) {
       final int pos=position-1;

        if(list.keySet().toArray()[pos]==null) return ;

        vb.ingradient.setId(pos);
//        list.get(((String) list.keySet().toArray()[pos])).setUserId(String.valueOf(vb.ingradient.getUserId()));

        vb.ingradient.setTag(list.keySet().toArray()[pos]);
        Log.d("fadzli", "bind: "+pos +" ingradient "+vb.ingradient.getText().toString()+" list item"+list.get((String) vb.ingradient.getTag()).getIngradientName());

        setIngradientAC(vb.ingradient,vb.getRoot().getContext());

//        if( pos!=list.size() && vb.ingradient.getTag().equals(list.keySet().toArray()[pos])){
//            if(!TextUtils.isEmpty(vb.ingradient.getText().toString()))
//                vb.ingradient.setText(vb.ingradient.getText().toString());
//        }

        if( pos<list.size() && vb.ingradient.getTag().equals(list.keySet().toArray()[pos])){
            if(list.get(((String) vb.ingradient.getTag()))!=null){
                list.get(((String) vb.ingradient.getTag())).setIngradientName(vb.ingradient.getText().toString());
                vb.ingradient.setText((list.get(vb.ingradient.getTag()).getIngradientName()));
            }
        }

//        for (String key : list.keySet()) {
//            if(vb.ingradient.getTag().equals(key)){
//                if(!TextUtils.isEmpty(list.get(key).getIngradientName()))
//                    vb.ingradient.setText((list.get(key).getIngradientName()));
////                list.get(key).setIngradientName(vb.ingradient.getText().toString());
//            }
//        }

//        for (int i = 0; i < list.size(); i++) {
//            if (vb.ingradient.getTag().equals(list.get(i).getUserId())){
//                list.get(i).setIngradientName(vb.ingradient.getText().toString());
//            }
//        }

        vb.ingradient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(!vb.ingradient.isFocusableInTouchMode()) vb.ingradient.setFocusableInTouchMode(true);
                Log.d("fadzli", "afterTextChanged: pos"+pos+" list "+list.size());
                if( pos<list.size() && vb.ingradient.getTag().equals(list.keySet().toArray()[pos])){
                    if(list.get(((String) vb.ingradient.getTag()))!=null)
                        list.get(((String) vb.ingradient.getTag())).setIngradientName(vb.ingradient.getText().toString());
                }
            }
        });
        vb.ingradient.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d("fadzli",event+" event" +actionId +" actionId");
                if(actionId == EditorInfo.IME_ACTION_NEXT){
//                    vb.ingradient.clearFocus();
                    // Gets the id of the view to use when the next focus
                    if(pos+1 <list.size()){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            vb.ingradient.setShowSoftInputOnFocus(true);
                            v.setHint("Please add your ingredient lah");
//                            Util.hideKeyboard(vb.ingradient);
                        }
                    } else if(list.size()==pos+1){
                        // enter new ingradient line
                        itemListener.onItemInserted();
                        v.requestFocus();
                    }
                }
                return false;
            }
        });

        vb.ingradient.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("fadzli", "onKey: "+keyCode+" event "+event.getAction());
                if(event.getAction()==KeyEvent.ACTION_DOWN && keyCode==KeyEvent.KEYCODE_ENTER){
                    vb.ingradient.clearFocus();
                    // Gets the id of the view to use when the next focus
                    if(pos+1 <list.size()){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            vb.ingradient.setShowSoftInputOnFocus(true);
//                            Util.hideKeyboard(vb.ingradient);
                        }
                    } else if(list.size()==pos+1){
                        // enter new ingradient line
                        itemListener.onItemInserted();
                        vb.ingradient.requestFocus();
                    }
                }
                return true;
            }
        });



        vb.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new AlertDialog.Builder(vb.getRoot().getContext())
                        .setTitle("Warning")
                        .setMessage("Are you sure want to delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                list.remove(pos);

                                //https://stackoverflow.com/questions/7100555/preventing-catching-illegalargumentexception-parameter-must-be-a-descendant-of
                                View currentFocus = ((AddRecipeActivity) v.getContext()).getCurrentFocus();
                                if(currentFocus!=null)
                                    currentFocus.clearFocus();


                                for (String key : list.keySet()) {
                                    int position = new ArrayList<String>(list.keySet()).indexOf(key);
                                    if(vb.ingradient.getTag().equals(key)){
                                        //https://stackoverflow.com/questions/10387290/how-to-get-position-of-key-value-in-linkedhashmap-using-its-key
                                        list.remove(key);
                                        itemListener.onItemRemoved(position);
                                        break;
                                    }
                                }

                                //remove based on view id
//                                for (int i = 0; i < list.size() ; i++){
//                                    if(list.get(i).getUserId().equals(vb.ingradient.getTag())){
//                                        list.remove(i);
//                                        itemListener.onItemRemoved(i);
//                                    }
//                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

    }

    public void setIngradientAC(AutoCompleteTextView v ,Context context){
//        String[] units = context.getResources().getStringArray(R.array.units);
        List<String> units = Arrays.asList(context.getResources().getStringArray(R.array.units));
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(units);

        CustomAdapter adapter = new CustomAdapter(context, android.R.layout.simple_list_item_1, v.getId() , arrayList);
        v.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
