package my.fadzlirazali.recipetory.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.xwray.groupie.GroupAdapter;
import com.xwray.groupie.Item;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import my.dearfadz.util.CircleTransform;
import my.dearfadz.util.DateTimeUtil;
import my.dearfadz.util.Util;
import my.dearfadz.widget.SpacesItemDecoration;
import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.ActivityRecipeDetailBinding;
import my.fadzlirazali.recipetory.event.RecipeEvent;
import my.fadzlirazali.recipetory.model.Ingradient;
import my.fadzlirazali.recipetory.model.Method;
import my.fadzlirazali.recipetory.model.Recipe;
import my.fadzlirazali.recipetory.model.RecipeType;
import my.fadzlirazali.recipetory.ui.addRecipe.PhotoPagerAdapter;
import my.fadzlirazali.recipetory.ui.addRecipe.TypeItem;
import my.fadzlirazali.recipetory.ui.profile.UserProfileActivity;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class RecipeDetailActivity extends AppCompatActivity {

    private static final String RECIPE = "recipe_item";
    private static final String RECIPE_ID = "id";
    private static final String RECIPE_LIST_POSITION = "position";
    private ActivityRecipeDetailBinding binding;
    private Recipe mRecipe;
    private String mLocation="";
    private boolean mIsOwner=false;
    private Bitmap mBitmap;
    private ShareDialog fbDialog;
    private File mFile;

    public static void startOwnedByRecipeId(Context context, String id,ImageView iv, TextView tv) {
        Intent intent = new Intent(context, RecipeDetailActivity.class);
        intent.putExtra(RECIPE_ID,id);
        Pair<View, String> p1 = Pair.create((View)tv, "text");
        Pair<View, String> p2 = Pair.create((View)iv, "recipe");
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((AppCompatActivity) context, p1, p2);
        context.startActivity(intent,options.toBundle());
    }

    public static void start(Context context, Recipe recipe, ImageView iv, TextView tv, int position) {
        Intent intent = new Intent(context, RecipeDetailActivity.class);
        intent.putExtra(RECIPE,recipe.toJson());
        intent.putExtra(RECIPE_LIST_POSITION,position);
        //used this if single item transition
        //ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(((MainActivity) context), (View) iv,"recipe");
        //context.startActivity(intent,optionsCompat.toBundle());
        Pair<View, String> p1 = Pair.create((View)tv, "text");
        Pair<View, String> p2 = Pair.create((View)iv, "recipe");
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((AppCompatActivity) context, p1, p2);
        context.startActivity(intent,options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_recipe_detail);

        //fb
        fbDialog = new ShareDialog(RecipeDetailActivity.this);

        //photos
        final PhotoPagerAdapter photoPagerAdapter = new PhotoPagerAdapter(this);
        binding.photoPager.setAdapter(photoPagerAdapter);
        binding.indicator.setViewPager(binding.photoPager);
        final float density = getResources().getDisplayMetrics().density;
        binding.indicator.setRadius(5 * density); //Set circle indicator radius

        if(getIntent().hasExtra(RECIPE)){   //from Main Screen
            mRecipe = Recipe.fromJson(getIntent().getStringExtra(RECIPE));
            setRecipeUI(photoPagerAdapter,mRecipe);
        } else if(getIntent().hasExtra(RECIPE_ID)){
            mIsOwner=true;
            //call firebase network for specific recipe
            //wrap into Recipe obj
            FirebaseDatabase.getInstance().getReference().child("recipe").child(getIntent().getStringExtra(RECIPE_ID)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //https://medium.com/google-developers/death-by-a-thousand-casts-460d25e8716e
                    // avoid to check 1 by 1 of object & do a lot of casting
                    Recipe recipe = dataSnapshot.getValue(Recipe.class);
//                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
//                        //Mapping data dari snapshot ke dlm object Recipe kita, & simpan primary key pd object recipe untuk handle edit & delete data
//                         recipe = snapshot.getValue(Recipe.class);
//                    }
                    if(recipe!=null){
                        recipe.setId(getIntent().getStringExtra(RECIPE_ID));
                        setRecipeUI(photoPagerAdapter,recipe);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, "onCancelled: "+databaseError.getMessage());
                }
            });

        }
    }


    private void setRecipeUI(PhotoPagerAdapter photoPagerAdapter, final Recipe recipe) {
        if(recipe.getPathList()!=null && recipe.getPathList().size()>0){
            photoPagerAdapter.addAllPath(recipe.getPathList());
            photoPagerAdapter.notifyDataSetChanged();
            binding.photoPager.setCurrentItem(photoPagerAdapter.getCount());        //always move to latest uploaded photo
            Glide.with(this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getPathList().get(0))
                    .override(300, 250)
                    .centerCrop()
                    .into(binding.dummyTransitionIv);
        }

        if(!TextUtils.isEmpty(recipe.getTitle()))
            binding.recipeTitle.setText(recipe.getTitle());

        if(!TextUtils.isEmpty(recipe.getDesc()))
            binding.recipeDesc.setText(recipe.getDesc());

        if(!TextUtils.isEmpty(recipe.getServing()))
            binding.servings.setText(recipe.getServing()+" people");

        if(!TextUtils.isEmpty(recipe.getDuration()))
            binding.duration.setText(recipe.getDuration()+" mins");

        if(!TextUtils.isEmpty(recipe.getCuisine()))
            binding.cuisine.setText(recipe.getCuisine());

        //recipe types
        GroupAdapter groupAdapter = new GroupAdapter();
        binding.typeRV.addItemDecoration(new SpacesItemDecoration(Util.dpToPx(this,2)));
        binding.typeRV.setAdapter(groupAdapter);
        binding.typeRV.setItemViewCacheSize(recipe.getTypeChecked().length());  //Number of views to cache offscreen before returning them to the general recycled view pool

        ArrayList<RecipeType> recipeTypes = new ArrayList<>();
        if(recipe.getTypeChecked()!=null){
            String [] typeArr = getResources().getStringArray(R.array.IngredientType);
            for (int i = 0; i < typeArr.length; i++) {
                if(recipe.getTypeChecked().contains(String.valueOf(i)))    //if string contain the id
                {
                    RecipeType rt = new RecipeType();
                    rt.setChecked(true);
                    rt.setId(String.valueOf(i));
                    rt.setType(typeArr[i]);
                    recipeTypes.add(rt);
                }
            }
        }

        List<Item> itemList = new ArrayList<>();
        itemList.add(new TypeItem(recipeTypes,true));
        groupAdapter.addAll(itemList);


        //ingredient
        //String [] ingArr = new String[]{"1/2 ts brown sugar","100 gm cheese","1 Onion","1/4 ts garlic"};
        StringBuilder ingredient = new StringBuilder();
        for (Ingradient item : recipe.getIngredient()) {
            ingredient.append(item.getIngradientName()).append("\n");
        }
        binding.ingredientDetail.setText(ingredient.toString());

        //method
        Method method1 = new Method("0","First, masukan asam garam lepas tu errr jap");
        Method method2 = new Method("1","Second, masukan lagi asam garam lepas tu sambung esok");
        //ArrayList<Method> methods = new ArrayList<>(mRecipe.getMethod());
        setMethodItemView(recipe.getMethod());

        //check total recooked
        if(recipe.getRecooked()!=0) binding.totalRecooked.setText(String.valueOf(recipe.getRecooked()));

        final String UID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //else UID = mRecipe.getUserId();

        //NETWORK check is liked
        FirebaseDatabase.getInstance().getReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("likes")){
                    if(dataSnapshot.child("likes").hasChild(UID)){
                        if(dataSnapshot.child("likes").child(UID).hasChild(recipe.getId())){
                            FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    binding.loveCheckableTv.setChecked(dataSnapshot.exists());
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        binding.loveCheckableTv.setText(String.valueOf(recipe.getLikes()));
        binding.loveCheckableTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(binding.loveCheckableTv.getText().toString());
                binding.loveCheckableTv.setChecked(!binding.loveCheckableTv.isChecked());

                if(binding.loveCheckableTv.isChecked()){
                    binding.loveCheckableTv.setText(String.valueOf(++count));
                    FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).setValue(true);
                } else{
                    binding.loveCheckableTv.setText(String.valueOf(--count<0 ? 0 : --count));
                    FirebaseDatabase.getInstance().getReference().child("likes").child(UID).child(recipe.getId()).removeValue();
                }
                final int countPost=count;
                recipe.setLikes(countPost>0? (long)countPost : 0);
                FirebaseDatabase.getInstance().getReference().child("recipe").child(recipe.getId()).child("likes").setValue(countPost>0? countPost : 0);

                //update the latest one
                if(mRecipe!=null && getIntent().hasExtra(RECIPE_LIST_POSITION)) EventBus.getDefault().postSticky(new RecipeEvent.OnNotifyRecipe(mRecipe,getIntent().getIntExtra(RECIPE_LIST_POSITION,-1)));
            }
        });


        //NETWORK check is bookmark
        FirebaseDatabase.getInstance().getReference().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("bookmarks")){
                    if(dataSnapshot.child("bookmarks").hasChild(UID)){
                        if(dataSnapshot.child("bookmarks").child(UID).hasChild(recipe.getId())){
                            FirebaseDatabase.getInstance().getReference().child("bookmarks").child(UID).child(recipe.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    binding.bookmarkCheckableTv.setChecked(dataSnapshot.exists());
                                    }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d(TAG, "onCancelled: "+databaseError.getDetails());
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        binding.bookmarkCheckableTv.setText(String.valueOf(recipe.getBookmarks()));
        binding.bookmarkCheckableTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(binding.bookmarkCheckableTv.getText().toString());
                binding.bookmarkCheckableTv.setChecked(!binding.bookmarkCheckableTv.isChecked());
                DatabaseReference bookmarkRef = FirebaseDatabase.getInstance().getReference().child("bookmarks").child(UID).child(recipe.getId());

                if(binding.bookmarkCheckableTv.isChecked()){
                    binding.bookmarkCheckableTv.setText(String.valueOf(++count));
                    Recipe bookmark =new Recipe();
                    bookmark.setTitle(recipe.getTitle());
                    bookmark.setHasCooked(false);
                    if(recipe.getPathList()!=null && recipe.getPathList().get(0)!=null) bookmark.setSmallPhoto(recipe.getPathList().get(0));
                    bookmarkRef.setValue(bookmark, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            if(databaseError!=null) Toast.makeText(RecipeDetailActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } else{
                    binding.bookmarkCheckableTv.setText(String.valueOf(--count));
                    bookmarkRef.removeValue();
                }

                final int countPost=count;
                FirebaseDatabase.getInstance().getReference().child("recipe").child(recipe.getId()).child("bookmarks").setValue(countPost);
            }
        });


        if(!TextUtils.isEmpty(recipe.getUserPhotoUrl())){
            Glide.with(this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                    .load(recipe.getUserPhotoUrl())
                    .centerCrop()
                    .error(R.color.black)
                    .transform(new CircleTransform(this))
                    .placeholder(R.mipmap.ic_launcher)
                    .into(binding.userPhoto);
        }
        if(!TextUtils.isEmpty(recipe.getFullname())) binding.username.setText(recipe.getFullname());
        if(recipe.getCreatedBy()!=0) binding.postedDate.setText(DateTimeUtil.recipePostedTime(new DateTime(Math.abs(recipe.getCreatedBy()))));
        binding.userInfoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserProfileActivity.start(view.getContext(),recipe.getUserId());
            }
        });


        FirebaseDatabase.getInstance().getReference().child("users").child(UID).child("profile")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot data: dataSnapshot.getChildren()) {
                            if(data.getKey().equals("location")){
                                mLocation = (String) data.getValue();
                            }
                            if(data.getKey().equals("state")){
                                String state = (String) data.getValue();
                                loadCountry(state);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(RecipeDetailActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });


        if(recipe.getPathList()!=null &&recipe.getPathList().size()>0) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                    try {
                        mBitmap = Glide.with(RecipeDetailActivity.this)  //https://futurestud.io/tutorials/glide-image-resizing-scaling
                                .load(recipe.getPathList().get(0)).asBitmap().into(100, 100).get();

                        mFile = new File(Environment.getExternalStorageDirectory() + File.separator + "test.jpg");
                        mFile.createNewFile();

                        //Convert bitmap to byte array
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        mBitmap.compress(Bitmap.CompressFormat.JPEG, 90 /*ignored for PNG*/, bos);
                        byte[] bitmapdata = bos.toByteArray();

                        //write the bytes in file
                        FileOutputStream fos = new FileOutputStream(mFile);
                        fos.write(bitmapdata);
                        fos.flush();
                        fos.close();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }).start();
        }
    }

    private void loadCountry(final String state) {
        Ion.with(this)
                .load("http://country.io/names.json")
                .addHeader("Content-Type", "application/json")
                .asString()
                .withResponse().setCallback(new FutureCallback<Response<String>>() {
             HashMap<String,String> mCountryMaps = new HashMap<>();
            @Override
            public void onCompleted(Exception e, Response<String> result) {
                if (result.getHeaders().code() == 200) {
                    mCountryMaps = new Gson().fromJson(result.getResult(), new TypeToken<HashMap<String, String>>() {
                    }.getType());

                    //if location stored before
                    if (mLocation != null && mCountryMaps != null) {
                        for (Map.Entry<String, String> map : mCountryMaps.entrySet()) {
                            if (mLocation.equals(map.getKey())) {
                                binding.userDetail.setText(TextUtils.isEmpty(state)? map.getValue(): state +", "+map.getValue());
                            }
                        }
                    }
                } else {
                    if (result.getException().getMessage() != null)
                        Toast.makeText(RecipeDetailActivity.this, result.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

        private void setMethodItemView(final ArrayList<Method> methods) {
        int num;
        for (int i = 0; i < methods.size(); i++) {

            LinearLayout ll = new LinearLayout(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            ll.setLayoutParams(params);
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setGravity(Gravity.CENTER|Gravity.START);
            ll.setPadding(10,5,10,10);

            num=i+1;
            TextView numTv = new TextView(this);
            numTv.setText(String.valueOf(num));
            numTv.setTextColor(ContextCompat.getColor(this,R.color.white));
            numTv.setBackground(ContextCompat.getDrawable(this,R.drawable.solid_green_btn));
            numTv.setPadding(15,10,15,10);

            final CheckedTextView methodTv = new CheckedTextView(this);
            LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int space = Util.dpToPx(this,8);
            paramsTv.setMargins(space,0,0,0);
            methodTv.setLayoutParams(paramsTv);
            methodTv.setText(methods.get(i).getMethodName());
            methodTv.setTextSize(15);
            methodTv.setTextColor(ContextCompat.getColor(this,R.color.lightBlack));
            methodTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!methodTv.isChecked()) methodTv.setPaintFlags(methodTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    else methodTv.setPaintFlags(methodTv.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                    methodTv.setChecked(!methodTv.isChecked());
                }
            });
            ll.addView(numTv);
            ll.addView(methodTv);
            binding.methodLayout.addView(ll);
        }
    }

    public void onCook(View view) {
        if(mRecipe!=null && binding.bookmarkCheckableTv.isChecked() && !TextUtils.isEmpty(binding.totalRecooked.getText())){
            long count = Long.valueOf(binding.totalRecooked.getText().toString());
            FirebaseDatabase.getInstance().getReference().child("recipe").child(mRecipe.getId()).child("recooked").setValue(++count);
            binding.totalRecooked.setText(String.valueOf(count));
        }
    }

    //https://guides.codepath.com/android/Sharing-Content-with-Intents
    public void onShare(View view) {
        //https://stackoverflow.com/questions/19253786/how-to-copy-text-to-clip-board-in-android?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Test", generateRecipeString());
        if(clipboard!=null) clipboard.setPrimaryClip(clip);

        new AlertDialog.Builder(this)
                .setTitle("SHARE RECIPE")
                .setMessage("Which's sharing provider?")
                .setPositiveButton("FACEBOOK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharePhoto photo = null;
                        if(mBitmap!=null){
                            photo = new SharePhoto.Builder()
                                    .setCaption(mRecipe.getTitle()+" ajsdoiajsoidjasd") //TODO, not allowed :( https://developers.facebook.com/docs/apps/review/prefill
                                    .setUserGenerated(true)
                                    .setBitmap(mBitmap)
                                    .build();
                        }
                        if(photo!=null){
                            SharePhotoContent content =  new SharePhotoContent.Builder()
                                    .addPhoto(photo)
                                    .build();

                            if(ShareDialog.canShow(SharePhotoContent.class))
                                fbDialog.show(content, ShareDialog.Mode.AUTOMATIC);

                        } else {
                            ShareLinkContent content = new ShareLinkContent.Builder()
                                    .setContentUrl(Uri.parse("https://developers.facebook.com"))
                                    .setQuote(mRecipe.getTitle()).build();

                            if(ShareDialog.canShow(ShareLinkContent.class))
                                fbDialog.show(content,ShareDialog.Mode.AUTOMATIC);
                        }
                    }

                }).setNegativeButton("Other", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Share Provider
                        String shareText = generateRecipeString();
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);

                        // change the type of data you need to share, for image use "image/*"
                        if(mRecipe.getPathList().size()>0){
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(mFile));
                        }
                        //intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, shareText);
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                }).show();
    }

    public String generateRecipeString(){
        if(mRecipe!=null){
            String ingredients ="";
            for (Ingradient ing: mRecipe.getIngredient()) {
                if(!TextUtils.isEmpty(ing.getIngradientName()))
                    ingredients=ingredients+ing.getIngradientName()+" \n";
            }

            String methods ="";
            for (Method method: mRecipe.getMethod()) {
                if(!TextUtils.isEmpty(method.getMethodName()))
                    methods=methods+method.getMethodName()+" \n";
            }

            return mRecipe.getTitle()+"\n"+mRecipe.getDesc()+"\n\nIngredients\n"+ingredients+"\nMethods\n"+methods;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
