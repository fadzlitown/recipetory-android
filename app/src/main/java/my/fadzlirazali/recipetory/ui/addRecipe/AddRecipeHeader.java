package my.fadzlirazali.recipetory.ui.addRecipe;

import android.support.annotation.NonNull;

import com.xwray.groupie.Item;

import my.fadzlirazali.recipetory.R;
import my.fadzlirazali.recipetory.databinding.AddRecipeHeaderItemBinding;

public class AddRecipeHeader extends Item<AddRecipeHeaderItemBinding> {

    private final String title;

    public AddRecipeHeader(@NonNull String title) {
    this.title=title;
    }

    @Override
    public int getLayout() {
        return R.layout.add_recipe_header_item;
    }

    @Override
    public void bind(AddRecipeHeaderItemBinding vb, int position) {
        vb.headerTitle.setText(title);
    }

}