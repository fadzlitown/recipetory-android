package my.fadzlirazali.recipetory.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Constant {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(value = {LoginType.PHONE, LoginType.EMAIL})
    public @interface LoginType {
        int PHONE = 0;
        int EMAIL = 1;
    }


    @IntDef({RoleType.PUBLIC, RoleType.USER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RoleType {
        int PUBLIC = 1;
        int USER = 2;
    }


    @IntDef({RecipeType.SELF , RecipeType.BOOKMARK})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RecipeType {
        int SELF = 1;
        int BOOKMARK = 2;
    }
}
