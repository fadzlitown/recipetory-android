package my.fadzlirazali.recipetory.util;

import android.content.Context;
import android.content.SharedPreferences;

import javax.annotation.Nullable;

public class ProfileSharedPref {

    private static final String TOTAL_RECIPE = "total_recipe";
    private static final String TOTAL_RECOOKED = "total_recooked";
    private static final String PROFILE = "profile";

    public static void setProfile(Context context, @Nullable String profile) {
        getSecurePreference(context)
                .edit()
                .putString(PROFILE, profile)
                .apply();
    }

    public static String getProfile(Context context) {
        return getSecurePreference(context)
                .getString(PROFILE, null);
    }



    public static void setTotalRecipe(Context context, String no) {
        getSecurePreference(context)
                .edit()
                .putString(TOTAL_RECIPE, no)
                .apply();
    }

    public static String getTotalRecipe(Context context) {
        return getSecurePreference(context)
                .getString(TOTAL_RECIPE, "0");
    }

    public static void setTotalRecooked(Context context, String no) {
        getSecurePreference(context)
                .edit()
                .putString(TOTAL_RECOOKED, no)
                .apply();
    }

    public static String getTotalRecooked(Context context) {
        return getSecurePreference(context)
                .getString(TOTAL_RECOOKED, "0");
    }

    private static SharedPreferences getSecurePreference(Context context) {
        return context.getSharedPreferences("my_recipe_prefs.xml", Context.MODE_PRIVATE);
    }
}
