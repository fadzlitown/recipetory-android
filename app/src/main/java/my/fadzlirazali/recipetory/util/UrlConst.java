package my.fadzlirazali.recipetory.util;

import android.net.Uri;

public class UrlConst {

    public static final String HTTP_CONTENT_TYPE = "Content-Type";
    static final String TYPE_JSON = "application/json";

    static final String SERVER_URL = "http://35.184.84.182//elasticsearch";
    private static final String API_RECIPES = SERVER_URL + "/recipes";

    public static String getBaseUrl() {
        return SERVER_URL;
    }


    public static String searchRecipe(String search) {
        return Uri.parse(API_RECIPES).buildUpon()
                .appendPath("_search")
                .appendQueryParameter("q", search)
                .toString();

    }
}
