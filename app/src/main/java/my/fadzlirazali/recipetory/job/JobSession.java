package my.fadzlirazali.recipetory.job;

public interface JobSession {
    public String getSessionId();
}