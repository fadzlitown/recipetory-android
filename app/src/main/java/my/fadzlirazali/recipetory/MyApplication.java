package my.fadzlirazali.recipetory;

import android.app.Application;
import android.util.LruCache;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;

import net.danlew.android.joda.JodaTimeAndroid;

import org.greenrobot.greendao.database.Database;

import my.fadzlirazali.recipetory.orm.DaoMaster;
import my.fadzlirazali.recipetory.orm.DaoSession;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApplication extends Application {

    private static MyApplication instance;
    private JobManager jobManager;
    private static LruCache<String, String> lruCache;

    /** A flag to show how easily you can switch from standard SQLite to the encrypted SQLCipher. */
    public static final boolean ENCRYPTED = true;
    private DaoSession daoSession;

    public static MyApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        configureJobManager();
        configureDoa();
        JodaTimeAndroid.init(this);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_regular))
                .setFontAttrId(R.attr.fontPath)
                .build());

    }

    private void configureDoa() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, ENCRYPTED ? "recipe-db-encrypted" : "recipe-db");
        Database db = ENCRYPTED ? helper.getEncryptedWritableDb("super-secret") : helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    private void configureJobManager() {
        Configuration.Builder builder = new Configuration.Builder(this)
                .minConsumerCount(1)// always keep at least one consumer alive
                .maxConsumerCount(3)// up to 3 consumers at a time
                .loadFactor(3)// 3 jobs per consumer
                .consumerKeepAlive(120);// wait 2 minute

        jobManager = new JobManager(builder.build());
    }

    public JobManager getJobManager() {
        return jobManager;
    }

    public static void addJobInBackground(Job job) {
        MyApplication.getInstance().getJobManager().addJobInBackground(job);
    }

    public static LruCache<String, String> getLruCache() {
        if (lruCache == null) {
            final int maxMemory = ((int) (Runtime.getRuntime().maxMemory() / 1024));
            final int cacheSize = maxMemory / 8;

            lruCache = new LruCache<String, String>(cacheSize);
        }
        return lruCache;
    }

}