package my.fadzlirazali.recipetory.model;

import my.dearfadz.util.Util;

public class IndexElastic {

    /**
     * took : 4
     * timed_out : false
     * _shards : {"total":5,"successful":5,"skipped":0,"failed":0}
     * hits : {"total":1,"max_score":1.5753641,"hits":[{"_index":"recipes","_type":"recipe","_id":"-LKlDIWvwFPfll2cgEui","_score":1.5753641,"_source":{}}]}
     */

    private int took;
    private boolean timed_out;
    private HitsX hits;

    public int getTook() {
        return took;
    }

    public void setTook(int took) {
        this.took = took;
    }

    public boolean isTimed_out() {
        return timed_out;
    }

    public void setTimed_out(boolean timed_out) {
        this.timed_out = timed_out;
    }

    public HitsX getHits() {
        return hits;
    }

    public void setHits(HitsX hits) {
        this.hits = hits;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static IndexElastic fromJson(String json) {
        return Util.getGson().fromJson(json, IndexElastic.class);
    }

}
