package my.fadzlirazali.recipetory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;

public class Recipe {


    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("typeChecked")
    @Expose
    public String typeChecked;

    private ArrayList<String> pathList;
    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("fullname")
    @Expose
    public String fullname;
    @SerializedName("userPhotoUrl")
    @Expose
    public String userPhotoUrl;
    @SerializedName("createdBy")
    @Expose
    public Long createdBy;

    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("desc")
    @Expose
    public String desc;
    @SerializedName("serving")
    @Expose
    public String serving;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("cuisine")
    @Expose
    public String cuisine;
    @SerializedName("likes")
    @Expose
    public Long likes;

    @SerializedName("bookmarks")
    @Expose
    public Long bookmarks;

    @SerializedName("Type")
    @Expose
    public ArrayList<RecipeType> typeList;

    @SerializedName("PhotoUri")
    @Expose
    public ArrayList<String> uriList;

    @SerializedName("ingredient")
    @Expose
    public ArrayList<Ingradient> ingredient;
    @SerializedName("method")
    @Expose
    public ArrayList<Method> method;

    @SerializedName("recooked")
    @Expose
    public Long recooked;

    @SerializedName("smallPhoto")
    @Expose
    public String smallPhoto;

    @SerializedName("hasCooked")
    @Expose
    private boolean hasCooked;



    public Recipe(String id, String title,String desc, String serving, String duration, String cuisinee, ArrayList<RecipeType> typeList, ArrayList<String> uriList,ArrayList<String> pathList,  ArrayList<Ingradient> ingredient, ArrayList<Method> method) {
        this.userId = id;
        this.title = title;
        this.desc = desc;
        this.serving = serving;
        this.duration = duration;
        this.cuisine = cuisinee;
        this.typeList = typeList;
        this.uriList = uriList;
        this.pathList = pathList;
        this.ingredient = ingredient;
        this.method = method;
    }

    public Recipe(String id, String fullname, String userPhotoUrl, long createdBy,  String title,String desc, String serving, String duration, String cuisinee, String typeChecked, ArrayList<String> uriList,ArrayList<String> pathList,  ArrayList<Ingradient> ingredient, ArrayList<Method> method,long likes, long bookmarks, long recooked) {
        this.userId = id;
        this.fullname = fullname;
        this.userPhotoUrl = userPhotoUrl;
        this.createdBy = createdBy;
        this.title = title;
        this.desc = desc;
        this.serving = serving;
        this.duration = duration;
        this.cuisine = cuisinee;
        this.typeChecked = typeChecked;
        this.uriList = uriList;
        this.pathList = pathList;
        this.ingredient = ingredient;
        this.method = method;
        this.likes=likes;
        this.bookmarks=bookmarks;
        this.recooked=recooked;
    }

    public Recipe() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserId(String id) {
        this.userId = id;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public String getServing() {
        return serving;
    }

    public String getDuration() {
        return duration;
    }

    public String getCuisine() {
        return cuisine;
    }

    public ArrayList<RecipeType> getTypeList() {
        return typeList;
    }

    public ArrayList<String> getUriList() {
        return uriList;
    }

    public ArrayList<String> getPathList() {
        return pathList;
    }

    public void setPathList(ArrayList<String> pathList) {
        this.pathList = pathList;
    }

    public ArrayList<Ingradient> getIngredient() {
        return ingredient;
    }

    public ArrayList<Method> getMethod() {
        return method;
    }

    public String getTypeChecked() {
        return typeChecked;
    }

    public void setTypeChecked(String typeChecked) {
        this.typeChecked = typeChecked;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public String getFullname() {
        return fullname;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public Long getBookmarks() {
        return bookmarks;
    }

    public Long getRecooked() {
        return recooked;
    }

    public void setRecooked(Long recooked) {
        this.recooked = recooked;
    }

    public String getSmallPhoto() {
        return smallPhoto;
    }

    public void setSmallPhoto(String smallPhoto) {
        this.smallPhoto = smallPhoto;
    }

    public boolean isHasCooked() {
        return hasCooked;
    }

    public void setHasCooked(boolean hasCooked) {
        this.hasCooked = hasCooked;
    }

    public void clear(){
        if(bookmarks!=null)
            bookmarks=null;
        if(createdBy!=null)
            createdBy=null;
        if(recooked!=null)
            recooked=null;
        if(likes!=null)
            likes=null;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static Recipe fromJson(String json) {
        return Util.getGson().fromJson(json, Recipe.class);
    }

    public static List<Recipe> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<Recipe>>() {}.getType());
    }

}
