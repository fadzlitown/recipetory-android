package my.fadzlirazali.recipetory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;

/**
 * Created by mohdfadzli on 7/11/2017.
 */

public class Ingradient {

    @SerializedName("id")
    @Expose
    private String Id;
    @SerializedName("IngradientName")
    @Expose
    private String IngradientName;

    public Ingradient(){

    }

    public Ingradient(String id, String name) {
        this.Id=id;
        this.IngradientName=name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIngradientName() {
        return IngradientName;
    }

    public void setIngradientName(String ingradientName) {
        IngradientName = ingradientName;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static Ingradient fromJson(String json) {
        return Util.getGson().fromJson(json, Ingradient.class);
    }

    public static List<Ingradient> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<Ingradient>>() {}.getType());
    }
}
