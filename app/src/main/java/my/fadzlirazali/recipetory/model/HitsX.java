package my.fadzlirazali.recipetory.model;

import java.util.List;

public class HitsX {
        /**
         * total : 1
         * max_score : 1.5753641
         * hits : [{"_index":"recipes","_type":"recipe","_id":"-LKlDIWvwFPfll2cgEui","_score":1.5753641,"_source":{}}]
         */

        private int total;
        private double max_score;
        private List<Hits> hits;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public double getMax_score() {
            return max_score;
        }

        public void setMax_score(double max_score) {
            this.max_score = max_score;
        }

        public List<Hits> getHits() {
            return hits;
        }

        public void setHits(List<Hits> hits) {
            this.hits = hits;
        }
        
    }