package my.fadzlirazali.recipetory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.joda.time.Instant;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;

public class User {

    @SerializedName("UserId")
    @Expose
    private long UserId;
    @SerializedName("UserName")
    @Expose
    private String UserName;
    @SerializedName("UserFullName")
    @Expose
    private String UserFullName;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("CreateDate")
    @Expose
    private Instant CreateDate;

    public long getUserId() {
        return UserId;
    }

    public String getUserName() {
        return UserName;
    }

    public String getUserFullName() {
        return UserFullName;
    }

    public String getEmail() {
        return Email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Instant getCreateDate() {
        return CreateDate;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static User fromJson(String json) {
        return Util.getGson().fromJson(json, User.class);
    }

    public static List<User> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<User>>() {}.getType());
    }

}