package my.fadzlirazali.recipetory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;

/**
 * Created by mohdfadzli on 28/11/2017.
 */

public class RecipeType {
    @SerializedName("id")
    @Expose
    private String Id;
    @SerializedName("Type")
    @Expose
    private String Type;
    @SerializedName("IsChecked")
    @Expose
    private boolean IsChecked;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public boolean isChecked() {
        return IsChecked;
    }

    public void setChecked(boolean checked) {
        IsChecked = checked;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static RecipeType fromJson(String json) {
        return Util.getGson().fromJson(json, RecipeType.class);
    }

    public static List<RecipeType> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<RecipeType>>() {}.getType());
    }
}
