package my.fadzlirazali.recipetory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import my.dearfadz.util.Util;

public class Method {

    @SerializedName("id")
    @Expose
    private String Id;
    @SerializedName("MethodName")
    @Expose
    private String MethodName;

    public Method(){

    }

    public Method(String id, String name) {
        this.Id=id;
        this.MethodName=name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getMethodName() {
        return MethodName;
    }

    public void setMethodName(String methodName) {
        MethodName = methodName;
    }

    public String toJson() {
        return Util.getGson().toJson(this);
    }

    public static Method fromJson(String json) {
        return Util.getGson().fromJson(json, Method.class);
    }

    public static List<Method> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<Method>>() {}.getType());
    }
}
