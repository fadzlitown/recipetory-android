package my.fadzlirazali.recipetory.orm;

import com.google.gson.reflect.TypeToken;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.ArrayList;

import my.dearfadz.util.Util;

/**
 * Created by mohdfadzli on 28/11/2017.
 */


@Entity
public class Recipe {

    @Id
    private Long id;

    @NotNull
    public String recipeJson;

    @Generated(hash = 779619340)
    public Recipe(Long id, @NotNull String recipeJson) {
        this.id = id;
        this.recipeJson = recipeJson;
    }

    @Generated(hash = 829032493)
    public Recipe() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecipeJson() {
        return this.recipeJson;
    }

    public void setRecipeJson(String recipeJson) {
        this.recipeJson = recipeJson;
    }

    public static my.fadzlirazali.recipetory.model.Recipe fromJson(String json) {
        return Util.getGson().fromJson(json, my.fadzlirazali.recipetory.model.Recipe.class);
    }

    public static ArrayList<my.fadzlirazali.recipetory.model.Recipe> toList(String json) {
        return Util.getGson().fromJson(json, new TypeToken<ArrayList<my.fadzlirazali.recipetory.model.Recipe>>() {}.getType());
    }

}
